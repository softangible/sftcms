require('request-local');
var path = require('path'),
    cluster = require('cluster'),
    mongoose = require('mongoose'),
    kcf = require('keystone-custom-fieldtypes'),
    sftModuleRoot = (function(_rootPath) {
        var parts = _rootPath.split(path.sep);
        parts.pop()
        return parts.join(path.sep);
    })(module.paths[0]),
    moduleRoot = (function(_rootPath) {
        return _rootPath.substring(0,_rootPath.indexOf('node_'));
    })(module.parent ? module.parent.paths[0] : module.paths[0]);    
    
function importFieldTypes(fieldsPath){
    kcf.loadFromDir(path.relative(moduleRoot,fieldsPath));  
}

//if (cluster.isMaster) {
//    importFieldTypes(path.join(sftModuleRoot,'./lib/fieldTypes'));
//}

//Loading Configuration according to SFT_ENV
require('./lib/core/config/configLoader')(moduleRoot);

var mainRouter = require('./lib/router'),
    keystone = require('keystone'),
    getterSetter = require('./lib/core/getterSetter');

var SftCms = function () {
    require('./lib/middleware/load-middlewares')(this);
    
    this.set('sft root',sftModuleRoot);
    this.set('module root',moduleRoot);
    
}

SftCms.prototype.List = require('./lib/List');
SftCms.prototype.View = require('./lib/View');
SftCms.prototype.ActiveTrail = require('./lib/activeTrail');
SftCms.prototype.Scheduler = require('./lib/scheduler');

SftCms.prototype.importFieldTypes = importFieldTypes;
SftCms.prototype.init = require('./lib/core/init');
SftCms.prototype.hook = require('./lib/core/hook');
SftCms.prototype.dbUtils = require('./lib/core/dbUtils');
SftCms.prototype.importModels = require('./lib/core/importModels');

SftCms.prototype.routes = require('./lib/routes');
SftCms.prototype.router = require('./lib/router');

SftCms.prototype.config = require('./lib/core/config/config');
SftCms.prototype.customAdmin = require('./lib/customAdmin');
SftCms.prototype.Logger = require('./lib/core/logging').logger;
SftCms.prototype.Email = require('./lib/core/Email');
SftCms.prototype.Storage = require('./lib/storage');
SftCms.prototype.sdk = require('./lib/sdks');


SftCms.prototype.get = getterSetter.get;
SftCms.prototype.set = getterSetter.set;

SftCms.prototype.importer = require('./lib/core/importer');

SftCms.prototype.Auth = require('./lib/auth');

SftCms.prototype.ImagePicker = require('./lib/core/imagePicker');

SftCms.prototype.setRouteMiddleware = function(fn){
    var router = keystone.express.Router();
    router.use(fn);
}

SftCms.prototype.Field = {
    Types : require('./admin/fields/fieldTypes'),
    Type : keystone.Field
}

keystone.Field.prototype.getValueFromData = function(data) {
	data[this.path] = this.path in data ? data[this.path] : (this.options.default ? this.options.default() : (this._path ? this._path.get(data) : null));
    return data[this.path];
};

SftCms.prototype.Crm = require('./lib/crm');

SftCms.prototype.Enums = require('./lib/enums');

SftCms.prototype.start = require('./lib/core/start');

SftCms.prototype.utils = require('./lib/utils');

SftCms.prototype.Admin = require('./admin/admin-export');

var sft = module.exports = exports = new SftCms();


SftCms.prototype.Types = require('./lib/core/types')(sft);

sft.contextService = require('./lib/context');

sft.express = keystone.express;

sft.content = keystone.content;

sft.middleware = require('./lib/middleware/middelwares');

sft.httpsServer = keystone.httpsServer;

sft.Promise = require('bluebird');
mongoose.Promise = require('bluebird');

sft.set('mongoose', mongoose);

