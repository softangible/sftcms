SFT-CMS
===

## Fields
The fields are splitted into 2 parts:
* Type: in `admin/fields/types` each field has its own direcotry with a `<FieldName>Type.js` file. In this file we export the prototype for each field. The prototype must contain the following:
    * `addToSchema` : Controls the schema that the type will cerate for the field.
    * `validateInput` : Custom validation that will execute on each save or update of the field
    * `updateItem` : Method that executes on each save or update and allows to add custom logic before saving the field data.


* UI: in `admin/ui/app/sftCms.Fields` there are folders for each of the fields. Each folder can have up to 3 different angular.js directives:
    * __Column Directive__ : A directive that is used in data tables (i.e. entities page) and in `noedit` scenarios (i.e. field or entity have noedit flag) and specifies how to display the data of the field.
    * __Field Directive__ : A directive that handles the edit/create mode of the field. This directive handles how the user interacts with the field and how to use it.
    * __Filter Directive__ : A directive that handles the way a filter of the field works and what inputs it display (i.e. date range picker for date fields)


## Context
On every request a context middleware is being applied.
All the configuration of the context is in `lib/context` directory.
The middleware saves the request to the context and then enables the system to use this context for multiple scenarios (i.e multilingual)


## Multilingual
The multilinugual is splitted into 3 parts:
* __Field properties__ : in `lib/List/proto/add-properties.js` there is the part of the code that handles the creeation of the multilingual fields. In case of multiple languages and that there is no `langs=false` flag this method handles the multiply of the field and altering it's key for each of the languages. It also creates a virtual field with the original key to allow the developer to keep using the field as is and in the background handle returning the correct data to it.
* __Virtual Field__ : in `lib/List/proto/_contextVirtualGetter.js` there is the function that operates as the virtual field of the original key. The field gets the language from the context (or uses the default language if no language specified) and checks the data. If the data is empty (i.e. empty array, null, undefined) it will fallback to the default language. If the data is empty also using the default language it will return its data anyway.
* __Middleware__ : a middleware that all requests go through. This middleware is responsible to detect the language the user wishes to use (the middleware is located in `lib/middleware/langs.js`). This is the order of the checks done:
    * The `lang` query string parameter. If specified use the value specified
    * Tries to extract the language from the path. If language is used as path parameter it will always be the first part of the path.
    * Trying to get language from the region of the user using the user's IP. For this puropose we use the package `geoip-country-only`.

If after all tries the system couldn't find the language code it would fallback to the default language. If a language code have been found but the system does not include the language it will also fallback to the default language


## Revisions
If the developer specifies `revision=true` in the model revisions will be created for this model. All the handling of the revision is located in `lib/List/proto/revision.js`. The handling of revisions is splitted into 3 parts:
* __Revisions collection__ : Each model revision gets its own revision collection that keeps up to 10 revisions for each model entity (in case of inheritance only a single collection will be created for all models that inherit the same parent).  Each revision have the following fields:
    * `_rev_itemId` : The ID of the entity that is being revised
    * `_rev_date` : The date of the revision 
    * `_rev_operation` : The opertion that was done (C/R/U/D)
    * `_rev_changes` : Array of the changes made 
    * `_rev_user` : The field exists only if `user model` is specified to the system and in such case the ID of the user that made the changes would also be saved
* __Mongoose Hooks__ : The revisions useage are hooked to the following mongoose hooks:
    * __save__ : On each save of an entity the follwing is done:
        * Remove oldest revision if the amount of revision of the entity is at the maximum amount (10).
        * Create the new revision
        * If update only then the revision is created and throws an error to prevent the save of the original entity. If update and publish the revision is created and then continues to save the original entity as well.
    * __remove__ : On each deletion of an entity a revision is saved for the deleted entity.
    * __find / findOne__ : Used to get the revisioned data in case a `sft-rev` query string parameter is specified. Currently the ability to specify a specific revision has not been implemented and it always returns the latest revision. In these hooks we alter the query object to look for the revisions collection instead for the original collection.


## Scheduler
We use `node-schedule` to execute scheduled jobs. Each scheduled job function is executed in a try/catch context so it wont be able to crush the system. All execution of scheduled jobs are logged into the `scheduledjoblogs` collection and each document have the following fields:
* __jobName__ : The name of the job
* __isSuccess__ : boolean indicates of the job have done succussfully or not.
* __date__ : The date of the log
* __exception__ : A JSON notation of the exception object (in case of failure)


## Authentication
The authentication mechanisem is located in `lib/auth` and contains the following:
* __Auth Providers__ : There are 3 authentication providers supported by the system:
    * Facebook : uses `passport-facebook` startegy
    * Google : uses `passport-google` strategy
    * Local : a wrapper around Keystone's built in authentication mechanisem and uses email/password to authenticate
* __Password Reset__ : Handling the whole password reset process and contains two parts:
    * Create a token to be used for the password reset and saves it to the user
    * Revice new password and password confirmation, checks that the token is valid and updates the password for the user.
* __Email Confirmation__ : Handling the confiramtion of the user's email and contains two parts:
    * Create a token to be used for the confirmation and saves it to the user
    * A route handler that checks the token is valid and sets the `isConfirmed` field of the user to `true`.


## Utilities
The CMS comes with 3 builtin utilities in the `lib/utils` dir:
* __device__ : exposes 3 methods indicating the device type (isMobile, isTablet, isDesktop)
* __enumToSelectOptions__ : A method that takes an enum and returns a formatted object to use in the Select Field
* __isFirstWorkerThread__ : A method that indicated if the current first is the first working thread or not

These utils are merged with the built in keystone utils.


## ImagePicker (aka Elfinder)
The system uses Elfinder for it's file picker UI. Elfinder was selected for it's ability to use different storage drivers.
Currently the only driver supported by SFTCMS is S3.

The code for the backend API of Elfinder is located in `lib/core/imagePicker`. The frontend part of it is located in `admin/ui/external/elfinder`.


## Configuration
The configurations of the system is set at uptime. The system first loads the `default` configuration and then merges it with the enviorment specific configuration. 
The Handling of the load and the merge of the configurations is done in `lib/core/config/configLoader`


## Logger (`lib/core/logging`)
The system uses `winston` as its logger. We created a custom transport for winston to save the logs to the DB (the transporter is located in `dbLogger.js`).


## Bindings
This feature allows the user to bind a field to another field (of the same type and language) of an entity that is related to it. 

The bindigs are built using multiple parts:
* __binding map__ : a property called `_bindingMap` added to the entity at the model registry which holds a JSON representing the binding between each field and its value. The type of binding can be one of the following: 
    * Override : The value will be taken from the value on the entity.
    * Binds : an array of refrences to fields on related entities
    * Eval : a string of a function that gets as a parameters the document and the request object. The value of the field will be the value returned from the function.
* __findBinded__ : a static method added to the model which gets a critiria and callback (the callback is optional). The method executes a `find` query using the critiria specified and applies all the binds to it. The code of the function is in `lib/list/get-binded.js`


## Admin UI
The admin UI is built using angular.js based on a prebuilt theme ([https://themeforest.net/item/altair-admin-material-design-uikit-template/12190654](https://themeforest.net/item/altair-admin-material-design-uikit-template/12190654)).

In order to build the Admin UI do the following from the `admin/ui` directory:
* Make sure `gulp` is installed globally (`npm i -g gulp`)
* Make sure `bower` is installed globally (`npm i -g bower`)
* Install bower dependencies : `bower install`
* Install npm dependencies : `npm install`
* Run gulp : `gulp`

The Admin UI contains the following:
* __Localization__ : the `i18n` direcotry contains JSON language files with the texts translations of any hard coded text.
* __Fields__ : the `sftCms.Fields` directory contains all the fields UI directives (as specified above in the fields section).


## Core Componnents

### Email (`lib/core/Email.js`)
The email service bases on `nodemailer` and supports all the options and services supported out of the box using `nodemailer`. Also, it is being enhanced enabling support for SES and Sendgrid transporters.

To support email tempaltes the `email-templates` package is being used.

### Init (`lib/core/init.js`)
Inits the system, sets the default options and accepts options from the developer.
These are the following that handled by the init process:
* Sets the session ID generator to use uuid
* Sets the default render engine (default: `swig`)
* Sets the path for the admin UI (default: `admin` )
* Adds the `queryAsArray` middleware. This middleware is for normalization of the parsed query string parameters. Because express parses the query string to multiple data types (string / array), we've decided to expose also `queryAsArray` which normalizes all values to arrays.

### Start (`lib/core/start.js`)
Handles the bootstraping of the system. This file handles the clustering and the bootstrap of the express server.
Also it adds the langs middleware (mentioned above in the multilingual section) in case of multiple languages configured.