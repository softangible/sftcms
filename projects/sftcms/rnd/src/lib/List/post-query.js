var keystone = require('keystone');
var _ = require('underscore');
var contextService = require('../context');
var Promise = require('bluebird');
var mongoose = require('mongoose');

function postQuery(doc) {
    console.log(doc.schema.options.collection);
    doc.toObject();
    var a = mongoose;
}

function getList(query){
    
    return new Promise((resolve,reject)=>{
        var list = keystone.list(query.model.modelName);
        if (!list) return reject(new Error('No Results'));

        if (list.inherited && list.inherited){
            query._originalFields = _.clone(query._fields);
            query._fields = {__t : 1};
            query.innerIgnore = true;
            return query.exec().then(res=>{
                if (!res) return reject(new Error('No Results'));

                if (query.op.indexOf('findOne') != -1)
                    list = keystone.list(res.__t);
                else 
                    list =  keystone.list(res[0].__t);
                
                if (!list) return reject(new Error('No Results'));

                query._fields = query._originalFields;
                delete query.innerIgnore;
                return resolve(list);
            })
        } else {
            delete query.innerIgnore;
            resolve(list);
        }
    })
}

module.exports = postQuery;