var keystone = require('keystone'),
	_ = require('lodash'),
	contextService = require('../../context'),
	errorCodes = require('../../error-code'),
	revisionModelSuffix = '_revisions',
	maxAmount = 10;

function getRevisionCollectionName(collection, discriminator) {
	return 'b-' + collection + revisionModelSuffix;
}

function transformModel(originalModel, newModel) {
	doc = new newModel();
	for (var path in originalModel.schema.paths) {
		if (path == doc.schema.options.discriminatorKey || path == doc.schema.options.versionKey || path == '_id')
			continue;
		doc.set(path, originalModel.get(path));
	}

	return doc;
}

function clearOldestRevisionAndCreateNew(originalModel) {
	var revisionsModel = getRevisionModel(originalModel.__t, originalModel.schema);
	return revisionsModel.find({ '_rev_itemId': originalModel.id }).sort({ '_rev_date': 1 }).then(res => {
		if (res && res.length >= maxAmount) {
			return revisionsModel.remove({_id : res[0].get('_rev_id')});
		}
		return {};
	}).then(res => {
		var userModel = keystone.get('user model');
		
		var doc = transformModel(originalModel, revisionsModel);

		doc.set('_rev_date', Date.now());
		doc.set('_rev_itemId', originalModel.id);

		if (userModel && originalModel._req_user) {
			doc.set('_rev_user', originalModel._req_user);
		}

		return doc;
	})
}

function getRevisionModel(discriminator, schema) {
	var revisionsModel = keystone.mongoose.models[discriminator + revisionModelSuffix];
	if (!revisionsModel) {
		revisionsModel = keystone.mongoose.models[getRevisionCollectionName(
			schema.options.collection,
			schema.discriminatorMapping ? schema.discriminatorMapping.value : null
		)];
	}

	return revisionsModel;
}

function cloneCleanSchema(originalSchema) {
	var newSchema = new keystone.mongoose.Schema();
	var schema = Object.create(keystone.mongoose.Schema.prototype);
	_.merge(schema, originalSchema);
	schema.options = newSchema.options;
	schema.callQueue = newSchema.callQueue;
	if (newSchema.s && schema.s)
		schema.s.hooks = newSchema.s.hooks;
	return schema;
}

function createRevisionMongooseModel(list, userModel, collection) {
	var baseModel = keystone.mongoose.models[collection];

	var schema = cloneCleanSchema(list.schema);
	if (baseModel) {
		schema._originalDiscriminator = list.key
		return baseModel.discriminator(list.key + revisionModelSuffix, schema);
	}

	schema.add({
		_rev_itemId: { type: keystone.mongoose.Schema.Types.ObjectId, ref: collection, index: true },
		_rev_date: { type: Date, index: true, required: true },
		_rev_operation: { type: String, index: true, required: true },
		_rev_changes: { type: [String], index: true }
	})

	if (userModel) {
		schema.add({
			_rev_user: { type: keystone.mongoose.Schema.Types.ObjectId, ref: userModel }
		});
	}

	schema.statics.getLatestByItemIdQuery = function (itemId) {
		return this.findOne({ _rev_itemId: itemId }).sort({ '_rev_date': -1 });
	}

	schema.post('init', postInit);

	return keystone.mongoose.model(collection, schema);
}

function getRev() {
	if (!contextService.request)
		return null;

	return contextService.request.query['sft-rev'] || null;
}

function getNewModelFromExisting(model) {
	var discriminator = model[model.schema.options.discriminatorKey];
	if (discriminator) {
		var newModel = keystone.mongoose.models[discriminator];
		if (newModel.schema._originalDiscriminator) {
			return keystone.mongoose.models[newModel.schema._originalDiscriminator];
		}
	}
}

///////////////////////////////////////////// Hooks

function preSave(list, collectionName) {

	return function (next) {

		if (this.passedSaveHook || (this.$basePath && this.$basePath == 'data'))
			return next();

		this.passedSaveHook = true;
		this.__rev = (typeof this.__rev === 'number') ? this.__rev + 1 : 1;
		var self = this;

		clearOldestRevisionAndCreateNew(this).then(doc => {

			doc._rev_operation = this.isNew ? 'create' : 'update';
			doc._rev_changes = [];

			var listToUse,
				collectionParts = collectionName.split('_l-');

			listToUse = (collectionParts.length < 2) ? list : (keystone.list(collectionParts[1].replace(revisionModelSuffix, '')) || list);

			for (var path in listToUse.fields) {
				if (this.isModified(path)) {
					doc._rev_changes.push(path);
				}
			}

			if (listToUse.autokey) {
				if (this.isModified(listToUse.autokey.path)) {
					doc._rev_changes.push(listToUse.autokey.path);
				}
			}

			doc.save(function (err) {
				if (err) return next(err);

				self.set('rev_id',doc.id);
				if (contextService.request && contextService.request.body && contextService.request.body.action == 'update'){
					next(new Error(errorCodes.SavedRevision));
				}
				else{	
					next();
				}
			});
		})
	}
}

function createPreFind(list) {
	return function (next) {
		if (this.forceRegular) {
			return next();
		}
		var sftRev = getRev();
		if (!sftRev)
			return next();

		contextService.set('rev', sftRev);
		var revisionQuery = list.revisionsModel.find();

		this['_sft-rev'] = sftRev;
		this._collection = revisionQuery._collection;
		this.schema = revisionQuery.schema;
		this.model = revisionQuery.model;
		this.sort('-_rev_date');
		this.limit(1)
		next();
	}
}

function postInit(doc) {
    var newModel = getNewModelFromExisting(this);
    var transformed = transformModel(this, newModel);
    var originalId = this._rev_itemId;
    transformed.set('_rev_id',this.id,{strict : false});
    Object.assign(this, transformed);
    this._rev_itemId = originalId;
}

function getPreRemove(list) {
	return function (next) {
		clearOldestRevisionAndCreateNew(this).then(doc=>{
			doc._rev_operation = 'delete';
			doc.save(next);
		})
	}
}


/**
 * List revision option
 *
 * When enabled, it tracks changes to each document on save or remove.
 */
module.exports = function revisions(list) {

	var collection = list.schema.options.collection;
	var discriminator = list.get('inherits') ? list.key : null;

	var collectionName = getRevisionCollectionName(collection, discriminator);

	list.revisionsModel = createRevisionMongooseModel(list, keystone.get('user model'), collectionName);
	list.schema.add({
		__rev: Number,
		rev_id : String
	});

	//If model already exists for a '_revisions' in an inherited model, log a warning but skip creating the new model (inherited _revisions model will be used).
	if (list.get('inherits') && collectionName.indexOf(revisionModelSuffix, collectionName.length - revisionModelSuffix.length) !== -1 && keystone.mongoose.models[collectionName]) {
		// Already exists by parent
		console.log(list.key + ' being revised by parent : ' + list.options.inherits.key);
		return;
	} else {
		var prefind = createPreFind(list);

		list.schema.pre('save', preSave(list, collectionName));
		list.schema.pre('remove', getPreRemove(list));
		list.schema.pre('find', prefind);
		list.schema.pre('findOne', prefind);
	}
};