var keystone = require('keystone'),
    utils = require('keystone-utils'),
    _ = require('underscore'),
    _contextVirtualGetter = require('./_contextVirtualGetter');

module.exports = function addProperties(props) {
    var args = arguments;
    
    var isBindedModel = this.model.options.bind && this.model.options.bind.length;

    for (var i = 0, length = args.length; i < length; i++) {
        if (!_.isObject(args[i])) continue;
        
        for (var key in args[i]) {
            if(isBindedModel && args[i][key].bindable!==false)
                args[i][key].bindable = true;
            
            this.model.clearFields[key] = args[i][key];
            if(!this.model.clearFields[key].langs && this.model.clearFields[key].langs !== false){
                this.model.clearFields[key].langs =[];
            }
        }

        var langs = keystone.get('langs');
        if (langs && this.model.options.langs !== false) {
            finalArgs = {}
            Object.keys(args[i]).forEach( key => {
                var field = args[i][key];
                if (field.langs !== false) {
                    
                    if (!field.label)
                        field.label = utils.keyToLabel(key);
                    langs.forEach( lang => {
                        finalArgs[key + '.' + lang] = _.clone(field);
                        finalArgs[key + '.' + lang].lang = lang;
                    });
                    
                    this.model.clearFields[key].langs = langs;

                    this.schema.virtualToInclude.push(key);
                    this.schema.virtual(key).get(_contextVirtualGetter(key));

                } else {
                    finalArgs[key] = field;
                }
            }, this);
            args[i] = finalArgs;
        }

        for (var key in args[i]) {
            if (this.model.fields[key]) {
                this.model.fields[key].options = _.extend(this.model.fields[key].options, args[i][key]);
                delete args[i][key];
            }
        }
    }

    this.model.add.apply(this.model, args);
}