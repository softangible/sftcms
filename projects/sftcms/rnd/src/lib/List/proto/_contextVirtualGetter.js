var _ = require('underscore');
var contextService = require('../../context');

module.exports = function (key){
    return function(){
        var self = this;
        var lang = contextService.getLangContext();
        var ret = this.get(key + '.' + lang);
        
        if (Array.isArray(ret) || _.isObject(ret)){
            return _.isEmpty(ret) ? getDefault() : ret;
        }
        if (ret===false || ret ===0 || ret)
            return ret;

        return getDefault();

        function getDefault(){
            return self.get(key + '.' + keystone.get('langs')[0]);
        }
        
    }
}