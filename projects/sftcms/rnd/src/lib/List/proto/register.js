var contextService = require('../../context');
var keystone = require('keystone');
var _ = require('underscore');
var revision = require('./revision');

module.exports = function register() {

    addDefaultBindings(this);
        
    if(this.model.options && this.model.options.revision){
        revision(this.model);
    }


    this.model.register();

    this.schema.options.toObject = this.schema.options.toJSON = {
        transform: docToObject
    }
}


function addDefaultBindings(list) {
    if (list.bindingMap && !list.model.options.abstract) {
        for (var key in list.model.fields){
            var defaultBindOptions =  list.model.fields[key].options.defaultBind;
            if (defaultBindOptions && defaultBindOptions.length){
                var fieldLang = list.model.fields[key].options.lang;
                for (var i=0, length=defaultBindOptions.length; i<length; i++){
                    if (defaultBindOptions[i].lang && defaultBindOptions[i].lang != fieldLang)
                        continue;

                    if (defaultBindOptions[i].mode == 'binds' && defaultBindOptions[i].binds && defaultBindOptions[i].binds.length){
                        var binds = JSON.parse(JSON.stringify(defaultBindOptions[i]));
                        delete binds.lang;
                        for (var j =0, length = binds.binds.length; j<length; j++){
                            binds.binds[j].innerPath += '.' + fieldLang;
                        }
                        list.bindingMap[key] = binds;
                    }else
                        list.bindingMap[key] = defaultBindOptions[i]
                }
            }
        }

        if (list.model.fields._bindingMap){
            list.model.fields._bindingMap.options.default = JSONstringifyWithFuncs(list.bindingMap);
            list.schema.paths._bindingMap.defaultValue = list.model.fields._bindingMap.options.default;
        } else {
            var bindMapProp = {
                _bindingMap: { type: String, hidden: true, required: true, langs: false, default: JSONstringifyWithFuncs(list.bindingMap) }
            };
            list.addProperties(bindMapProp);
        }
        
    }
}

function JSONstringifyWithFuncs(obj) {
    Object.prototype.toJSON = function () {
        var sobj = {}, i;
        for (i in this)
            if (this.hasOwnProperty(i))
                sobj[i] = typeof this[i] == 'function' ?
                    this[i].toString() : this[i];

        return sobj;
    };
    Array.prototype.toJSON = function () {
        var sarr = [], i;
        for (i = 0; i < this.length; i++)
            sarr.push(typeof this[i] == 'function' ? this[i].toString() : this[i]);

        return sarr;
    };

    var str = JSON.stringify(obj);

    delete Object.prototype.toJSON;
    delete Array.prototype.toJSON;

    return str;
}

function docToObject(doc, ret, options) {
    var req = contextService.get('request');
    var adminRoot = '/' + keystone.get('admin path');
    if (!req || req.url.indexOf(adminRoot) == 0)
        return ret;

    if (doc.schema.virtualToInclude && doc.schema.virtualToInclude.length){
        for (var i = 0, length = doc.schema.virtualToInclude.length; i < length; i++) {
            ret[doc.schema.virtualToInclude[i]] = doc[doc.schema.virtualToInclude[i]];
        }
    }

    if (doc._populatedRelationships) {
        _.each(doc._populatedRelationships, function(on, key) {
            if (!on) return;
            ret[key] = doc[key];
        });
    }

    return ret;
}