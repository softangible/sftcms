var keystone = require('keystone');
var _ = require('underscore');
var contextService = require('../context');
var Promise = require('bluebird');

function preQuery(next) {
    if (this.innerIgnore) return next();

    var req = contextService.get('request');
    if (!req) return next();

    var lang = contextService.getLangContext();
    if (!lang) return next();

    var self = this;
    getList(this).then(list=>{
        console.log(self);
        next();
    }).catch(err=>{
        next();
    })
}

function getList(query){
    
    return new Promise((resolve,reject)=>{
        var list = keystone.list(query.model.modelName);
        if (!list) return reject(new Error('No Results'));

        if (list.inherited && list.inherited){
            query._originalFields = _.clone(query._fields);
            query._fields = {__t : 1};
            query.innerIgnore = true;
            return query.exec().then(res=>{
                if (!res) return reject(new Error('No Results'));

                if (query.op.indexOf('findOne') != -1)
                    list = keystone.list(res.__t);
                else 
                    list =  keystone.list(res[0].__t);
                
                if (!list) return reject(new Error('No Results'));

                query._fields = query._originalFields;
                delete query.innerIgnore;
                return resolve(list);
            })
        } else {
            delete query.innerIgnore;
            resolve(list);
        }
    })
}

module.exports = preQuery;