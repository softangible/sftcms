const getList = require('../core/dbUtils').getList;
const contextService = require('../context');
const _ = require('underscore');

function GetBinded(critiria, cb) {
    var list = getList(this.modelName);
    var binds = list.options.bind;
    var req = contextService.get('request');
    var query = this.find(critiria).populate(binds.join(' '));
    var resultBindMapping = mapResultBinding(req, list);

    if (cb) {
        query.exec((err, results) => {
            if (err) return cb(err);

            results = results.map(resultBindMapping);
            cb(err, results);
        })
    } else {
        return query.exec().then(results => {
            return results.map(resultBindMapping);
        })
    }
}

function mapResultBinding(req, list) {
    return function (result) {
        result.$__.strictMode = false;
        if (!result._bindingMap) return result;

        var bindingMap = JSON.parse(result._bindingMap)

        for (key in bindingMap) {
            var keyToUse = getKeyToUse(key,list);
            getKeyBindedValue(key, bindingMap[key], result, req);

            if (!result.get(key) && bindingMap[key].default) {
                getKeyBindedValue(key, bindingMap[key].default, result, req);
            }
        }

        return result;
    }
}

function getKeyToUse(key, list) {
    var listKeys = Object.keys(list.clearFields);
    var keyToUse;
    for (var i = 0, length = listKeys.length; i < length; i++) {
        if (key.indexOf(listKeys[i] + '.') != -1) {
            keyToUse = listKeys[i];
            break;
        }
    }
    if (!keyToUse) return key;
    return keyToUse;
}

function getKeyBindedValue(key, bind, doc, req) {
    var bindedVal;
    switch (bind.mode) {
        case 'binds':
            bindedVal = bindsMap(doc, bind.binds);
            break;
        case 'eval':
            bindedVal = evalFuncBind(doc, bind.evalFunc, req);
            break;
        case 'override':
            bindedVal = doc.get(key);
            break;
        default:
            bindedVal = null;
            break;
    }

    if(!bindedVal) return;

    var cast;
    if (_.isArray(bindedVal) && bindedVal.length > 1)
        cast = Array;
    else if (bindedVal.length == 1){
        bindedVal = bindedVal[0];
    }
    
    doc.set(key,bindedVal, cast);
}

function bindsMap(doc, binds) {
    var bindObjs = [];

    for (var i=0, length=binds.length; i<length; i++){
        var bind = binds[i];
        
        var innerPathParts = bind.innerPath.split('.');
        var innerPath = "['" + innerPathParts.join("']['") + "']";

        var path = "['";
        if (!bind.modelIndex && bind.modelIndex!==0){
            path += bind.fieldPath +"'].toBSON()" + innerPath;//bind.innerPath
        } else {
            path += bind.fieldPath + "'][" + bind.modelIndex + '].toBSON()' + innerPath;//bind.innerPath;
        }
         
        var val = undefined;
        try {
            // eval('var bindObj = doc.' + path.replace('.', '.toBSON()'));
            eval('var bindObj = doc' + path);
            val = bindObj;
        } catch (err) {}
        bindObjs = bindObjs.concat(val);
    }

    return bindObjs;
}

function evalFuncBind(doc, funcString, req) {
    try{
        eval('var evalFn = ' + funcString);
        return evalFn(doc, req);
    } catch(err){
        return undefined;
    }
    
}

module.exports = GetBinded;