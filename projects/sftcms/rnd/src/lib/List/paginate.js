function paginate(options, callback) {
	var list = this;
	var model = this.model;

	options = options || {};

	var query = model.find(options.filters);

	query._original_exec = query.exec;
	query._original_sort = query.sort;
	query._original_select = query.select;

	var currentPage = Number(options.page) || 1;
	var resultsPerPage = Number(options.perPage) || 50;
	var maxPages = Number(options.maxPages) || 10;
    var skipFactor = Number(options.skipFactor) || 0;
	var skip = (currentPage - 1) * resultsPerPage + skipFactor;
	list.pagination = { maxPages: maxPages };

	// as of mongoose 3.7.x, we need to defer sorting and field selection
	// until after the count has been executed

	query.select = function () {
		options.select = arguments[0];
		return query;
	};

	query.sort = function () {
		options.sort = arguments[0];
		return query;
	};

	query.exec = function (callback) {
		return new Promise((resolve,reject) => {
			query.count((err, count) => {
				
				if (err){
					if (callback) callback(err);
					return reject(err);
				}

				query.find().limit(resultsPerPage).skip(skip);

				// apply the select and sort options before calling exec
				if (options.select) {
					query._original_select(options.select);
				}

				if (options.sort) {
					query._original_sort(options.sort);
				}

				query._original_exec(function (err, results) {
					if (err){ 
						if (callback) callback(err);
						return reject(err);
					};
					var totalPages = Math.ceil(count / resultsPerPage);
					var rtn = {
						total: count,
						results: results,
						currentPage: currentPage,
						totalPages: totalPages,
						pages: [],
						previous: (currentPage > 1) ? (currentPage - 1) : false,
						next: (currentPage < totalPages) ? (currentPage + 1) : false,
						first: skip + 1,
						last: skip + results.length
					};
					list.getPages(rtn, maxPages);
					if (callback)
						callback(err, rtn);
					
					return resolve(rtn);
				});
			});
		})
	};

	return query;
}

module.exports = paginate;