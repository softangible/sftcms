var keystone = require('keystone'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    _contextVirtualGetter = require('./proto/_contextVirtualGetter'),
    GetBinded = require('./get-binded');

// preQueryHook = require('./pre-query'),
// postQueryHook = require('./post-query');

keystone.List.prototype.paginate = require('./paginate');

function Model(key, options) {
    if (options && options.inheritsSft) {
        options.inherits = require('../models/' + options.inheritsSft);
    }

    if (options && options.inherits) {
        options.inherits = _.clone(options.inherits.model);
        options.inherits.schemaFields = _.clone(options.inherits.schemaFields);
    }

    this.model = new keystone.List(key, options);

    applyInheritance(options, this);
    applyBindings(this);
}

function applyBindings(list) {
    if (list.model.options && list.model.options.bind && list.model.options.bind.length) {
       list.bindingMap = {};
       list.schema.statics.findBinded = GetBinded;
    }
}

function applyInheritance(options, list) {
    if (options && options.inherits) {
        list.model.clearFields = options.inherits.clearFields;
        var inheritedKey = options.inherits.model.modelName,
            inheritedList = keystone.list(inheritedKey);

        if (!inheritedList.inherited) inheritedList.inherited = [];
        inheritedList.inherited.push(list.model);
        list.schema.virtualToInclude = _.clone(options.inherits.schema.virtualToInclude);
        
        // Add super virtuals to the child inherits
        options.inherits.schema.virtualToInclude.forEach(function (name) {
            list.model.schema.virtuals[name] = new mongoose.VirtualType(undefined, name);
            list.model.schema.virtuals[name].get(_contextVirtualGetter(name));
            this[name] = list.model.schema.virtuals[name];
        }, list.model.schema.tree)

    } else {
        list.model.clearFields = {};
        list.schema.virtualToInclude = [];
    }
}

Model.prototype.addProperties = require('./proto/add-properties');
Model.prototype.register = require('./proto/register');
Model.prototype.relationship = require('./proto/relationship');

Object.defineProperty(Model.prototype, "defaultColumns", {
    get: function () {
        return this.model.defaultColumns;
    },
    set: function (val) {
        this.model.defaultColumns = val;
    }
});

// To allow access the schema through {list}.schema
Object.defineProperty(Model.prototype, "schema", {
    get: function () {
        return this.model.schema;
    }
});


Model.prototype.getSchema = function () {
    return this.schema;
}

Model.prototype.setDefaultColumns = function (defaultCols) {
    this.defaultColumns = defaultCols;
}

module.exports = Model;