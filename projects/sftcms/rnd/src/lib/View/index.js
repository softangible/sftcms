var keystone = require('keystone');

function View(req, res) {
    var preView =keystone.get('pre:view');
    this.view = new keystone.View(req, res);

    if (preView)
        this.on('init',function(next){
            preView(req,res,function(){
                next(); 
            }) 
        })
}

View.prototype.on = function(on,callback){
   this.view.on(on,callback);
}

View.prototype.render = function (renderFn, locals, callback) {
   this.view.render(renderFn, locals, callback);
}

module.exports = View;