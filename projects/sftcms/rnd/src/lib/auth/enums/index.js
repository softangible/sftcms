module.exports = {
    EmailConfirmation : require('./email-confirmation'),
    PasswordReset : require('./password-reset')
}