module.exports = {
    SUCCESS : 1,
    ALREADY_CONFIRMED : 2,
    TOKEN_EXPIRED : 3,
    ERROR : -1
}