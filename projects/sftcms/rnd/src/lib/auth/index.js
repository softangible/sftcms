module.exports = {
  Providers : {
    Facebook : require('./providers/facebook'),
    FacebookToken : require('./providers/facebook-token'),
    Google : require('./providers/google'),
    Local : require('./providers/local')
  },
  Routes : {
    EmailConfirmation : require('./routes/email-confirmation'),
    ResetPassword : require('./routes/reset-password')
  },
  EmailConfirmation :{
      GetNewToken : require('./email-confirmation/get-new-token'),
  },
  PasswordReset : {
    GetNewToken : require('./password-reset/get-new-token'),
    ResetPassword : require('./password-reset/reset-password')
  },
  Enums : require('./enums'),
  Signout : require('./signout')
}