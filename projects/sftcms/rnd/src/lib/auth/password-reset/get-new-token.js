var dbUtils =  require('../../core/dbUtils'),
    uuid = require('uuid/v4'),
    Router = require('../../router');

module.exports = function (email, cb){
    var userModel = dbUtils.getList('BaseUser').model;
        
    userModel.findOne({email : { $regex : new RegExp('^' + email + '$', "i") }}).exec(function (err, user) {
        if (err || !user) return cb(err);

        user.passwordResetToken = uuid();
        user.passwordResetExpires = Date.now() + 3600000;

        user.save(function (err) {
            if (err) return cb(err);
            return cb(err, user);
        })
    });
}