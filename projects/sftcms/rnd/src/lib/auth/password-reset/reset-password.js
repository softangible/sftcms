var dbUtils = require('../../core/dbUtils'),
    keystone = require('keystone');

module.exports = function (req, res, token, newPassword, confirmPassword, cb) {
    if (newPassword !== confirmPassword)
        return cb("Passwords not match");

    var userModel = dbUtils.getList('BaseUser').model;

    userModel.findOne({ 'passwordResetToken': token, }).exec(function (err, user) {
        if (err || !user || !user.passwordResetToken) return cb(err || "passwordResetToken", req, res);

        if (user.passwordResetExpires > Date.now()) {
            user.password = newPassword
            user.passwordResetToken = null;
            user.save(function (err) {
                if (err) return cb(err, req, res);

                return cb("", req, res);
            })
        }
        else {
            return cb("Token Expired", req, res);
        }
    });
}