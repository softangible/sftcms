var dbUtils =  require('../../core/dbUtils'),
    Router = require('../../router'),
    PasswordResetEnum = require('../enums').PasswordReset;

module.exports = function(cb){ 
    
    return function (req,res){
        var userModel = dbUtils.getList('BaseUser').model;
        
        userModel.findOne({passwordResetToken:req.query.guid}).exec(function (err, user) {
            if (err || (!user && !user.passwordResetToken)) return cb(PasswordResetEnum.ERROR, req,res);

            if (user.passwordResetExpires > Date.now())
                return cb(PasswordResetEnum.SUCCESS,req,res);
            
            return cb(PasswordResetEnum.TOKEN_EXPIRED,req,res);
        });
    }
}