var dbUtils =  require('../../core/dbUtils'),
    Router = require('../../router'),
    keystone = require('keystone'),
    EmailConfirmationEnums = require('../enums').EmailConfirmation;

module.exports = function(cb){ 
    
    return function (req,res){
        var userModel = dbUtils.getList('BaseUser').model;
        
        userModel.findOne({confirmationToken:req.query.guid}).exec(function (err, user) {
            if (err || !user) return returnErr();

            if (user.isConfirmed){
                keystone.session.signinWithUser(user,req,res,function(user){
                    cb(EmailConfirmationEnums.ALREADY_CONFIRMED,req,res);
                })
                return;
            } 
            
            if (user.confirmationExpires > Date.now())
            {
                user.isConfirmed = true;
                user.save(function (err) {
                    if (err) return returnErr();

                    keystone.session.signinWithUser(user,req,res,function(user){
                        cb(EmailConfirmationEnums.SUCCESS,req,res);
                    })
                })
            } else {
                return cb(EmailConfirmationEnums.TOKEN_EXPIRED,req,res);
            }
        });

        function returnErr(){
            cb(EmailConfirmationEnums.ERROR, req,res)
        }
    }
}