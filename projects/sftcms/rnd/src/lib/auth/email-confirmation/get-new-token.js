var dbUtils =  require('../../core/dbUtils'),
    uuid = require('uuid/v4'),
    Router = require('../../router');

module.exports = function (email, timeSpan, cb){
    var userModel = dbUtils.getList('BaseUser').model;
        
    userModel.findOne({email : { $regex : new RegExp('^' + email + '$', "i") }}).exec(function (err, user) {
        if (err || !user) return cb(err);

        user.confirmationToken = uuid();
        user.confirmationExpires = Date.now() + ( timeSpan || 86400000 );

        user.save(function (err) {
            if (err) return cb(err);
            return cb(err, user);
        })
    });
}