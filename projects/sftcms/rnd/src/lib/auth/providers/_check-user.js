var keystone = require('keystone'),
    dbUtils = require('../../core/dbUtils');

module.exports = function (req, profile, search, mapping, onUserExist, externalMapping, cb) {

    var userModel = dbUtils.getList(keystone.get('user model')).model;
    userModel.findOne(search).exec(function (err, user) {

        if (err) return cb(err)
        if (!user) return createUser(profile, cb);

        if (onUserExist) {
            return onUserExist(user, function (userToSave, isUpdated) {
                if (isUpdated)
                    return saveAndLogin(userToSave);

                req.user = userToSave;
                cb(err, userToSave);
            })
        }

        req.user = user;
        cb(err, user);
    });


    function saveAndLogin(userToSave) {
        userToSave.save(function (err) {
            if (err) return cb(err);
            req.user = userToSave;
            cb(err, userToSave);
        })
    }


    function createUser(profile, cb) {
        mapping(profile)

        user = new userModel(mapping(profile))

        if (externalMapping)
            return externalMapping(user, profile, saveAndLogin)

        saveAndLogin(user);
    }

}