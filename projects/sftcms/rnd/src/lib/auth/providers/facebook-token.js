var passport = require('passport'),
    FacebookTokenStrategy = require('passport-facebook-token'),
    AuthConfig = require('../../core/config/config').get('Auth'),
    CheckUser = require('./_check-user'),
    CreateRoutes = require('./_create-routes'),
    GetCallbackUrl = require('./_get-callback-url'),
    FacebookAuthConfig;

if (AuthConfig)
    FacebookAuthConfig = AuthConfig.Facebook,

/// Facebook scope options : https://developers.facebook.com/docs/facebook-login/permissions

module.exports = function (url, scope, externalMapping, onDone) {

    var callbackURL = GetCallbackUrl(url);
    var scopes = ['email', 'user_about_me'].concat(scope || []);

    passport.use(new FacebookTokenStrategy({
        clientID: FacebookAuthConfig.APP_ID,
        clientSecret: FacebookAuthConfig.APP_SECRET,
        // callbackURL: callbackURL.url,
        profileFields: ['id', 'displayName', 'email'],
        passReqToCallback: true
    },
        function (req, accessToken, refreshToken, profile, cb) {
            var profile_id = profile._json.id || '';
            var profile_email = profile._json.email || '';
            var profile_name = profile._json.name || '';

            var search = {
                $or: [
                    {facebook_id: profile_id}, 
                    {email: profile_email}
                ]
            }

            function onUserExist(user, cb) {
                if (user.facebook_id != profile_id || user.email != profile_email) {
                   user.facebook_id = profile_id;
                    user.email = profile_email
                    cb(user, true);
                } else
                    cb(user, false);
            }

            function mapping(profile) {
                return {
                    facebook_id: profile_id,
                    name: profile_name,
                    email: profile_email,
                    isActive: true,
                    isConfirmed: true,
                    isAdmin: false
                }
            }


            CheckUser(req,profile, search, mapping, onUserExist, externalMapping, cb);

        }));


    CreateRoutes('facebook-token', scopes, url, callbackURL.route, null, onDone);

}