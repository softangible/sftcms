var passport = require('passport'),
    url = require('url'),
    qs = require('querystring'),
    Router = require('../../router'),
    keystone = require('keystone'),
    BaseUrl = require('../../core/config/config').get('Base_URL');



module.exports = function (authType, scope, authUrl, callbackURL, paramType, onDone) {

    function Login(user, req, res, cb) {
        keystone.session.signinWithUser(user, req, res, cb);
    }

    function onUserLoggedIn(req,res,onDone){
        if (req.user)
            Login(req.user, req, res, function (user) {
                onDone(req, res)
            });
        else
            onDone(req, res)
    }

    function authenticate(authType, callbackUrl, scope, state, req, res, next) {
        console.log('cb url: ' + callbackUrl);
        var passportAuth = {
            scope: scope,
            state: state,
            session: false
        }
        if (callbackUrl)
            passportAuth.callbackURL = callbackUrl;
        
        passport.authenticate(authType, passportAuth)(req, res, function(err){
            if(err) return next(err);
            if (callbackUrl) return next();
            onUserLoggedIn(req,res,onDone);
        })
    }


    var mappers = {
        state: {
            callbackRoute: callbackURL,
            callbackUrl: function (param) {
                return url.resolve(BaseUrl, callbackURL);
            },
            retriveData: function (req) {
                return req.query.state;
            }
        },
        query: {
            callbackRoute: callbackURL,
            callbackUrl: function (param) {
                var urlObj = {
                    pathname: callbackURL
                }
                if (param) {
                    urlObj.query = qs.parse(param);
                }
                return url.resolve(BaseUrl, url.format(urlObj));
            },
            retriveData: function (req) {
                return url.parse(req.originalUrl).query;
            }
        },
        path: {
            callbackRoute: callbackURL + '/:param*',
            callbackUrl: function (param) {
                return url.resolve(url.resolve(BaseUrl, callbackURL + '/'), param ? encodeURIComponent(param) : 'sft_empty=true')
            },
            retriveData: function (req) {
                return req.params.param;
            }
        }
    }

    var mapper = mappers[paramType];

    function getUrl(param) {
        return mapper ? mapper.callbackUrl(param) : null;
    }

    function routes(app) {
        if(mapper && mapper.callbackRoute){
            app.get(mapper.callbackRoute, function (req, res, next) {
                var param = mapper.retriveData(req);
                authenticate(authType, getUrl(req.params.param), scope, param, req, res, next);
            }, function (req, res) {
                req.originalParams = qs.parse(mapper.retriveData(req));
                onUserLoggedIn(req,res,onDone);
            });
        }
        app.get(authUrl, function (req, res, next) {
            var param = url.parse(req.originalUrl).query;
            authenticate(authType, getUrl(param), scope, param, req, res, next);
        })
    }

    Router.setRoutes(routes);
}