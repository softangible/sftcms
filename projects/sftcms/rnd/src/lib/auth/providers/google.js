var passport = require('passport'),
    GoogleStrategy = require('passport-google-oauth20').Strategy,
    AuthConfig = require('../../core/config/config').get('Auth'),
    CheckUser = require('./_check-user'),
    CreateRoutes = require('./_create-routes'),
    GetCallbackUrl = require('./_get-callback-url'),
    GoogleAuthConfig;

if(AuthConfig)
   GoogleAuthConfig = AuthConfig.Google;

module.exports = function(url, scope, externalMapping, onDone) {

    var callbackURL = GetCallbackUrl(url);
    var scopes = ['profile', 'email'].concat(scope || []);

    passport.use(new GoogleStrategy({
            clientID: GoogleAuthConfig.APP_ID,
            clientSecret: GoogleAuthConfig.APP_SECRET,
            callbackURL: callbackURL.url,
            passReqToCallback : true
        },
        function(req,accessToken, refreshToken, profile, cb) {

            var search = {
                $or: [{
                    google_id: profile.id
                }, {
                    email: profile.emails[0].value
                }]
            }

            function onUserExist(user,cb){
                if (user.google_id != profile.id || user.email != profile.emails[0].value) {
                    user.google_id = profile.id;
                    user.email = profile.emails[0].value
                    cb(user, true);
                } else
                    cb (user, false);
            }

            function mapping(profile){
                return {
                        google_id: profile.id,
                        name: profile.displayName,
                        email: profile.emails[0].value,
                        isActive: true,
                        isConfirmed: true,
                        isAdmin: false
                    }
            }


            CheckUser(req,profile, search, mapping, onUserExist, externalMapping, cb);
        }));

    CreateRoutes('google',scopes,url,callbackURL.route,'state',onDone);
}