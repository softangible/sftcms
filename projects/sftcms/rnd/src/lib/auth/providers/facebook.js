var passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy,
    AuthConfig = require('../../core/config/config').get('Auth'),
    CheckUser = require('./_check-user'),
    CreateRoutes = require('./_create-routes'),
    GetCallbackUrl = require('./_get-callback-url'),
    FacebookAuthConfig;

if (AuthConfig)
    FacebookAuthConfig = AuthConfig.Facebook,

/// Facebook scope options : https://developers.facebook.com/docs/facebook-login/permissions

module.exports = function (url, scope, externalMapping, onDone) {

    var callbackURL = GetCallbackUrl(url);
    var scopes = ['email', 'user_about_me'].concat(scope || []);

    passport.use(new FacebookStrategy({
        clientID: FacebookAuthConfig.APP_ID,
        clientSecret: FacebookAuthConfig.APP_SECRET,
        // callbackURL: callbackURL.url,
        profileFields: ['id', 'displayName', 'emails'],
        passReqToCallback: true
    },
        function (req, accessToken, refreshToken, profile, cb) {

            var search = {
                $or: [{
                    facebook_id: profile.id
                }, {
                        email: profile.emails[0].value
                    }]
            }

            function onUserExist(user, cb) {
                if (user.facebook_id != profile.id || user.email != profile.emails[0].value) {
                   user.facebook_id = profile.id;
                    user.email = profile.emails[0].value
                    cb(user, true);
                } else
                    cb(user, false);
            }

            function mapping(profile) {
                return {
                    facebook_id: profile.id,
                    name: profile.displayName,
                    email: profile.emails[0].value,
                    isActive: true,
                    isConfirmed: true,
                    isAdmin: false
                }
            }


            CheckUser(req,profile, search, mapping, onUserExist, externalMapping, cb);

        }));


    CreateRoutes('facebook', scopes, url, callbackURL.route, 'path', onDone);

}