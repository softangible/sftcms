var URL = require('url'),
    BaseUrl = require('../../core/config/config').get('Base_URL');

module.exports=function(url){
   var lastChar = url.substr(-1);
   return {
       route : (lastChar == '/' || lastChar == '\\') ? URL.resolve(url, 'callback') : URL.resolve(url + '/', 'callback'),
       url : (lastChar == '/' || lastChar == '\\') ? URL.resolve(URL.resolve(BaseUrl,url),'callback') : URL.resolve(URL.resolve(BaseUrl,url)+'/','callback')
   };

}