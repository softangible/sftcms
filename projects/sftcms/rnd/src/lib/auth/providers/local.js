var keystone = require('keystone'),
    dbUtils = require('../../core/dbUtils'),
    uuid = require('uuid/v4'),
    Signout = require('../signout');

function login(req,res, email, password, extraValidation, cb){
    function onSuccess(user){
        if (user.isActive && (extraValidation ? extraValidation(user) : true)) cb();
        else {
            Signout(req,res,function(){
                cb('User not valid');
            })
        }
    }

    function onFail(err){
        cb(err);
    }
    
    keystone.session.signin({email:email, password:password},req,res,onSuccess,onFail)
}


function register(userData, cb){
    var UserModel = dbUtils.getList(keystone.get('user model')).model;
    
    var user = new UserModel(userData),
        now = new Date();
    
    user.confirmationToken = uuid();
    user.confirmationExpires = new Date(now.setTime(now.getTime() + 1 * 86400000 ));
    user.isActive = true;
    user.isConfirmed = false;

    user.save(function(err){
        cb(err,err ? null : user);
    });
}

module.exports = {
    login : login,
    register : register
}