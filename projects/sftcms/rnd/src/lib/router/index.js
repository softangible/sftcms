var keystone = require('keystone');
var contextService = require('../context');

function _loadExceptionHandler(app) {
    app.use(function (err, req, res, next) {
        console.log(err);
        res.err(err, req);
    });
}

var adminRoutes = [];

module.exports = {
    setAdminRoutes: function (fn) {
        keystone.get('app').use(contextService.middleware);
        adminRoutes.push(fn);
    },
    addAdminRoutes: function (app) {
        adminRoutes.forEach(function (fn) {
            fn(app);
        });
    },
    setAppUse: function (fn) {
        keystone.get('app').use(fn);
    },
    setRoutes: function (fn) {
        var routs = keystone.get('sft-routs') || [];
        routs.push(fn)
        keystone.set('sft-routs', routs)
    },
    setMiddleware: function (url, middleware) {
        keystone.get('app').all(url, middleware);
    },
    setStatics: function (url, path) {
        if (url)
            keystone.get('app').use(url, keystone.express.static(path));
        else
            keystone.get('app').use(keystone.express.static(path));
    },
    loadRoutes: function () {

        var routs = keystone.get('sft-routs') || [];
        routs.forEach(function (fn) {
            fn(keystone.get('app'))
        })

        var publicRoutes = keystone.get('public-routes');
        if (publicRoutes) {
            publicRoutes(keystone.get('app'));
        }

        _loadExceptionHandler(keystone.get('app'));
    }
}