
function get(key){
        try{
            return JSON.parse(process.env[key]);
        }catch(err){
            return process.env[key];
        }
    } 

function isPluginActive(pluginName){
    var pluginsConfig = get("Plugins");
    
    return (pluginsConfig && pluginsConfig[pluginName] && pluginsConfig[pluginName].active===false) ? false : true;
}

function getPluginConfig(pluginName){
    var pluginsConfig = get("Plugins");
    
    if (pluginsConfig)
        return pluginsConfig[pluginName];
    
    return undefined;
}

module.exports = {
    get :get,
    isPluginActive : isPluginActive,
    getPluginConfig : getPluginConfig
}