var path = require('path'),
    _ = require('lodash');


function formatter(objValue, srcValue) {
    if (!_.isString(srcValue) && !_.isNumber(srcValue)){
        return JSON.stringify(srcValue);
    }
    return srcValue;
}

module.exports = function(moduleRootPath){
    var configsPath = path.join(moduleRootPath,'./configs');
    require('dotenv').config({path: path.join(moduleRootPath,'.env')});
    if (process.env.SFT_ENV){
        var defaultConfig = require(path.join(configsPath,'./default.json'));
        var config = require(path.join(configsPath,'./'+process.env.SFT_ENV+'.json'))
        _.assignInWith(process.env,defaultConfig,formatter);
        _.assignInWith(process.env,config,formatter);
    }
}
