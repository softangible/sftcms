var keystone = require('keystone');

function set(key, obj){
    keystone.set(key,obj);        
}

function get(key){
    return keystone.get(key);        
}

module.exports = {
    set : set,
    get : get
};