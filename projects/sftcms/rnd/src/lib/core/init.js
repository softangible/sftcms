var swig = require('swig'),
    _ = require('lodash'),
	keystone = require('keystone'),
	uuidv4 = require('uuid/v4');
    router = require('../router'),
	hook = require('./hook'),
    loadBodyParser = require('../middleware/load-body-parser');
var passport = require('passport');

function init (options) {
    
    swig.setDefaults({ cache: false });
    
	var types = require('../../admin/fields/fieldTypes');
	keystone.Field.Types = types;
	
	var sessionOptions = {
		genid: function(req) {
			return uuidv4() // use UUIDs for session IDs
		  }
	}

    var defaultOptions = {
    'less': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'swig',
	'custom engine': swig.renderFile,
	'headless': true,
	'auto update': true,
	'session': true,
	'auth': true,
	'session options': sessionOptions,
	'user model': 'User',
	'wysiwyg images' :true,
	'wysiwyg menubar': true,
	'wysiwyg skin': 'lightgray',
	'wysiwyg cloudinary images' : true,
    'wysiwyg additional buttons' : 'alignjustify | forecolor | fontsizeselect',
	'wysiwyg additional plugins': 'table, advlist, anchor,'
		 + ' autolink, autosave, charmap, contextmenu, '
		 + ' directionality, emoticons, hr, media, pagebreak,'
		 + ' paste, preview, print, searchreplace, textcolor,'
		 + ' visualblocks, visualchars, wordcount, colorpicker'
    }
	
	this.set('admin path',options['admin path'] || 'admin');
    
    if(options)
        _.extend(defaultOptions, options);
    
	tinyMCEConfig = this.config.get('TinyMCE');
    if(tinyMCEConfig)
        this.set('wysiwyg-extend',tinyMCEConfig);
	
	var multilingualConfig = this.config.getPluginConfig('Multilingual');
    if(multilingualConfig && multilingualConfig.languages)
        this.set('langs',multilingualConfig.languages);

	keystone.init(defaultOptions);
    
    loadBodyParser();
    router.setMiddleware('/api*',keystone.middleware.cors);
	
	
	hook.pre('routes',this.middleware.queryAsArray);

	router.setAdminRoutes(function(app){
		app.all('/keystone*', function(req,res,next){
			keystone.get('express session')(req,res,function(){
				keystone.get('session options').cookieParser(req,res,function(){
					keystone.session.persist(req,res,next); 
				});
			});
		});
		app.use(passport.initialize());
	});
}

module.exports = init;