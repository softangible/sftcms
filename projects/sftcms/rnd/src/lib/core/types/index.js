var fieldTypesFiles = [];

module.exports = function(self) {
    return {
        importTypes : function(dirPath){ 
            fieldTypesFiles = fieldTypesFiles.concat(require('./import')(self,dirPath));
        },
        getFieldTypesFiles : function(){ return fieldTypesFiles }
    }
    
}