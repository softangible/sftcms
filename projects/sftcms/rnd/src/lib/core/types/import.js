var path = require('path'),
    fs = require('fs'),
    glob = require('glob');
    
module.exports = function(self,dirPath, cb){
    var fullDirPath = path.join(self.get('module root'), dirPath);
    var result = fs.readdirSync(fullDirPath);

    result.forEach(function(fieldName){
        var typeFullPath = path.join(fullDirPath, './'+fieldName+'/Type');
        addType(self,fieldName, typeFullPath);
    })

    return glob.sync(path.join(fullDirPath + '/**/field/*.js'));
}

function addType(self, name, typeFullPath){
    Object.defineProperty(self.Field.Types, name, { get: function () { return require(typeFullPath); } });
}