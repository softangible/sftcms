var nodemailer = require('nodemailer'),
    sgTransport = require('nodemailer-sendgrid-transport'),
    sesTransport = require('nodemailer-ses-transport'),
    EmailTemplate = require('email-templates').EmailTemplate,
    _ = require('lodash'),
    path = require('path'),
    config = require('./config/config'),
    keystone = require('keystone'),
    transporter =null,
    templatesDirPath =null;

function checkTransporter() {
    if (!transporter)
    {
        console.log('Can not send emails before setMailer is used');
        return false;
    }
    return true;
}

function setMailer (options, defaults){
    if (!options)
        throw new Error("Please provide email service provider configuration");
    
    var transportConfig;
    var serviceName;
    
    if (options.service){
        serviceName = options.service.toLowerCase();
    }
    
    if (serviceName && serviceName == 'sendgrid' && options.auth && options.auth.api_key){
        transportConfig = sgTransport(options);
    } else if(serviceName == 'ses'){
        transportConfig = sesTransport(options);
    } else {
        transportConfig = options;
    }

    transporter = nodemailer.createTransport(transportConfig,defaults);
    
    if (options.templateDir) 
        templatesDirPath = path.join(keystone.get('module root'), options.templateDir); 

    
    transporter.verify(function(error, success) {
        if (error)
           console.log('Error connecting SMTP server: ' + error);
    });
}

function _sendEmail(message,cb){
    if (!config.isPluginActive("Email"))
        return cb({err:"Email plugin is not active"});
    
    var conf =  config.getPluginConfig("Email");
    if (conf.overrideEmail)
        message.to = conf.overrideEmail;
        
    transporter.sendMail(message, cb);
}

function send(message, cb) {
    if (!checkTransporter()) return;
    
    _sendEmail(message,cb);
}

function sendWithTemplate(templateName, data, message, cb) {
    if (!checkTransporter()) return;
    if (!templatesDirPath) {
        console.log('No templates dir was set! The Email will not be sent');
        return;
    }
    
   new EmailTemplate(path.join(templatesDirPath,templateName)).render(data, function (err, result) {
        if (err) {
            console.log(err);
            return;
        }
        
        var ext = {}
        if (result.html) ext.html = result.html;
        if (result.subject) ext.subject = result.subject;
        if (result.text) ext.text = result.text;
        
        _.extend(message, ext);
        
        _sendEmail(message,cb);
    });
}


module.exports = {
    send : send,
    setMailer : setMailer,
    sendWithTemplate : sendWithTemplate
}; 