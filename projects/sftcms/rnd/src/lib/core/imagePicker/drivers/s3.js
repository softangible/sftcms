var knox= require('knox-s3'),
    mimes = require('../mimes'),
    rootDirName,tmbUrl, client,rootPath, volumeName, options, cachePeriod,
    path = require('path'),
    url =  require('url'),
    sftMediaConfig = require('../../config/config').get('SftMedia'),
    async = require('async');

function Driver (opts, mediaOpts){
    options = opts;

    if (mediaOpts && mediaOpts.ImageHandler && mediaOpts.ImageHandler.cachePeriod)
        cachePeriod = mediaOpts.ImageHandler.cachePeriod;

    client = knox.createClient(opts.driver.config);
    rootPath = opts.rootPath;
    rootDirName = opts.rootDirName;
    tmbUrl = opts.options.tmbUrl;
    volumeName = path.parse(rootPath).dir;
    this.sep = '/';
}

Driver.prototype.open = function(opts, cb){
    _listItems(opts.args.target || rootPath, function(err, data){
        var cwd = data[0];
        if (opts.args.init){
            cwd = data.splice(0,1)[0];
            
            data = data.filter(function (result) {
                return result.mime!='directory';
            })
            data.splice(0, 0,_getParents(rootPath)[0]);
        }
        else{
            data = data.filter(function (result) {
                return result.mime!='directory';
            })
            data.splice(0, 0,cwd);
        }   
        
        cb(err, data,cwd);
    });
}

Driver.prototype.ls = function(opts, cb){
    _listItems(opts.args.target, function(err, data){
       cb(err,_intersectLs(opts.args.intersect,data));
    });
}

Driver.prototype.tree = function(opts, cb){
    _listDirs(opts.args.target,function(err,data){
        cb(err, data);    
    })
}

Driver.prototype.info = function(opts, cb){
    cb();
}

Driver.prototype.parents = function(opts, cb){
    var parents = _getParents(opts.args.target)
    var promises = parents.map(function(parent){
        return function(cb){
            _listDirs(parent.hash,cb)
        }
    })
    // promises.splice(promises.length-1,1);
    
    
    
    var finalResults = [];
    
    async.series(promises, function(err, results) {	
        
        results.forEach(function (result) {
            finalResults.push(result.splice(0,1)[0]);
        })
        
        var finalResultsHashes = finalResults.map(function (dir) {
            return dir.hash;
        })
            
        for (var i = results.length-1; i >= 0; --i) {
            
            if (parents.length < 3)
                results[i] = results[i].filter(function (dir) {
                    return dir.hash != opts.args.target
                })
               
                results[i] = results[i].filter(function (dir) {
                    return finalResultsHashes.indexOf(dir.hash) == -1;
                })
            
            finalResults = finalResults.concat(results[i]);   
        }
        
        cb(err, finalResults);
    });
    
}

Driver.prototype.search = function(opts, cb){
    cb();
}

Driver.prototype.rm = function(opts, cb){
    var targets = opts.args.targets.map(function (target) {
        var path = (target.substr(-1) != '/') ? target : target.slice(0,-1);
        
        return _remove.bind(null,path)
    });
    
    async.parallel(targets, function(err, res) {
        cb(err, res);
    });
    
}

Driver.prototype.upload = function(opts, cb){
    var files =  opts.files.length ? opts.files : [opts.files];
    var uploads = files.map(function(file){
        return _upload.bind(null,file, opts.args)
    })
    
    async.parallel(uploads, function(err, res) {
        cb(err, res);
    });
    
}

Driver.prototype.mkdir = function(opts, cb){
    var buffer = new Buffer('');
    var headers = {'Content-Type': 'text/plain'};
    
    client.putBuffer(buffer, opts.args.target+opts.args.name, headers, function(err, res){
        if (!res || err){
            return cb(err);
        }
        res.resume();
        
        var fileData = formatObjects([{
                Key : opts.args.target+opts.args.name,
                Size : 0,
                LastModified : new Date()
        }], opts.args.target)
        
        cb(err,fileData);
    });
}

function _getParents(path){
    var pathParts = path.replace(rootPath,"").split('/').filter(function(part){
        return (!part)==false
    });
    
    pathParts.splice(0,0,"")
    
    var startPath = rootPath,
        result = [];
    
    pathParts.forEach(function(part){
        
        result.push(formatObjects([{
                Key : startPath+part,
                Size : 0,
                LastModified : new Date()
        }],rootPath)[0]);
            
        if (part) startPath += part+'/';
    })
    
    return result;
    /**
     
     */
    
    
}

function _remove(path, cb){
    client.deleteFile(path, function(err, res)
    { 
        if (res)
            res.resume();
        else
            return cb(err);
            
        cb(err,path); 
    });
}

function _upload(file, args, cb){
    var headers = {};
    if (cachePeriod){
        var cacheHeaderValue = 'public, max-age='+cachePeriod;
        headers['x-amz-meta-Cache-Control'] = cacheHeaderValue;
        headers['Cache-Control'] = cacheHeaderValue;
    }
    
    client.putFile(file.path, args.target + file.originalname, headers, function(err, res) {
			if (err) return cb(err);
			if (res) {
				if (res.statusCode !== 200) {
					return cb('Amazon returned Http Code: ' + res.statusCode);
				} else {
					res.resume();
				}
			}

			var fileData = formatObjects([{
                Key : args.target + file.originalname,
                Size : file.size,
                LastModified : new Date()
            }], args.target)[0]

			cb(null, fileData);
    });
}

function _listDirs(prefix,cb){
    _list(prefix,function(err,data){
        var dirsResults;
        if (data.CommonPrefixes)
            dirsResults = formatObjects(data.CommonPrefixes.map(function(cPrefix){
                return {
                    Key : cPrefix.Prefix.slice(0,-1),
                    Size : 0,
                    LastModified : new Date()
                }
            }), prefix);
        else{
            dirsResults = [];
        }
        
        var dirNames = dirsResults.map(function(dir){
            return dir.name;
        })
        
        var results = formatObjects(data.Contents, prefix).filter(function (result) {
            
            return result.mime=='directory' && dirNames.indexOf(result.name)==-1;
        });
        
        dirsResults = dirsResults.concat(results);
        
        if (!dirsResults[0] || dirsResults[0].hash != prefix){
            dirsResults.splice(0, 0,
                formatObjects([{
                    Key : prefix.slice(0,-1),
                    Size : 0,
                    LastModified : new Date()
                }])[0]
            );
        }
        
        
        
        cb(err, dirsResults);    
    })
}

function _listItems(prefix, cb){
    _list(prefix, function(err, data){
        var results = formatObjects(data.Contents, prefix);
        if (!results[0] || results[0].hash != prefix){
            results.splice(0, 0,
                formatObjects([{
                    Key : prefix.slice(0,-1),
                    Size : 0,
                    LastModified : new Date()
                }],prefix)[0]
            );
        }
        cb(err, results);
    });
}

function _listRecursive(prefix, data, cb){
    var search = { prefix: prefix, delimiter:'/'};
    if (data && data.IsTruncated){
        search.marker=data.Contents[data.Contents.length-1].Key
    }
    
    client.list(search, function(err, data2){
            if (data && data2.CommonPrefixes && data.CommonPrefixes)
                data.CommonPrefixes = data.CommonPrefixes.concat(data2.CommonPrefixes) 
            
            if (data &&  data2.Contents && data.Contents)
                data.Contents = data.Contents.concat(data2.Contents) 
            
            data2.CommonPrefixes = data.CommonPrefixes;
            data2.Contents = data.Contents;
            
            if (data2.IsTruncated)
                _listRecursive(prefix,data2, cb);
            else 
                cb(err, data2);
    });
}

function _list(prefix,cb){
    if (prefix.substr(-1) != '/') prefix += '/';
    
    var data = {
        CommonPrefixes : [],
        Contents : []
    }
    
    _listRecursive(prefix,data, cb);
}

function _intersectLs(intersect, data) {
    
    var results = [],
        resultObj = {};
    
    intersect.forEach(function(fileName){
        results = results.concat(data.filter(function(dataItem){
            return fileName.toLowerCase() == dataItem.name.toLowerCase();
        }));
    })
    
    results.forEach(function(result) {
        resultObj[result.hash] = result.name;
    })
    
    return resultObj;
}

function _getPhash(hash){
    var dirname = path.parse(hash).dir;
    
    if (dirname.substr(-1) != '/') dirname += '/';
    
    return dirname;
}

function formatObjects(arr, volumeid){
    return arr.map(function(item, index){
        if (!volumeid){
            volumeid = _getPhash(item.Key);
        }
        var mime = mimes.getMimeTypeFromPath(item.Key, item.Size),
            fullKey = (item.Key.substr(-1) != '/') ? item.Key + '/' : item.Key;
            dirs = mime=='directory' ? 1 : 0,
            name = fullKey==rootPath ? rootDirName : item.Key.replace(volumeid,""),
            hash = (item.Key.substr(-1) != '/') ? item.Key + '/' : item.Key;
          
            result =  {
                hash : hash,
                url : url.resolve(options.options.url.replace(options.rootPath,''),item.Key),
                volumeid : volumeName,
                mime : mime,
                locked : 0,
                name :  name,
                size : item.Size,
                dirs : dirs,
                isowner : true,
                read : 1, write : 1,
                ts : item.LastModified.getTime(),
                uiCmdMap : [],
                disabled : [],
            };
            
            if (result.hash != volumeid)
                result.phash = volumeid;
            else
                result.csscls = 'elfinder=navbar-root-local'; 
            
            
            if (result.mime != 'directory'){
                result.tmb = item.Key.replace(rootPath,'');
                result.dim = '100X100'
                result.relativeUrl = result.tmb;
                result.cdnUrl = url.resolve(sftMediaConfig.imagePath || '', result.relativeUrl);
            }
            
            return result;
    })
}

module.exports = Driver;