var routs = require('./routes'),
    adminPath = '/' + require('keystone').get('admin path');

module.exports = function(app) {
    app.all(adminPath + '/imagePicker/api/*?',routs.elfinder);
    
};