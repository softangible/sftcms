var errors = require('./errors'),
    commands = require('./commands'),
    drivers = require('./drivers'),
    router = require('../../router'),
    hook = require('../hook'),
    path=require('path'),
    getterSetter=require('../getterSetter'),
    rootPath, baseUrl,
    adminPath = require('keystone').get('admin path');

var Elfinder = function Elfinder(opts,mediaOpts){
    opts.options.uiCmdMap = [];
    this.options = opts.options || {};
    rootPath = opts.rootPath;
    this.cache={};
    baseUrl = opts.options.url.replace(rootPath,'');
    if (drivers[opts.driver.name]){
        this.driver = new drivers[opts.driver.name](opts,mediaOpts);
        this.options.separator = this.driver.sep;    
    }
    else
        throw 'Driver "'+ opts.driver +'" does not exists';
    
    getterSetter.set('image-picker',this);
    
    hook.pre('routes', require('./routes').signout)
    router.setAdminRoutes(require('./routesLoader'));
    router.setStatics(adminPath + '/imagePicker/public',__dirname + path.sep + 'public');
}

Elfinder.prototype.command = function(opts, cb){
    if (commands[opts.args.cmd] && this[opts.args.cmd])
        this[opts.args.cmd](opts,function(err,result){
            if (opts.args.init && result){
                result.api = "2.1";
                result.netDrivers = [];
                result.uplMaxSize = 10485760;    
            }
            
            if (err)
                this.error(err,cb);
            else
               cb(null, result); 
        });
    else    
        this.error(errors.ERROR_UNKNOWN_CMD,cb); 
}

Elfinder.prototype.open = function(opts, cb) {
    var self = this;
    
    var cacheKey = 'open-' + opts.args.target;
    if (opts.args.init) cacheKey = 'init-' + cacheKey;
    if (opts.args.tree) cacheKey = 'tree-' + cacheKey;
    
    var cache = this.getCache(opts.args.sessionId, cacheKey);
    if (cache) return cb(null,cache);
        
    this.driver[opts.args.cmd](opts,function(err,result,cwd){
        var options = self.options;
        options.path = opts.args.target;
        
        var response = {
            cwd : cwd,
            files : result,
            options : self.options
        }
        
        self.setCache(opts.args.sessionId, cacheKey,response);
        cb(err, response)
    })
}

Elfinder.prototype.tree = function(opts, cb) {
    var self = this;
    
    var cacheKey = 'tree-' + opts.args.target,
        cache = this.getCache(opts.args.sessionId,cacheKey);
    if (cache) return cb(null,cache);
    
    this.driver[opts.args.cmd](opts,function(err,result){
        self.setCache(opts.args.sessionId, cacheKey,{tree : result});
        cb(err, {
            tree : result,
        })
    })
}

Elfinder.prototype.rm = function(opts, cb) {
    var self = this;
    var cacheKey = opts.args.targets[0];
    this.removeCache(path.parse(opts.args.targets[0]).dir+'/');
    
    this.driver[opts.args.cmd](opts,function(err,result){
        cb(err, {
            removed : result,
        })
    })
}

Elfinder.prototype.mkdir = function(opts, cb) {
    var self = this;
    
    this.removeCache(opts.args.target);
    
    this.driver[opts.args.cmd](opts,function(err,result){
        cb(err, {
            added  : result,
        })
    })
}

Elfinder.prototype.ls = function(opts, cb) {
    var self = this;
    this.driver[opts.args.cmd](opts,function(err,result){
        cb(err, {
            list : result,
        })
    })
}

Elfinder.prototype.upload = function(opts, cb) {
    this.removeCache(opts.args.target);
    this.driver[opts.args.cmd](opts,function(err,result){
        cb(err, {
            added : result,
        })
    })
}

Elfinder.prototype.parents = function(opts, cb) {
    var self = this,
        cacheKey = 'parents-' + opts.args.target,
        cache = this.getCache(opts.args.sessionId, cacheKey);
    
    if (cache) return cb(null,cache);
    
    this.driver[opts.args.cmd](opts,function(err,result){
       
       self.setCache(opts.args.sessionId, cacheKey,{tree:result});
       cb (err,{tree:result});
        
    });
}

Elfinder.prototype.file = function(opts, cb) {
    cb (null, {
        redirect : baseUrl + opts.args.target.slice(0,-1)
    })
}

Elfinder.prototype.removeCache =function (key){
    var self = this,
        cacheRemoves = ['init-open','tree-open','init-tree-open','tree-init-open','tree','parents','open'];
    
    cacheRemoves.forEach(function(removeKey){
        var removalKey = removeKey+'-'+key;
        for (var sessionId in self.cache)
        {
            if (self.cache[sessionId][removalKey])
                delete self.cache[sessionId][removalKey];
        }    
    })
    
}

Elfinder.prototype.getCache =function (sessionId, key){
    if (this.cache[sessionId])
        return this.cache[sessionId][key];
}

Elfinder.prototype.removeSessionCache =function (sessionId){
    if (this.cache[sessionId])
        delete this.cache[sessionId];
}

Elfinder.prototype.setCache =function (sessionId, key, data){
    if (!this.cache[sessionId])
        this.cache[sessionId] = {};
        
    return this.cache[sessionId][key] = data;
}

Elfinder.prototype.error = function(err, cb){
    
}

module.exports={
    set : function(options,mediaOptions){
        new Elfinder(options,mediaOptions);
    }
}