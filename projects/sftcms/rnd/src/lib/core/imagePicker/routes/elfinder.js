var getterSetter=require('../../getterSetter'),
    imagePicker = getterSetter.get('image-picker')

module.exports = function(req, res) {
    res.header('Access-Control-Allow-Credentials', true);
    if (req.method=='OPTIONS')
        return res.sendStatus(200);
        
    var args = req.body ? (req.body.cmd ? req.body : req.query) : req.query;
    args.sessionId = req.sessionID;
    
    var files = req.files ? req.files['upload[]'] : []
    
    imagePicker.command({args:args,files:files},function(err,result){
        if (!result.redirect)   
            res.json(result);
        else
            res.redirect(301, result.redirect)
    })
};