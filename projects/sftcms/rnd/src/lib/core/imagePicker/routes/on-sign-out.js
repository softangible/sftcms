var getterSetter = require('../../getterSetter');
var imagePicker = getterSetter.get('image-picker');

module.exports = function(req, res, next) {
    if (req.originalUrl.endsWith('/signout')){
        if (imagePicker)
            imagePicker.removeSessionCache(req.sessionID);
    }
    next();
};