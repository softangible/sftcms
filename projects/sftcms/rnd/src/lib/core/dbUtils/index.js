module.exports = {
    getList : require('./getList'),
    populateRelated : require('./populateRelated')
};