var keystone = require('keystone');

function populateRelated(docs,populateItems,cb){
    keystone.populateRelated(docs,populateItems,function(err) {
        cb(err, docs);
    });
}

module.exports = populateRelated;