var keystone = require('keystone');

function getList(listName){
    return keystone.list(listName);
}

module.exports = getList;