/* 
 * Copyright (C) Softangible Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Softangible Dev Team <dev@sft.io>
 */

var os = require('os'),
	im = require('imagemagick'),
	config = require('./config/config'),
	_ = require('underscore');

function ImageHandler() {
	this.options = {
		width: 0,
		height: 0,
		size: '',
		format: '',
		forceResizing: false
	};

	this.mimeType = '';
}

ImageHandler.prototype.setOptions = function (strOptions) {
	var parts = strOptions.trim().split(','), self = this;

	parts.forEach(function (el) {
		var keyVal = el.split('-');

		if (keyVal.length != 2)
			return;

		switch (keyVal[0]) {
			case 'w': //width
				self.options.width = parseInt(keyVal[1]);
				break;

			case "h": //height
				self.options.height = parseInt(keyVal[1]);
				break;

			case "s": //predefined size
				switch (keyVal[1]) {
					case "small":
						self.options.width = 100;
						self.options.height = 100;
						break;
					case "medium":
						self.options.width = 250;
						self.options.height = 250;
						break;
					case "large":
						self.options.width = 500;
						self.options.height = 500;
						break;
					case "original":
						self.options.originalSize = true;
						break;
				}
				break;

			case "f": //format
				self.options.format = keyVal[1];

			case "fr": //Force resizing
				self.options.forceResizing = true;
				break;

		}
	});

	if (isNaN(self.options.width) || isNaN(self.options.height) || (self.options.width == 0 && self.options.height == 0 && self.options.originalSize != true))
		throw "Width or height should be set";
};


ImageHandler.prototype._setFormat = function () {
	var format = this.options.format.toLowerCase();

	switch (format) {
		case "png":
		case "gif":
			this.mimeType = "image/" + format;
			return format;

		default:
			this.mimeType = "image/jpeg";
			return "jpg";
	}
};

ImageHandler.prototype._fixSize = function (orgWidth, orgHeight) {
	if (this.options.originalSize == true){
		this.options.height = orgHeight;
		this.options.width  = orgWidth;
		return;
	}

	if (this.options.width === 0) {
		this.options.width = parseInt(this.options.height / orgHeight * orgWidth);
	} else if (this.options.height === 0) {
		this.options.height = parseInt(this.options.width / orgWidth * orgHeight);
	}

	if (!this.options.forceResizing && (this.options.width > orgWidth || this.options.height > orgHeight)) {
		var newImageRation = this.options.width / this.options.height;

		if (this.options.width > orgWidth) {
			this.options.width = orgWidth;
			this.options.height = parseInt(this.options.width / newImageRation);
		} else {
			this.options.height = orgHeight;
			this.options.width = parseInt(this.options.height * newImageRation);
		}
	}
};

ImageHandler.prototype.generate = function (file, callback) {
	var self = this;

	im.identify(['-verbose', file], function (err, result) {

		if (err) {
			callback(err);
			return;
		}

		var features = parseIdentify(result);

		self.mimeType = features['mime type'];
		if (self.options.format == '')
			self.options.format = features.format;

		self._fixSize(features.width, features.height);

		var prefix = (os.platform() == 'win32') ? '^>^^' : '\>^';
		var resizeOptions = {
			quality : 1,
			srcPath: file,
			format: self._setFormat(),
			width: self.options.width,
			height: self.options.height + prefix,
			strip: true,
			interlace: "none",
			colorspace: "sRGB",
			dither: "none",
			customArgs: [
				'-define', 'jpeg:fancy-upsampling=off',
				'-define', 'jpeg:sampling-factor=2x2,1x1,1x1',
				'-filter', 'Triangle',
				'-define', 'filter:support=2',
				'-dither', 'None',
				'-gravity', 'center',
				'-unsharp', '0.25x0.25+8+0.065',
				'-interlace', 'none',
				'-colorspace', 'sRGB',
				'-crop', self.options.width + 'x' + self.options.height + '+0+0',
				'+repage'
			]
		}
		if (config.get("SftMedia").ImageHandler && config.get("SftMedia").ImageHandler.resizeOptions) {
			_.extend(resizeOptions, config.get("SftMedia").ImageHandler.resizeOptions);
		}
		im.resize(resizeOptions, function (err, stdout) {
			callback(err, stdout, self.mimeType);
		});
	});
};

function parseIdentify(input) {
	var lines = input.split("\n"),
		prop = {},
		props = [prop],
		prevIndent = 0,
		indents = [indent],
		currentLine, comps, indent, i;

	lines.shift(); //drop first line (Image: name.jpg)

	for (i in lines) {
		currentLine = lines[i];
		indent = currentLine.search(/\S/);
		if (indent >= 0) {
			comps = currentLine.split(': ');
			if (indent > prevIndent) indents.push(indent);
			while (indent < prevIndent && props.length) {
				indents.pop();
				prop = props.pop();
				prevIndent = indents[indents.length - 1];
			}
			if (comps.length < 2) {
				props.push(prop);
				prop[currentLine.split(':')[0].trim().toLowerCase()] = {};
			} else {
				prop[comps[0].trim().toLowerCase()] = comps[1].trim()
			}
			prevIndent = indent;
		}
	}

	prop.geometry = prop.geometry.split(/x/);
	prop.format = prop.format.match(/\S*/)[0]
	prop.width = parseInt(prop.geometry[0]);
	prop.height = parseInt(prop.geometry[1]);
	prop.depth = parseInt(prop.depth);
	if (prop.quality !== undefined) prop.quality = parseInt(prop.quality) / 100;


	return prop;
};

function generate(file, options, callback) {
	file = encodeURI(file);
	var imgHandler = new ImageHandler();
	try {
		imgHandler.setOptions(options);
		imgHandler.generate(file, callback);
	} catch (err) {
		callback(err);
	}
}

module.exports = {
	generate: generate
};