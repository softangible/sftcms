var path = require('path'),
    keystone = require('keystone'),
    coreModelsPath = (function(_rootPath) {
        var parts = _rootPath.split(path.sep);
        parts.pop();
        parts=parts.concat(['lib','models']);
        return parts.join(path.sep);
    })(module.parent ? module.parent.paths[0] : module.paths[0]);
    
function importer(dirname) {
    
    var coreModelsRelativePath = path.relative(this.get('module root'),coreModelsPath);
    
    keystone.import(coreModelsRelativePath);
    keystone.import(dirname);

    checkModelsRelations();
}

function checkModelsRelations(){
    var lists = Object.keys(keystone.lists);
    for (var i=0, length=lists.length; i<length; i++){
        var list = keystone.lists[lists[i]];
        if (list.options.bind){
            for (var z=0, length2=list.options.bind.length; z<length2;z++){
                var bind = list.options.bind[z];
                var refList = keystone.lists[list.fields[bind].options.ref];
                if (!refList.bindedLists)
                    refList.bindedLists = [];
                
                var found = false;
                for (var j=0, length3= refList.bindedLists.length; j<length3; j++){
                    var checkList = keystone.lists[refList.bindedLists[j].list];
                    if (!checkList.inherited || !checkList.inherited.length)
                        continue;
                    
                    var inheritedLists = checkList.inherited.map(function(l){return l.key});
                    if (inheritedLists.indexOf(lists[i]) != -1){
                        found = true;
                        break;
                    }
                }
                
                if (!found)
                    refList.bindedLists.push({
                        list : lists[i],
                        key : bind
                    });
            }
        }
    } 
}

module.exports = importer;