var keystone = require('keystone')

function pre(hookName, fn){
    keystone.pre(hookName, fn);
}

function preView(fn){
    keystone.set('pre:view', fn);
}

module.exports = {
    pre : pre,
    preView : preView
};