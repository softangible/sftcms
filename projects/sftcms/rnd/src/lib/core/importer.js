var keystone = require('keystone');

function importer(dirname) {
    return keystone.importer(dirname);
}

module.exports = importer;