var winston = require('winston'),
  Enums = require('../../enums'),
  winstonLogger;

module.exports = function () {
  require('./dbLogger');
  winstonLogger = new winston.Logger({
    level: 'info',
    transports: [
      new (winston.transports.DbLogger)()
    ]
  });

  var logger = {}

  Object.keys(Enums.Log.LogLevel).forEach(function(level){
    logger[level] = function (type, msg, meta) {
      log(level, type, msg, meta);
    }
  });

  return logger;
}

function log(level, type, msg, meta) {
  if (meta) {
    meta = {
      type: type,
      data: meta
    }
  }
  else {
    meta = { type: type }
  }

  winstonLogger[level](msg,meta);
}