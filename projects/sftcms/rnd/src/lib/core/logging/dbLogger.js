var util = require('util'),
    dbUtils = require('../dbUtils'),
    Enums = require('../../enums'),
    winston = require('winston');

var DbLogger = winston.transports.DbLogger = function (options) {
    //
    // Name this logger
    //
    this.name = 'dbLogger';

    //
    // Set the level from your options
    //
    this.level = options ? options.level || 'info' : 'info';

    //
    // Configure your storage backing as you see fit
    //
};

//
// Inherit from `winston.Transport` so you can take advantage
// of the base functionality and `.handleExceptions()`.
//
util.inherits(DbLogger, winston.Transport);

DbLogger.prototype.log = function (level, msg, meta, callback) {
    //
    // Store this message and metadata, maybe use some custom logic
    // then callback indicating success.
    //
    var Log = dbUtils.getList('Log').model
    var newLog = new Log({
        type: meta.type.id,
        message: msg,
        data: meta.data ? JSON.stringify(meta.data) : '',
        level: Enums.Log.LogLevel[level].id
    })

    newLog.save(function (err) {
        callback(err);
    });

};