var logger;

module.exports = {
    configure : require('./configure'),
    get logger(){
        if (!logger)
            logger = require('./configure')();
            
        return logger;
    }
}