var keystone = require('keystone'),
    cluster = require('cluster'),
    path = require('path'),
    Log = require('./logging'),
    admin = require('../../admin');

function start() {
    var sftModuleRoot = this.get('sft root')


    if (cluster.isMaster) {
        var clusterConf = this.config.getPluginConfig("Cluster");

        if (clusterConf && !clusterConf.active) {
            _appStart(this, this.router);
        } else {
            var workers = {};
            var numOfThreads = (clusterConf && clusterConf.numOfThreads) ? clusterConf.numOfThreads : require('os').cpus().length;
            // Fork workers.
            for (var i = 0; i < numOfThreads; i++) {
                var worker = cluster.fork({ ThreadIndex: i });
                workers[worker.process.pid] = { ThreadIndex: i };
            }

            cluster.on('exit', function (worker, code, signal) {
                console.log('thread ' + worker.process.pid + ' died, forking a new one');

                var env = workers[worker.process.pid];
                var newWorker = cluster.fork(env);

                delete workers[worker.process.pid];
                workers[newWorker.process.pid] = env;
            });
        }
    } else
        _appStart(this, this.router);

}

function _appStart(self, router) {
    keystone.start();

    var langs = self.get('langs');
    if (langs) {
        var multilingualConfig = self.config.getPluginConfig('Multilingual') || {};
        if (multilingualConfig.defaultLanguage) {
            var defaultLangIndex = langs.indexOf(multilingualConfig.defaultLanguage);
            if (defaultLangIndex != -1) {
                langs.splice(defaultLangIndex, 1);
                langs.splice(0, 0, multilingualConfig.defaultLanguage)
            } else {
                console.warn('SFT : Default language not exists in the languages options. Fallback to first language.')
            }
        }
        self.set('langs', langs);
        var middleware = self.middleware;
        self.router.setRoutes(function (app) {
            app.use(middleware.langs);
        })
    }

    //admin.staticRouter.prebuild();
    admin.load(self, keystone.get('app'));

    router.loadRoutes();
}

module.exports = start;