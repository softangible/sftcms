module.exports = {
    error : {id:0, label:'Error'},
    warn : {id:1, label:'Warning'},
    info : {id:2, label:'Info'},
    verbose : {id:3, label:'Verbose'},
    debug : {id:4, label:'Debug'},
    silly : {id:5, label:'Silly'}
}