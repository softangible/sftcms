module.exports = {
    //Paypal
    PayapalPaymentCreateStart : {id:0, label:'Payal - Create Payment Start'},
    PayapalPaymentCreateResponse : {id:1, label:'Payal - Create Payment Response'},
    PayapalPaymentExecuteStart : {id:2, label:'Payal - Execute Payment Start'},
    PayapalPaymentExecuteResponse : {id:3, label:'Payal - Execute Payment Response'},
    //Invoice4U
    Invoice4UProcessRequestStart : {id:4, label:'Invoice4U - ProcessRequest Start'},
    Invoice4UProcessRequestResponse : {id:5, label:'Invoice4U - ProcessRequest Response'},
    Invoice4UTransactionResult : {id:6, label:'Invoice4U - Transaction Result'},
    Invoice4ULoginRequestStart : {id:10, label:'Invoice4U - Login Request Start'},
    Invoice4ULoginRequestResponse : {id:11, label:'Invoice4U - Login Request Response'},
    Invoice4UCreateDocumentRequestStart : {id:12, label:'Invoice4U - Create Document Request Start'},
    Invoice4UCreateDocumentRequestResponse : {id:13, label:'Invoice4U - Create Document Request Response'},
    //Facebook Webhook
    FacebookWebhookRequestLeadStart : {id:7, label:'Facebook Webhook - Request Lead Start'},
    FacebookWebhookRequestLeadResponse : {id:8, label:'Facebook Webhook - Request Lead Response'},
    FacebookWebhookNoData : {id:9, label:'Facebook Webhook - Facebook Request No Data'},

    //Maskyoo
    Maskyoo : {id:14, label:'Maskyoo - Inside Call Inforamtion'},
    MaskyooApiRequest : {id:100, label:'Maskyoo - Api Request'},
    MaskyooApiResponse : {id:101, label:'Maskyoo - Api Response'},

    //TapuzDelivery
    TapuzDelivrySaveDataStartProcess : {id:15, label:'TapuzDelivery - SaveData - StartProcess'},
    TapuzDelivrySaveDataRequest : {id:16, label:'TapuzDelivery - SaveData - Request Details'},
    TapuzDelivrySaveDataError : {id:17, label:'TapuzDelivery - SaveData - Error'},
    TapuzDelivrySaveDataResponse : {id:18, label:'TapuzDelivery - SaveData - Response'}
}