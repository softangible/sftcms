var types = require('./log-types');

module.exports = {
    LogLevel : require('./log-levels'),
    addTypes : addTypes,
    get LogTypes () { return types; }   
}

function addTypes(externalTypes) {
    Object.keys(externalTypes).forEach(function (key) {
        types[key] = externalTypes[key];
    })
}