module.exports = {
	answer: {id:1, label:'שיחה נענתה',name:"answer"},
	noanswer: {id:2, label:'שיחה לא נענתה',name:"noanswer"},
	busy: {id:3, label:'עסוק',name:"busy"},
	callerCancel: {id:4, label:'שיחה בוטלה',name:"caller cancel"},
	notActive: {id:5, label:'לא פעיל',name:"not active"},
	blackList: {id:6, label:'רשימה שחורה',name:"black list"},
	outOfTime: {id:7, label:'outOfTime',name:"out of time"}
};

