var request = require('request'),
	deepExtend = require('deep-extend'),
	config = require('../core/config/config').get('ActiveTrail');

//http://webapi.mymarketing.co.il/api/docs/User/Api/POST-api-templates-Id-campaign
function CreateCampaignFromTemplate(templateId, name, subject, pairs, data, cb) {

	var sendData = {
		campaign_details: {
			name: name,
			subject: subject
		},
		pairs: pairs
	};

	if (data)
		deepExtend(sendData, data);

	var options = {
		method: 'POST',
		url: 'api/templates/' + templateId + '/campaign',
		json: sendData
	}

	_sendRequest(options, cb)
}

//http://webapi.mymarketing.co.il/api/docs/User/Api/POST-api-campaigns
function CreateCampaign(name, subject, user_profile_id, content, language_type, data, cb) {
	language_type = (language_type || config.langId) || 40;

	var sendData = {
		details: {
			name: name,
			subject: subject,
			user_profile_id: user_profile_id || config.userProfileId
		},
		design: {
			content: content,
			language_type: language_type,
			header_footer_language_type: language_type,
		}
	};

	if (data)
		deepExtend(sendData, data);

	var options = {
		method: 'POST',
		url: 'api/campaigns',
		json: sendData
	}

	_sendRequest(options, cb)
}

//https://webapi.mymarketing.co.il/api/docs/User/Api/GET-api-contacts_CustomerStates_SearchTerm_FromDate_ToDate_Page_Limit
function GetContacts(search_term, from_date, to_date, customer_states, data, cb) {

	var sendData = {
		SearchTerm: search_term,
		customer_states: customer_states,
		from_date: from_date,
		to_date: to_date
	};

	if (data)
		deepExtend(sendData, data);

	var options = {
		method: 'GET',
		url: 'api/contacts',
		qs: sendData
	}

	_sendRequest(options, cb);
}
//https: //webapi.mymarketing.co.il/api/docs/User/Api/POST-api-contacts
	function CreateOrUpdateContact(subscribe_ip, email, sms, first_name, last_name, status, data, cb) {

		var sendData = {
			subscribe_ip: subscribe_ip,
			email: email,
			sms: sms,
			first_name: first_name,
			last_name: last_name

		};

		if (data)
			deepExtend(sendData, data);

		var options = {
			method: 'POST',
			url: 'api/contacts',
			json: sendData
		}

		_sendRequest(options, cb)
	}


function CreateOrUpdateContactWithGroup(groupId, subscribe_ip, email, sms, first_name, last_name, status, data, cb) {
	var contacts = [{
		subscribe_ip: subscribe_ip,
		email: email,
		sms: sms,
		first_name: first_name,
		last_name: last_name

	}];
    var sendData = {
        group: groupId,
        contacts: contacts

    };

    if (data)
        deepExtend(sendData, data);


	var options = {
		method: 'POST',
		url: 'api/contacts/Import',
		json: sendData
	}

	_sendRequest(options, cb)
}

function _sendRequest(options, cb) {
	options.url = 'http://webapi.mymarketing.co.il/' + options.url;
	options.headers = {
		Authorization: 'Basic ' + config.apiKey
	}

	var req = request(options, function (err, httpResponse, body) {
		cb(err, body);
	})
}



module.exports = {
	CreateCampaignFromTemplate: CreateCampaignFromTemplate,
	CreateCampaign: CreateCampaign,
	GetContacts: GetContacts,
	CreateOrUpdateContact: CreateOrUpdateContact,
	CreateOrUpdateContactWithGroup: CreateOrUpdateContactWithGroup
}
