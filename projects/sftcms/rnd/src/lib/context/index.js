/**
 * Relys on depreacted Node.js functionallity
 * Consider using 
 * https://github.com/skonves/express-http-context
 * For node.js 8+ (beside 10.0.0-10.3.0)
 */
var contextService = require('request-local'),
    contextMiddleware = require('request-local/middleware').create(),
    keystone = require('keystone'),
    ns = 'context';

module.exports = {
    middleware : contextMiddleware,
    set : set,
    get : get,
    getLangContext : getLangContext,
    setLocale : setLocale,
    get request(){
        return getRequest();
    }
}

//////////////////////////////////////////////////////////////////////////

function middleware(req,res,next){
    require('request-local/middleware').create()(req,res,next);
}

function set(key,value){
    try{
        eval('contextService.data.' + key + '='+ JSON.stringify(value) +';');
        return true;
    } catch(err){
        return false;
    }
}

function get(key){
    try{
        return eval('contextService.data.' + key);
    } catch(err){
        return undefined;
    }
}

function getRequest(){
    return get('request');
}

function setLocale(locale){
    return set('request.query.lang',locale);
}

function getLangContext(){
    var lang = get('request.query.lang');

    if (!lang){
        var langs = keystone.get('langs');
        if (!langs || !langs.length) return undefined;
        return langs[0];
    }

    return lang;
}