module.exports = {
    Ecom : require('./e-com'),
    Payments : require('./payments'),
    Leads : require('./leads'),
    IRS : require('./irs')
}