var soap = require('soap'),
    _ = require('underscore'),
    Enums = require('../../../enums'),
    invoiceConfig = require('../../../core/config/config').get('Invoice4U'),
    domain, wsdl, client, loginClient, docsClient, loginWsdl, loginEndpoint, docsWsdl, docsEndpoint;

if (invoiceConfig) {
    domain = (invoiceConfig.mode == "sandbox") ? 'privateqa' : 'private';
    wsdl = 'https://' + domain + '.invoice4u.co.il/Services/MeshulamService.svc?singleWsdl';
    loginEndpoint = 'http://' + domain + '.invoice4u.co.il/Services/LoginService.svc/Soap';
    loginWsdl = 'http://' + domain + '.invoice4u.co.il/Services/LoginService.svc?singleWsdl';
    docsEndpoint = 'http://' + domain + '.invoice4u.co.il/Services/DocumentService.svc/Soap';
    docsWsdl = 'http://' + domain + '.invoice4u.co.il/Services/DocumentService.svc?singleWsdl';
}

function getLogger() {
    return require('../../../..').Logger;
}

////////////////////////////////////////////// Create Clients

function _getClient(cb) {
    if (client)
        return cb(null, client);
    soap.createClient(wsdl, function (err, cl) {
        client = cl;
        cb(err, client)
    })
}

function _getLoginClient(cb) {
    if (loginClient)
        return cb(null, loginClient);
    soap.createClient(loginWsdl, { endpoint: loginEndpoint }, function (err, cl) {
        loginClient = cl;
        cb(err, loginClient)
    })
}

function _getDocsClient(cb) {
    if (docsClient)
        return cb(null, docsClient);
    soap.createClient(docsWsdl, { endpoint: docsEndpoint }, function (err, cl) {
        docsClient = cl;
        cb(err, docsClient)
    })
}

//////////////////////////////////////////// Utils

function _formatArgs(keys, request) {
    var args = {}

    keys.forEach(function (key) {
        if (request[key] !== undefined && request[key] !== null)
            args[key] = request[key];
    })

    return args;
}

/////////////////////////////////////////// Internal Methods

function _getLoginToken(cb) {
    if (!invoiceConfig || !invoiceConfig.UserEmail || !invoiceConfig.UserPassword) {
        return cb(new Error('Please specify UserEmail and UserPassword in the config or the request'));
    }
    var logger = getLogger();

    _getLoginClient(function (err, cl) {
        if (err) return cb(err);
        var args = {
            username: invoiceConfig.UserEmail,
            password: invoiceConfig.UserPassword,
            isPersistent: false
        };

        logger.info(Enums.Log.LogTypes.Invoice4ULoginRequestStart, 'Starting Login Request', args);

        cl.VerifyLogin(args, function (err, result) {
            var logLevel, msg, data;
            if (err || !result || !result.VerifyLoginResult) {
                data = err;
                logLevel = 'error';
                msg = err ? err.message : 'General Failure';
            } else {
                data = result;
                logLevel = 'info';
                msg = 'Successfully done Login Request'
            };

            logger[logLevel](Enums.Log.LogTypes.Invoice4ULoginRequestResponse, msg, data);

            if (err || !result || !result.VerifyLoginResult) {
                return cb(err || new Error('Could not get token'));
            }
            cb(null, result.VerifyLoginResult);
        })
    })
}


function _createDocument(args, cb) {
    if (!args.Subject || !args.Total || !args.Items || !args.Items.length || !args.Payments || !args.Payments.length)
        return cb(new Error("Subject, Total, Items and Payments are mandatory"));

    if (!args.DocumentType) args.DocumentType = 3;

    var logger = getLogger();

    _getDocsClient(function (err, cl) {
        if (err) return cb(err);
        _getLoginToken(function (err, token) {
            if (err) return cb(err);

            args = _formatArgs(Object.keys(cl.describe().DocumentService.BasicHttpBinding_DocumentService.CreateDocument.input.doc), args);

            args.Items = { DocumentItem: args.Items }
            args.Payments = { Payment: args.Payments }

            if (args.AssociatedEmails)
                args.AssociatedEmails = { AssociatedEmail: args.AssociatedEmails }

            var sentData = { doc: args, token: token };

            logger.info(Enums.Log.LogTypes.Invoice4UCreateDocumentRequestStart, 'Starting Create Document Request', sentData);

            cl.CreateDocument(sentData, function (err, result) {
                var logLevel, data, msg;
                if (err || !result || !result.CreateDocumentResult || result.CreateDocumentResult.Errors) {
                    logLevel = 'error';
                    data = err || new Error(result.CreateDocumentResult.Errors ? JSON.stringify(result.CreateDocumentResult.Errors) : 'General Error Occured');
                    msg = data.message;
                } else {
                    logLevel = 'info';
                    data = result;
                    msg = 'Successfully done Create Document Request'
                }

                logger[logLevel](Enums.Log.LogTypes.Invoice4UCreateDocumentRequestResponse, msg, data);

                if (err || !result || !result.CreateDocumentResult || result.CreateDocumentResult.Errors)
                    return cb(err || new Error(result.CreateDocumentResult.Errors ? JSON.stringify(result.CreateDocumentResult.Errors) : 'General Error Occured'));

                cb(null, {
                    documentId: result.CreateDocumentResult.ID,
                    documentNumber: result.CreateDocumentResult.DocumentNumber,
                    pdfUrl: 'https://' + domain + '.invoice4u.co.il/he/Views/PDF.aspx?docid=' + result.CreateDocumentResult.ID + '&docNumber=' + result.CreateDocumentResult.DocumentNumber + '&original=true'
                })
            })
        })
    })

}

/////////////////////////////////////////// External Methods

function GetIframeUrl(request, cb) {
    _getClient(function (err, client) {
        if (err) return cb(err);

        if (invoiceConfig && invoiceConfig.UserEmail) {
            request.UserEmail = request.UserEmail || invoiceConfig.UserEmail;
            request.UserPassword = request.UserPassword || invoiceConfig.UserPassword;
        }

        if (!request.UserEmail || !request.UserPassword)
            return cb('Please specify UserEmail and UserPassword in the config or the request');

        request.Type = request.Type || 2;
        request.PaymentsNum = request.PaymentsNum || 1;

        var args = _formatArgs(Object.keys(client.describe().MeshulamService.BasicHttpBinding_MeshulamService.ProccessRequest.input.request), request);

        args = { request: args };

        var Logger = require('../../../..').Logger;
        Logger.info(Enums.Log.LogTypes.Invoice4UProcessRequestStart, 'Starting ProccessRequest', args);

        client.ProccessRequest(args, function (err2, result) {
            if (!err2) {
                if (result.ProccessRequestResult.Errors) err2 = result.ProccessRequestResult.Errors;
                else if (!result.ProccessRequestResult.ClearingRedirectUrl) err2 = { message: 'No Iframe URL Recivied' };
            }

            var logLevel = err2 ? 'error' : 'info',
                msg = err2 ? (err2.message ? err2.message : 'General Failure') : 'Successfully done ProccessRequest';
            Logger[logLevel](Enums.Log.LogTypes.Invoice4UProcessRequestResponse, msg, err2 || result);

            cb(err2, result ? result.ProccessRequestResult : null);
        })

    })
}

function InvoiceReturnMiddleware(req, res, next) {
    var result = JSON.parse(new Buffer(req.query.json, 'base64').toString()),
        logLevel = 'info',
        msg = 'Transaction ended successfully';

    if (result.status != 1 || req.query.response.toLowerCase() != 'success') {
        logLevel = 'error';
        msg = result.err.message;
    }

    var Logger = require('../../../..').Logger;
    Logger[logLevel](Enums.Log.LogTypes.Invoice4UTransactionResult, msg, result);

    next();
}


/////////////////////////////////////// Document Prototype

function Document(subject, taxPrecentage) {
    this.Items = [];
    this.Payments = [];
    this.AssociatedEmails = [];
    this.total = null;
    this.Subject = subject;
    this.TaxIncluded = taxPrecentage ? false : true;
    this.TaxPercentage = taxPrecentage;
}

Document.prototype.addEmail = function (email, isUserMail) {
    if (!email)
        throw new Error('email is required');

    this.AssociatedEmails.push({
        IsUserMail: isUserMail,
        Mail: email
    });
}

Document.prototype.AddItem = function (name, quantity, totalItemPrice, code) {
    if (!name || !totalItemPrice || !quantity)
        throw new Error('Name, Quantity and Price are required');

    var priceWithoutTax;
    if (!this.TaxIncluded) {
        priceWithoutTax = totalItemPrice / ((100 + this.TaxPercentage) / 100);
    }

    this.Items.push({
        Code: code,
        Name: name,
        Price: this.TaxIncluded ? totalItemPrice : priceWithoutTax,
        Quantity: quantity,
        TaxPrecentage: this.TaxIncluded ? null : this.TaxPercentage,
        TotalWithOutTax: this.TaxIncluded ? null : quantity * priceWithoutTax,
        Total: totalItemPrice * quantity
    });
}

Document.prototype.SetGeneralCustomer = function (name, identifier) {
    this.GeneralCustomer = {
        Name: name,
        Identifier: identifier
    }
}

Document.prototype.AddPayment = function (date, amount, paymentType, accountNumber, bankName, branchName, creditCardType, expirationDate, numberOfPayments, paymentNumber) {
    if (!date)
        date = new Date();

    if (_.isDate(date))
        date = date.toJSON();

    this.Payments.push({
        AccountNumber: accountNumber,
        Amount: amount || null,
        BankName: bankName,
        BranchName: branchName,
        CreditCardType: creditCardType,
        Date: date,
        ExpirationDate: expirationDate,
        NumberOfPayments: numberOfPayments,
        PaymentNumber: paymentNumber,
        PaymentType: paymentType || 1
    })
}

Document.prototype.Create = function (cb) {
    var nullPayment = false, total = 0;
    for (var i = 0, length = this.Payments.length; i < length; i++) {
        if (this.Payments[i].Amount === null) {
            if (nullPayment)
                throw new Error('Payments are formed incorrectly');
            nullPayment = true;
            var pTotal = 0;
            for (var itemIndex = 0, itemsCount = this.Items.length; itemIndex < itemsCount; itemIndex++)
                pTotal += this.Items[itemIndex].Total;

            this.Payments[i].Amount = pTotal;
        }

        total += this.Payments[i].Amount;
    }

    this.Total = total;

    _createDocument(this, cb);
}

module.exports = {
    GetIframeUrl: GetIframeUrl,
    InvoiceReturnMiddleware: InvoiceReturnMiddleware,
    /// Create Methods
    Document: Document
}