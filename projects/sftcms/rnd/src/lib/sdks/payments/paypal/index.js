var paypal = require('paypal-rest-sdk'),
    url = require('url'),
    Enums = require('../../../enums'),
    config = require('../../../core/config/config'),
    BASE_URL = config.get('Base_URL'),
    paypalConfig = config.get('Paypal'),
    RedirectUrls = null;
    
if (paypalConfig){
    if(paypalConfig.RedirectUrls)
        RedirectUrls = _getRedirectUrls();
    if (paypalConfig.config)
        paypal.configure(paypalConfig.config);
}

module.exports = {
    CreateItem: CreateItem,
    GetStandartPaymentObject: GetStandartPaymentObject,
    CreatePayment: CreatePayment,
    CreateStandartPayment: CreateStandartPayment,
    ExecutePayment: ExecutePayment
}

function _getRedirectUrls(){
    return {
        return_url : url.resolve(BASE_URL,paypalConfig.RedirectUrls.return_url),
        cancel_url : url.resolve(BASE_URL,paypalConfig.RedirectUrls.cancel_url)
    }
}

function CreateItem(name, description, price, quantity, currency) {
    return {
        name: name,
        description: description,
        price: price,
        quantity: quantity,
        currency: currency
    }
}

function _calculateTotal(items) {
    var sum = 0;

    items.forEach(function (item) {
        sum += (item.price * item.quantity)
    })

    return sum;
}

function _createPayment(pay, cb) {
    var Logger = require('../../../..').Logger;
    Logger.info(Enums.Log.LogTypes.PayapalPaymentCreateStart ,'Starting create new payment',pay);
    paypal.payment.create(pay, function (err, payment) {
        var logLevel = (err) ? 'error' : 'info';
        Logger[logLevel](Enums.Log.LogTypes.PayapalPaymentCreateResponse ,err ? err.message : 'Payment Created Successfully', err || payment)

        cb(err, payment);
    })
}

function GetStandartPaymentObject(description, email, fname, lname, phone, items, total, currency) {
    if (!currency) currency = items[0].currency;
    var payment = {
        intent: "sale",
        payer: {
            payment_method: "paypal",
            payer_info: {
                email: email,
                first_name: fname,
                last_name: lname
            }
        },
        transactions: [{
            item_list: {
                items: items
            },
            amount: {
                total: total || _calculateTotal(items),
                currency: currency
            },
            description: description
        }]
    }

    if (RedirectUrls)
        payment.redirect_urls = RedirectUrls;


    return payment;

}

function CreatePayment(payment, cb) {
    if (RedirectUrls)
        payment.redirect_urls = RedirectUrls;

    payment.intent = "sale";

    _createPayment(payment, cb);
}

function CreateStandartPayment(description, email, fname, lname, phone, items, total, currency, cb) {
    var payment = GetStandartPaymentObject(description, email, fname, lname, phone, items, total, currency);

    _createPayment(payment, cb);
}


function ExecutePayment(paymentId, payer_id, cb) {
    var data = { "payer_id": payer_id };
    
    var Logger = require('../../../..').Logger;
    Logger.info(Enums.Log.LogTypes.PayapalPaymentExecuteStart ,'Starting execute payment',{payerId : payer_id, paymentId:paymentId});

    paypal.payment.execute(paymentId, data, function (err, payment) {
        var logLevel = err ? 'error' : 'info';
        Logger[logLevel](Enums.Log.LogTypes.PayapalPaymentExecuteResponse ,err ? err.message : 'Starting execute payment',err || payment);
        cb(err, payment);
    });
}