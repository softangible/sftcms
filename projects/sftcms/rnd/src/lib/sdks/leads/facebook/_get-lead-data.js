var fbWebhookConf = require('../../../core/config/config').get('FacebookWebhook'),
    request = require('request'),
    Enums = require('../../../enums'),
    fbBaseUrl = 'https://graph.facebook.com/v2.5/',
    Logger;

module.exports = function (fblead, accessToken,cb) {
    if (!accessToken && !fbWebhookConf)
        return cb(new Error('no access token specified'));

    accessToken = accessToken || fbWebhookConf.accessToken;

    var url = fbBaseUrl + fblead.leadgen_id + '?access_token=' + accessToken;

    if (!Logger)
        Logger = require('../../../..').Logger;
    
    Logger.info(Enums.Log.LogTypes.FacebookWebhookRequestLeadStart ,'Starting Lead data request',{url : url});

    request(url, function (error, response, body) {
        if (error || response.statusCode != 200){
            var err = error || new Error('Recivied Status: ' + response.statusCode);
            Logger.error(Enums.Log.LogTypes.FacebookWebhookRequestLeadResponse ,err.message, {err : err, response : response});
            return cb(err)
        }

        var lead = JSON.parse(body);
        lead.formattedData = {};

        lead.field_data.forEach(function (field) {
            lead.formattedData[field.name] = field.values[0];
        })

        lead.fbChange = fblead

        Logger.info(Enums.Log.LogTypes.FacebookWebhookRequestLeadResponse ,'Retrived Lead Successfully', {lead : lead, response : response});

        cb(null, lead);
    })
}