var fbWebhookConf = require('../../../core/config/config').get('FacebookWebhook')

module.exports = function(req,res){
    if (!fbWebhookConf || !fbWebhookConf.verifyToken){
        return res.sendStatus(500);
    }

    if (req.query['hub.verify_token'] === fbWebhookConf.verifyToken) {
        return res.send(req.query['hub.challenge']);
    }

    res.status(403).send('Error, wrong validation token');

}