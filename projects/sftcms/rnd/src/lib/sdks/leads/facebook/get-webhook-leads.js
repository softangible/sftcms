var fbWebhookConf = require('../../../core/config/config').get('FacebookWebhook'),
    GetLeadData = require('./_get-lead-data'),
    Enums = require('../../../enums'),
    async = require('async');

module.exports = function (requestBody, accessToken, done) {

    if (requestBody && requestBody.entry && requestBody.entry[0].changes) {
        var leads = [];

        requestBody.entry[0].changes.forEach(function (fbChange) {
            if (fbChange.field === 'leadgen') {
                var fbLead = fbChange.value;
                leads.push(function (cb) {
                    GetLeadData(fbLead, accessToken, cb)
                })
            }
        })

        async.parallel(leads, done);

    } else{
        require('../../../..').Logger.error(Enums.Log.LogTypes.FacebookWebhookNoData ,'No Data', {req : requestBody});
        return done('not a valid body');
    }
}

