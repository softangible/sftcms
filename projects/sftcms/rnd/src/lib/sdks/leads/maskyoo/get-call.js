var Enums = require('../../../enums'),
    Logger;

module.exports = function (maskyooWhbhookData, done) {
    if (!Logger)
        Logger = require('../../../..').Logger;

    Logger.info(Enums.Log.LogTypes.Maskyoo, 'Starting Maskyoo inside call information', { data: request });
     var data =JSON.parse(JSON.stringify(maskyooWhbhookData).replace(/amp;/g,''))
    var request = {
        data: JSON.stringify(data)
    }
   

    Object.keys(Enums.Maskyoo.Status).forEach(function (key) {
        if (Enums.Maskyoo.Status[key]) {
            if (data.status.toLowerCase() == Enums.Maskyoo.Status[key].name)
                request.status = Enums.Maskyoo.Status[key].id;
        }
    });
  
    request.totalSecounds = data.cdr_totac_sec;
    request.startCall = data.cdr_start_call;
    request.fromPhone = data.ani;
    request.statusString = data.status;
    request.destination = data.cdr_destination;
    request.maskyooPhone = data.cdr_ddi;

    done(request);

}
