var maskyooConf = require('../../../core/config/config').get('Maskyoo'),
    Enums = require('../../../enums'),
    Promise = require('bluebird'),
    rp = require('request-promise'),
    Logger;

var MASKYOO_BASE_URL = 'http://maskyoo.co.il/'

module.exports = function(data){
    if (!Logger)
        Logger = require('../../../..').Logger;

    if (!maskyooConf)
        return Promise.reject(new Error("No configuration specified"));

    if(!data)
        data = {format : 'json'}
    else 
        data.format = 'json';

    var requestOptions = {
        uri :  MASKYOO_BASE_URL + maskyooConf.account + '/api/',
        json : true,
        qs : data
    }

    Logger.info(Enums.Log.LogTypes.MaskyooApiRequest, 'Starting Maskyoo API Request', { data: requestOptions });

    return rp(requestOptions).auth(null, null, true, maskyooConf.api_token).then(res=>{
        if (res.status.code != 200){
            Logger.warn(Enums.Log.LogTypes.MaskyooApiResponse, 'Failed response from maskyoo', { data: res });
            return Promise.reject(new Error(res.status.description || res.status.message))
        }

        Logger.info(Enums.Log.LogTypes.MaskyooApiResponse, 'Got response from Maskyoo API', { data: res });
        return res;
    }).catch(err=>{
        Logger.warn(Enums.Log.LogTypes.MaskyooApiResponse, 'Failed response from maskyoo', { data: err });
        throw err;
    });
}
