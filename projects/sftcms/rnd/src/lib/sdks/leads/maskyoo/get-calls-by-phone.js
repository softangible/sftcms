var getCallsByNumbers = require('./_get-calls-by-numbers');

module.exports = function (numbers) {
    return getCallsByNumbers(numbers,'user_phone');
}