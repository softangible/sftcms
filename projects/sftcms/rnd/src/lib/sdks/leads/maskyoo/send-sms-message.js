var maskyooConf = require('../../../core/config/config').get('Maskyoo'),
    Enums = require('../../../enums'),
    Promise = require('bluebird'),
    xml2js = require('xml2js'),
    rp = require('request-promise'),
    Logger;

var MASKYOO_SMS_BASE_URL = 'https://sms.deals/ws.php'

/**
 * Send an sms using maskyoo account
 * 
 * @param {object} data - a json object representing the sms send request including:
 *      dest is the Destination Phone Number - the number to send the sms to
 *      sender is the Origin phone number - the number the sms was sent from
 *      message is a string representing the message to send
 *      callback is a uri represnting the callback end point 
 *      {
 *          dest        : <phone number>,
 *          sender      : <phone number>,
 *          message     : <text>,
 *          callback    : <uri>
 *      }
 * 
 * Maskyoo Response: 
// var mskyoosmsresponse = '<?xml version="1.0" encoding="UTF-8"?><SMS_Service><service>send_sms</service><Message>activation code</Message><Source>0547696076</Source><sms_recipients><sms_0>' +
//         '<Message_id>146411210</Message_id>' +
//         '<Destination>0547696076</Destination>' + 
//         '<Status>Message in Action</Status>'+
//         '<Send_time>2018-10-22 09:21:35</Send_time>'+
//         '</sms_0></sms_recipients><Total_sms>1</Total_sms><Total_Charge>1</Total_Charge></SMS_Service>';
 

Configuration File should include the following:
  
"Maskyoo" : {
    "account" : "<the name of the account>",
    "sms" : {
      "message"  : "<The text message to send to the user>",
      "user" : "<user name>",
      "password" : "<password>"
    }
  },

 *      
 */
module.exports = function(data, cb){
    //"https://sms.deals/ws.php?service=send_sms&dest=destinationPphoneNumber&sender=originPhoneNumber&
    //username=userName&password=password&message=Hello%20world&callback=returnCallbackEndPoint"
    
    if (!Logger)
    Logger = require('../../../..').Logger;

    if (!maskyooConf)
        return Promise.reject(new Error("No configuration specified."));

    if(!data)
        return Promise.reject(new Error("No data was provided."));

    data.format = 'json';
    data.service = 'send_sms';
    data.username = maskyooConf.sms.user;
    data.password = maskyooConf.sms.password;
    data.message = data.message;

    var requestOptions = {
        uri :  MASKYOO_SMS_BASE_URL,
        json : true,
        qs : data
    }

    Logger.info(Enums.Log.LogTypes.MaskyooSmsApiRequest, 'Starting Maskyoo SMS API Request', { data: requestOptions });

    rp(requestOptions).auth(null, null, true, maskyooConf.api_token).then(res=>{
        var parser = new xml2js.Parser();
        var xmlResponse = res;
        parser.parseString(res, function (err, result) {
            cb(err, result, xmlResponse);
        });    
    }).catch(err=>{
        Logger.warn(Enums.Log.LogTypes.MaskyooSmsApiResponse, 'Failed response from maskyoo sms api', { data: err });
        throw err;
    });
}
