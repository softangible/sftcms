var Enums = require('../../../enums'),
    makeMaskyooCall = require('./_make-api-call');

module.exports = function (startDate, endDate) {
    
    var wheres = []
    if(startDate)
        wheres.push(`'${dateToString(startDate)}'`)
    
    if(endDate)
        wheres.push(`'${dateToString(endDate)}'`)

    var query = 'SELECT id, start_call, end_call, call_duration, cdr_ani, cdr_ddi, onetouch, user_phone, user_name, cdr_uniqueid, call_status FROM webserviceview';
    query += ' where (call_status != "Black List") '
    if(wheres.length){
        query += ' and '
        if (wheres.length > 1)
            query += `start_call between ${wheres[0]} and ${wheres[1]}`
        else if(startDate) 
            query += `start_call>=${wheres[0]}`
        else
            query += `start_call>=${wheres[0]}`
    }

    var data = {
        service : 'cdr_query',
        sql : query
    }
    
    return makeMaskyooCall(data);
}


function dateToString(date){
    function formatNumber(num){
        return (num < 10) ? `0${num}` : num;
    }

    var year = date.getFullYear(),
        month = formatNumber(date.getMonth()+1),
        day = formatNumber(date.getDate()),
        hours = formatNumber(date.getHours()),
        minutes = formatNumber(date.getMinutes()),
        seconds = formatNumber(date.getSeconds());


    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}