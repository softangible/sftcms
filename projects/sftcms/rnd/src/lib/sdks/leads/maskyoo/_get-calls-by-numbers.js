var maskyooConf = require('../../../core/config/config').get('Maskyoo'),
    makeMaskyooCall = require('./_make-api-call');

var MASKYOO_BASE_URL = 'http://maskyoo.co.il/'

module.exports = function(numbers, key){
    var query = 'SELECT id, start_call, end_call, call_duration, cdr_ani, cdr_ddi, onetouch, user_phone, user_name, cdr_uniqueid, call_status FROM webserviceview';
    
    if (numbers.length){
        query += ` where ${key} in ('${numbers.join(`','`)}')`
    }

    var data = {
        service : 'cdr_query',
        sql : query
    }
    
    return makeMaskyooCall(data);
}
