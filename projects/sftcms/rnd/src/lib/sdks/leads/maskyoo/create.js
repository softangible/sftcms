var makeMaskyooCall = require('./_make-api-call'),
    _ = require('lodash');

/**
 * 
 * @param {int | string} phoneNumber the phone number to connect the maskyoo to.
 * @param {object} extraData additional options to pass
 * http://portal.maskyoo.com/api/api_maskyoo.html#api-Default-create_maskyoo
 * 
 */
module.exports = function (phoneNumber, extraData) {
    var data = {
        greeting_in : 'empty',
        active_status : 1,
        description : 'NdlCom phone'
    }

    data = _.assign(data,extraData);
    
    data.service = 'release_maskyoo';
    data.maskyoo = '';
    data.call_destination_phone = phoneNumber;

    return makeMaskyooCall(data);
}