var makeMaskyooCall = require('./_make-api-call');

module.exports = function (maskyooNumber) {
    var data = {
        service : 'release_maskyoo',
        maskyoo : maskyooNumber
    }
    
    return makeMaskyooCall(data);
}