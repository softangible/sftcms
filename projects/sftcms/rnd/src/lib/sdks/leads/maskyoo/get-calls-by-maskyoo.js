var getCallsByNumbers = require('./_get-calls-by-numbers');

module.exports = function (numbers) {
    return getCallsByNumbers(numbers,'cdr_ddi');
}