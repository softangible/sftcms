module.exports = {
    GetCall : require('./get-call'),
    GetCallsByDate : require('./get-calls-by-date'),
    GetCallsByMaskyooNumbers : require('./get-calls-by-maskyoo'),
    GetCallsByPhoneNumbers : require('./get-calls-by-phone'),
    ReleaseNumber : require('./release'),
    CreateNumber : require('./create'),
    SendSms : require('./send-sms-message'),
}