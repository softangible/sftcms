var Promise = require('bluebird');
var rp = require('request-promise');

var geocoder = require('./geocoder');

var IRS_URL = 'https://www.nadlan.gov.il/Nadlan.REST/Main/';

function assetsArrayToBatches(arr, size) {
    size = size || 30;
    var result = [];

    while (arr.length > 0)
        result.push(arr.splice(0, size));

    return result;
}

function setDealSumNeighborhood(assets) {

    var dealsBatches = assetsArrayToBatches(assets);
    var results = [];
    return Promise.resolve(dealsBatches).map(getPromiseFromBatch, { concurrency: 1 }).then(res => {
        res.forEach(assetArray => {
            results = results.concat(assetArray);
        })
        return results;
    });

    function getPromiseFromBatch(assetArray) {
        return Promise.all(assetArray.map(asset => {
            return GetSumNeighborhood(asset.dealId).then(sumNeighborhood => {
                asset.sumNeighborhood = sumNeighborhood;
                return asset;
            });
        }));
    }
}

function setDealHistoryDeals(assets) {

    var assetsBatches = assetsArrayToBatches(assets);
    var results = [];
    return Promise.resolve(assetsBatches).map(getPromiseFromBatch, { concurrency: 1 }).then(res => {
        res.forEach(assetArray => {
            results = results.concat(assetArray);
        })
        return results;
    });

    function getPromiseFromBatch(assetArray) {
        return Promise.all(assetArray.map(asset => {
            return GetAllAssestHistoryDeals(asset.dealId).then(historicDeals => {
                asset.historyDeals = historicDeals.map(d => irsDealToHistoricDeal(d));
                return asset;
            });
        }));
    };
}

function GetAssetsRecursive(queryData, results) {
    queryData.PageNo = !queryData.PageNo ? 1 : (queryData.PageNo + 1);
    var options = {
        uri: 'GetAssestAndDeals',
        json: true,
        method: 'post',
        body: queryData
    }

    return makeRequest(options).then(res => {
        results.allResults = results.allResults.concat((res.AllResults).map(d => irsDealToAsset(d)));
        
        if (res.SpecificAddressData)
            results.specificAddressData = results.specificAddressData.concat((res.SpecificAddressData).map(d => irsDealToAsset(d)));
        
        console.log(res);
        
        return (res.IsLastPage) ? results : GetAssetsRecursive(queryData, results);
    }).catch(err => {
        return Promise.resolve(results);
    })
}

function makeRequest(options) {
    var userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
    //`Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36${(new Date()).getTime()}`
    options.uri = IRS_URL + options.uri;

    var default_options = {
        timeout : 10000,
        headers : {
            'User-Agent': userAgent,
            'Accept' : 'application/json, text/plain, */*',
            'Content-Type':'application/json;charset=UTF-8',
            'Connection' : 'Keep-Alive',
            'Cache-Control': 'no-cache',

        }
    }
    
    default_options = Object.assign(default_options, options);
    
    return rp(default_options)
    .then(res => {        
        if(res===undefined){
            return Promise.reject();
        }else{
            console.log(res);
            return Promise.resolve(res);
        }
    })
    .catch(err => {
        console.error(err);
        return Promise.reject(err);
    });
}

function irsDealToHistoricDeal(deal) {
    return {
        dealDate : deal.DEALDATE,
        dealDateTime : deal.DEALDATETIME,
        dealNatureDescription: deal.DEALNATUREDESCRIPTION,
        dealNature: deal.DEALNATURE,
        dealAmount: deal.DEALAMOUNT
    }
}

function irsDealToAsset(deal) {
    var now = new Date();
    now.setDate(now.getDate() + 7);
    return {
        expirationDate: now,
        dealDate: deal.DEALDATETIME,
        fullAdress: deal.FULLADRESS,
        displayAdress: deal.DISPLAYADRESS,
        gush: deal.GUSH,
        assetRoomNum: deal.ASSETROOMNUM,
        floorNum: deal.FLOORNO,
        dealNatureDescription: deal.DEALNATUREDESCRIPTION,
        dealNature: deal.DEALNATURE,
        dealAmount: deal.DEALAMOUNT,
        newProjectText: deal.NEWPROJECTTEXT,
        projectName: deal.PROJECTNAME,
        buildingYear: deal.BUILDINGYEAR,
        yearBuilt: deal.YEARBUIL,
        buildingFloors: deal.BUILDINGFLOORS,
        dealId: deal.KEYVALUE,
        type: deal.TYPE,
        polygonId: deal.POLYGON_ID,
        trendIsNegative: deal.TREND_IS_NEGATIVE,
        trendFormat: deal.TREND_FORMAT
    }
}

function GetAllAssetsByAddress(address, radius) {
    radius = radius || 250;
    if (radius > 250) radius = 250;
    
    var finalResults;

    return GetPropertyDataByAddress(address).then(propertyData => {
        if(propertyData){
            //propertyData.MoreAssestsType = "1";
            propertyData.Distance = radius;
            
            return GetAssetsRecursive(propertyData, {
                specificAddressData: [],
                allResults: []
            });
        }else{
            Promise.reject("No records found");
        }
    }).then(results => {
        console.log('got all deals');
        finalResults = results;
        return setDealSumNeighborhood(finalResults.allResults)
    }).then(res => {
        finalResults.allResults = res;
        return setDealSumNeighborhood(finalResults.specificAddressData)
    }).then(res => {
        finalResults.specificAddressData = res;
        console.log('got all neighborhoods');
        return setDealHistoryDeals(finalResults.allResults)
    }).then(res => {
        finalResults.allResults = res;
        return setDealHistoryDeals(finalResults.specificAddressData)
    }).then(res => {
        finalResults.specificAddressData = res;
        console.log('got all history deals');
        return finalResults;
    });
}

function GeocodeAsset(asset) {
    return ((asset.fullAdress || !asset.gush) ?  geocoder.batchGeocode([asset.fullAdress]) : 
        geocoder.batchGushToAddress([asset.gush]).then(addresses=>{
            var address = addresses[0];
            if (!address) {
                return Promise.reject(new Error("Could not get address by Gush"))
            } else {
                var displayAdress = address[1] + ' ' + address[2];
                var fullAddress = displayAdress + ', ' + address[0];

                asset.fullAdress = fullAddress;
                asset.displayAdress = displayAdress;
            }
            return geocoder.batchGeocode([asset.fullAdress]);
    })).then(geocodeResult=>{
        if (!geocodeResult || !geocodeResult.length)
            return Promise.reject(new Error("Could not geocode address"));

        asset.coordinate = [geocodeResult[0].longitude, geocodeResult[0].latitude];
        return geocoder.batchReverseGeocode([asset.coordinate]);
    }).then(addresses=>{
        asset.formattedAddress = addresses[0].formatted;
        return asset;
    })
}

function GetFullDataByAddress(address, radius) {
    var finalResults;
    var fullResults;
    var specificAddressGushes = [];
    var allResultsGushes = [];
    
    
    GetAllAssetsByAddress(address,radius).then(res => {
        finalResults = res;
        if (finalResults.specificAddressData && finalResults.specificAddressData.length) {
            for (var i = 0, length = finalResults.specificAddressData.length; i < length; i++) {
                if (finalResults.specificAddressData[i].fullAdress || !finalResults.specificAddressData[i].gush)
                    continue;

                specificAddressGushes.push({ gush: finalResults.specificAddressData[i].gush, index: i });
            }
        }

        for (var i = 0, length = finalResults.allResults.length; i < length; i++) {
            if (finalResults.allResults[i].fullAdress || !finalResults.allResults[i].gush)
                continue;

            allResultsGushes.push({ gush: finalResults.allResults[i].gush, index: i });
        }

        return geocoder.batchGushToAddress(specificAddressGushes.concat(allResultsGushes).map(g => g.gush));
    }).then(addreses => {

        var specific = addreses.slice(0, specificAddressGushes.length);
        var all = addreses.slice(specificAddressGushes.length);

        for (var i = specificAddressGushes.length - 1; i >= 0; i--) {
            if (!specific[i]) {
                finalResults.specificAddressData.splice(specificAddressGushes[i].index,1);
            } else {
                var displayAdress = specific[i][1] + ' ' + specific[i][2];
                var fullAddress = displayAdress + ', ' + specific[i][0];

                finalResults.specificAddressData[specificAddressGushes[i].index].fullAdress = fullAddress;
                finalResults.specificAddressData[specificAddressGushes[i].index].displayAdress = displayAdress;
            }
        }

        for (var i = allResultsGushes.length - 1; i >= 0; i--) {
            if (!all[i]) {
                finalResults.allResults.splice(allResultsGushes[i].index,1);
            } else {
                var displayAdress = all[i][1] + ' ' + all[i][2];
                var fullAddress = displayAdress + ', ' + all[i][0];

                finalResults.allResults[allResultsGushes[i].index].fullAdress = fullAddress;
                finalResults.allResults[allResultsGushes[i].index].displayAdress = displayAdress;
            }
        }

        return finalResults;
    }).then(res => {
        console.log('start geocodes');
        var allResultsAddresses = [];
        if (finalResults.specificAddressData && finalResults.specificAddressData.length)
            allResultsAddresses.push(finalResults.specificAddressData[0].fullAdress);
        allResultsAddresses = allResultsAddresses.concat(finalResults.allResults.map(res => res.fullAdress));

        return geocoder.batchGeocode(allResultsAddresses,address);
    }).then(geocodeResult => {
        console.log('got all geocodes');
        if (!geocodeResult || !geocodeResult.length)
            return Array.prototype.concat(finalResults.allResults, finalResults.specificAddressData);

        if (finalResults.specificAddressData && finalResults.specificAddressData.length) {
            var specificLocation = geocodeResult.splice(0, 1)[0];

            finalResults.specificAddressData.forEach(res => {
                res.coordinate = !specificLocation ? null : [specificLocation.longitude, specificLocation.latitude];
            })
        }

        finalResults.allResults.forEach((deal, i) => {
            deal.coordinate = !geocodeResult[i] ? null : [geocodeResult[i].longitude, geocodeResult[i].latitude];
        })

        fullResults = Array.prototype.concat(finalResults.allResults, finalResults.specificAddressData);
        for (var i = fullResults.length - 1; i >= 0; i--) {
            if (!fullResults[i].coordinate)
                fullResults.splice(i, 1);
        }

        return fullResults;
    }).then(results=>{
        var coordinates = results.map(res=> res.coordinate);

        return geocoder.batchReverseGeocode(coordinates);
    }).then(addresses=>{
        for(var i=0, length=fullResults.length; i<length;i++){
            fullResults[i].formattedAddress = addresses[i].formatted;
        }
        return fullResults;
    });

}

function GetPropertyDataByAddress(query) {
    var options = {
        uri: 'GetDataByQuery?t='+(new Date()).getTime()+'&query=' + encodeURI(query),
        json: true
    } 

    return makeRequest(options).then(res => {
        return res;
    });
}

function GetAllAssestHistoryDeals(assetId) {
    var options = {
        uri: 'GetAllAssestHistoryDeals',
        json: true,
        method: 'post',
        body: assetId
    }
    return makeRequest(options);
}

function GetSumNeighborhood(assetId) {
    var options = {
        uri: 'SumNeighborhood',
        json: true,
        method: 'POST',
        body: assetId
    }

    return makeRequest(options);
}

module.exports = {
    GetSumNeighborhood: GetSumNeighborhood,
    GetAllAssestHistoryDeals: GetAllAssestHistoryDeals,
    GetAssetsRecursive: GetAssetsRecursive,
    GetFullDataByAddress: GetFullDataByAddress,
    GetAllAssetsByAddress : GetAllAssetsByAddress,
    GetPropertyDataByAddress : GetPropertyDataByAddress,
    GeocodeAsset : GeocodeAsset
}