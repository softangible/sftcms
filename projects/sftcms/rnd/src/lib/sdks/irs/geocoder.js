var Promise = require('bluebird');
var rp = require('request-promise');
var proj4 = require('proj4');

var email = require('../../core/Email');
var config = require('../../core/config/config');
var irsConfig = config.get('IRS');
var googleConfig = config.get('Google');

var token = "5a4b8472-b95b-4687-8179-0ccb621c7990";
var googleToken;
var referer = "http://localhost";


var UTM_SOURCE_PROJECTION = '+proj=tmerc +lat_0=31.73439361111111 +lon_0=35.20451694444445 +k=1.0000067 +x_0=219529.584 +y_0=626907.39 +ellps=GRS80 +towgs84=-48,55,52,0,0,0,0 +units=m +no_defs';
var LONG_LAT_DEST_PROJECTION = "+proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees +no_defs";

var GOVMAP_BASE_URI = "https://ags.govmap.gov.il/";
var GOVMAP_GEOCODER_BASE_URI = GOVMAP_BASE_URI + "Api/Controllers/GovmapApi/";
var GOVMAP_SEARCH_BASE_URI = GOVMAP_BASE_URI + "Search/SearchLocate";

var GOOGLE_GEOCODE_URL = "https://maps.googleapis.com/maps/api/geocode/json"

if (irsConfig) {
    token = irsConfig.apiToken || token;
    referer = irsConfig.referer || referer;
}

if (googleConfig && googleConfig.API && googleConfig.API.Geocode && googleConfig.API.Geocode.key) {
    googleToken = googleConfig.API.Geocode.key;
}

function sendEmail(err, subject, originalAddress) {
    if (!irsConfig || !irsConfig.GeocoderError || !irsConfig.GeocoderError.sendEmail)
        return null;

    var errMsg = JSON.stringify(err);
    var message = {
        to: irsConfig.GeocoderError.sendTo || [],
        from: irsConfig.GeocoderError.sendFrom || '',
        subject: subject,
        html: errMsg,
        text: errMsg
    }
    email.send(message, (err) => { });
}

function batchGeocode(addresses, originalAddress) {
    return getAuth().then(auth => {
        if (!auth || !auth.token)
            throw "Invalid token or referer";

        return auth;
    }).then(auth => {

        var uniqueAdresses = addresses.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });

        var errors = [];

        function addressToCoordinate(address) {
            return getGeocode(address, auth).catch(err => {
                errors.push(err);
                return null;
            })
        }

        return Promise.resolve(uniqueAdresses).map(addressToCoordinate, { concurrency: 1 }).then(results => {
            var finalResults = [];

            if (errors.length) {
                var subject = 'IRS Geocoding failed';
                if (originalAddress)
                    subject += ' for address: ' + originalAddress;
                sendEmail(errors, subject);
            }

            addresses.forEach(address => {
                for (var i = 0, length = uniqueAdresses.length; i < length; i++) {
                    if (address == uniqueAdresses[i])
                        finalResults.push(results[i]);
                }
            })

            return finalResults;
        });
    })


}

function batchReverseGeocode(coordinates) {

    var stringCoords = coordinates.map(cord => {
        return '' + cord[1] + ',' + cord[0]
    });

    var uniqueCoordinates = stringCoords.filter((value, index, self) => {
        return self.indexOf(value) === index;
    });

    var errors = [];
    var batches = arrayToBatches(uniqueCoordinates);

    function getPromiseFromBatch(batch) {
        return new Promise((resolve, reject) => {
            var promises = batch.map(cord => {
                return makeGoogleRequest({
                    qs: {
                        latlng: cord
                    }
                })
            })

            Promise.all(promises).then(res => { resolve(res); }).catch(err => { reject(err) });
        }).delay(1000);
    }


    return Promise.resolve(batches).map(getPromiseFromBatch, { concurrency: 1 }).then(results => {
        var allResults = Array.prototype.concat.apply([], results);

        allResults.map(singleResult => {
            if (singleResult.status != "OK"){
                singleResult.formatted = null;
                return singleResult;
            };

            var address = {
                formattedAddress: singleResult.results[0].formatted_address
            };

            singleResult.results[0].address_components.forEach(function (address_component) {

                if (address_component.types.indexOf('street_number') != -1) {
                    address.streetNum = address_component.long_name;
                }
                else if (address_component.types.indexOf('route') != -1) {
                    address.streetName = address_component.long_name;
                }
                else if (address_component.types.indexOf('neighborhood') != -1) {
                    address.neighborhoodName = address_component.long_name;
                }
                else if (address_component.types.indexOf('locality') != -1) {
                    address.cityName = address_component.long_name;
                }
            });

            singleResult.formatted = address;
            return singleResult;
        })

        uniqueCoordinates = Array.prototype.concat.apply([], batches);

        var finalResults = [];

        stringCoords.forEach(cords => {
            for (var i = 0, length = uniqueCoordinates.length; i < length; i++) {
                if (cords == uniqueCoordinates[i]) {
                    finalResults.push(allResults[i]);
                }
            }
        })

        return finalResults;
    });
}

function batchGushToAddress(gushes) {
    var uniqueGushes = gushes.filter((value, index, self) => {
        return self.indexOf(value) === index;
    });

    return Promise.resolve(uniqueGushes).map(getAddressByGush, { concurrency: 1 }).then(results => {
        var finalResults = [];
        gushes.forEach(gush => {
            for (var i = 0, length = uniqueGushes.length; i < length; i++) {
                if (gush == uniqueGushes[i])
                    finalResults.push(results[i]);
            }
        })

        return finalResults;
    });
}

function utmToCoordiante(x, y) {
    var cords = proj4(UTM_SOURCE_PROJECTION, LONG_LAT_DEST_PROJECTION, [x, y]);
    return {
        latitude: cords[1],
        longitude: cords[0]
    }
}

function getGeocode(address, authData) {
    var options = {
        uri: 'Geocode',
        method: 'post',
        body: {
            keyword: address
        },
        headers: {
            auth_data: JSON.stringify(authData)
        }
    }

    return makeRequest(options).then(res => {
        if (!res.data || !res.data.length)
            throw "Could not get data for address : " + address;

        return utmToCoordiante(res.data[0].X, res.data[0].Y);
    });
}

function getAddressByGush(gush) {
    var gushToSearch;
    var gushParts = gush.split('-');

    if (gushParts.length > 1)
        gushToSearch = [gushParts[0], gushParts[1]].join('--');
    else
        return Promise.resolve(null);

    var options = {
        uri: GOVMAP_SEARCH_BASE_URI,
        method: 'post',
        json: true,
        body: {
            whereValues: [
                "ID",
                gushToSearch,
                "text"
            ],
            locateType: 3
        }
    }

    return rp(options).then(res => {
        if (!res.data || !res.data.Values || !res.data.Values.length || !res.data.Values[0].Values || !res.data.Values[0].Values.length || res.data.Values[0].Values.length < 3)
            return null

        return res.data.Values[0].Values;
    });
}

function getAuth() {
    var options = {
        uri: 'Auth',
        method: 'post',
        body: {},
        headers: {
            auth_data: JSON.stringify({ "api_token": token, "user_token": "", "domain": "", "token": "" }),
        }
    }

    return makeRequest(options).catch(err => {
        sendEmail(err, "IRS Geocoding - authentication failed");
        throw err;
    });
}

function arrayToBatches(arr, size) {
    size = size || 50;
    var arrToSplice = arr;
    var result = [];

    while (arrToSplice.length > 0)
        result.push(arrToSplice.splice(0, size));

    return result;
}

function makeRequest(options) {
    options.uri = GOVMAP_GEOCODER_BASE_URI + (options.uri || '');
    options.headers.Referer = referer;
    options.json = true;

    return rp(options);
}

function makeGoogleRequest(options) {
    options.uri = GOOGLE_GEOCODE_URL + (options.uri || '');
    options.headers;
    options.json = true;

    if (options.qs) {
        options.qs.language = 'iw';
        options.qs.key = googleToken;
    } else {
        options.qs = {
            key: googleToken,
            language : 'iw'
        };
    }

    return rp(options);
}

module.exports = {
    batchGeocode: batchGeocode,
    batchGushToAddress: batchGushToAddress,
    batchReverseGeocode: batchReverseGeocode
}