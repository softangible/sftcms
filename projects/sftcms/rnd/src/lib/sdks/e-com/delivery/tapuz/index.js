var soap = require('soap'),
	Promise = require('bluebird'),
	Enums = require('../../../../enums'),
	tapuzDelivery = require('../../../../core/config/config').get('TapuzDelivery'),
    endpoint, wsdl, client,
    password,username,
    Logger,
    moment = require('moment');
    
   
if (tapuzDelivery) {
	password = tapuzDelivery.password;
    username = tapuzDelivery.username;
	if(tapuzDelivery.endpoint){
		endpoint = tapuzDelivery.endpoint
	}else{
		endpoint = (tapuzDelivery.mode == "sandbox") ? 'http://62.219.7.158/harws/Service.asmx?wsdl' : 'http://d.tpz.co.il/baldarwebservice/Service.asmx?wsdl';
	}
	endpoint = encodeURI(endpoint + "&password=" + password + "&username=" + username);
}
////////////////////////////////////////////// Create Clients

function _getClient(cb) {
	return new Promise((res, rej) => {
		if (client)
			return res(client);
		soap.createClient(endpoint, function (err, cl) {
			if (err) return rej(err);

			client = cl;
			return res(client);
		})
	});
}

/**  
  * @param {object} obj 	
        1. סוג משלוח -  obj.deliveryType || 1,
		2. רחוב מוצא - 	obj.incommingStreet || "",
		3. מס בית מוצא - obj.incommingHouseNumber || "",
		4. עיר מוצר - obj.incommingCity || "",
		5. רחוב יעד - obj.destinationStreet || "",
		6. מספר בית יעד - obj.destinationHouseNumber || "",
		7. עיר יעד - obj.destinationCity || "",
		8. שם חברה מוצא - obj.incommingCompanyName || "",
		9. שם חברה ביעד - obj.destinationCompanyName || "",
		10. הוראות למשלוח - obj.deliveryNotes || "",
		11. דחיפות - obj.priority || 1,
		12. קבוע - 	0,
        13. סוג דיוור - obj.packageType || 1,
		14. מספר חיבות - obj.numberOfPackages || 1,
		15. משלוח רגיל או כפול - obj.packageSingleOrDouble || 1,
        16. קבוע - 	0,
		17. מספר תעודה שלכם - obj.orderId || "",
		18. קוד לקוח בתפוז - obj.tapuzCustomerId || tapuzDelivery.customerId,
		19. ברקוד שלכם - skus,
		20. הערות לדו"ח - obj.notes || "",
		21. מספר משטחים - numberOfFlat || 0,
		22. מיקוד עיר מוצא - "",
		23. מיקוד עיר יעד - "",
		24. שם אישר קשר ביעד - obj.contactName || "",  
		25.	טלפון נייד איש קשר ביעד - obj.contactPhone || "",
        26. אימייל -  obj.contactEmail || "",
        27. תאריך ביצוע - obj.date ||  moment(Date.now()).format("YYYY-MM-DD");,
        28. COD - obj.COD || ""
  */  
function SaveData(obj) {
    if (!Logger)
        Logger = require('../../../../..').Logger;
        
    var obj = obj;
    Logger.info(Enums.Log.LogTypes.TapuzDelivrySaveDataStartProcess ,'Starting SaveData Process',"orderId - " + obj.orderId + " - " +JSON.stringify(obj));
	return _getClient().then(client => {

		var invalidCharReg = "('|\\*|\"|&|;|,)";
		for (var property in obj) {
			if (obj.hasOwnProperty(property) && obj[property]) {
				obj[property] = obj[property].replace(new RegExp(invalidCharReg, 'g'), "");
			}
		}
		
		var skus = "";

		if (obj.skus)
			skus = obj.skus.join("/");

		var arr = [
			obj.deliveryType || 1,
			obj.incommingStreet || "",
			obj.incommingHouseNumber || "",
			obj.incommingCity || "",
			obj.destinationStreet || "",
			obj.destinationHouseNumber || "",
			obj.destinationCity || "",
			obj.incommingCompanyName || "",
			obj.destinationCompanyName || "",
			obj.deliveryNotes || "",
			obj.priority || 1,
			0,
			obj.packageType || 1,
			obj.numberOfPackages || 1,
			obj.packageSingleOrDouble || 1,
			0,
			obj.orderId || "",
			obj.tapuzCustomerId || tapuzDelivery.customerId,
			skus,
			obj.notes || "",
			obj.numberOfFlat || 0,
			"",
			"",
			obj.contactName || "",
			obj.contactPhone || "",
            obj.contactEmail || "",
            obj.date || moment(Date.now()).format("YYYY-MM-DD"),
            obj.COD || ""
		]

	
		var data = {
			pParam: arr.join(';')
		}
		
		return new Promise((res, rej) => {
            Logger.info(Enums.Log.LogTypes.TapuzDelivrySaveDataRequest ,'SaveData request Details',"orderId - " + obj.orderId + " - " + JSON.stringify(data));
			client.SaveData(data, function (err, results) {
				if (err) {
                    Logger.error(Enums.Log.LogTypes.TapuzDelivrySaveDataError ,'SaveData Error',"orderId - " + obj.orderId + " - " + JSON.stringify(err));
                    return rej(err);
                }
                Logger.info(Enums.Log.LogTypes.TapuzDelivrySaveDataResponse ,'SaveData request Details', "orderId - " + obj.orderId + " - " + JSON.stringify(results));   
				return res(results);
			});

		})
	});
}
module.exports = {
    SaveData:SaveData
}
