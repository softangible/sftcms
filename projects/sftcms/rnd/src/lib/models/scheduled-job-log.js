var List = require('../List'),
    Types = require('keystone').Field.Types;
   
var ScheduledJobLog = new List('ScheduledJobLog', {
   hidden: true
});

ScheduledJobLog.addProperties({
   jobName: {type: String, label:'Job Name'},
   date: {type: Types.Datetime, default: Date.now},
   isSuccess: {type: Types.Boolean},
   exception: {type: String},
});


ScheduledJobLog.register();

module.exports = ScheduledJobLog;