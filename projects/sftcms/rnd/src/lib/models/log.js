var List = require('../List'),
    Types = require('keystone').Field.Types,
    utils = require('../utils'),
    Enums = require('../enums');

var Log = new List('Log', {
    hidden: true
});

Log.addProperties({
    type: { type: Types.Select, numeric: true, options: utils.enumToSelectOptions(Enums.Log.LogTypes) },
    date: { type: Types.Datetime, default: Date.now },
    level: { type: Types.Select, numeric: true, options: utils.enumToSelectOptions(Enums.Log.LogLevel) },
    message: { type: String },
    data: { type: String }
});


Log.register();

module.exports = Log;