var List = require('../List'),
    Types = require('keystone').Field.Types;
   
var BaseUser = new List('BaseUser', {
   hidden: true,
   track : true,
   viewField : 'name'
});

BaseUser.addProperties('פרטי משתמש',{
   name: {type: String, required: true,initial:true, index: true},
   email: {type: Types.Email,index: true,required: true,initial:true},
   password: {type: Types.UserPassword, initial:true},
   isActive:{type:Boolean,label:'משתמש פעיל'},
   isConfirmed:{type:Boolean},
   isAdmin:{type:Boolean, label: 'Can access Admin'},
   facebook_id : {type:String, noedit : true, hidden: true},
   google_id : {type:String, noedit : true, hidden: true},
   confirmationToken : {type:String, noedit : true, hidden: true},
   confirmationExpires : {type:Types.Datetime , noedit : true, hidden: true},
   passwordResetToken : {type:String , noedit : true, hidden: true},
   passwordResetExpires : {type:Types.Datetime , noedit : true, hidden: true}
});

// Provide access to Admin Back offices
BaseUser.getSchema().virtual('canAccessKeystone').get(function() {
	return this.isAdmin;
});

BaseUser.getSchema().pre('validate', function(next) {
	if (!this.facebook_id && !this.google_id && !this.password)
		return next(Error("Password is required for local users"));

    next();
});

BaseUser.register();

module.exports = BaseUser;