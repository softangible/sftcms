var List = require('../List'),
    Types = require('keystone').Field.Types,
    _ = require('lodash'),
    dbUtils = require('../core/dbUtils')

var BasePage = new List('BasePage', {
    hidden: true,
    metaTags: {
        title: '',
        description: '',
        keywords: '',
        image: ''
    }
});

BasePage.addProperties('Page Settings', {
    nameUrl: { type: String, required: true, initial: true },
    prio: { type: Types.Number, default: 0 },
    seoTitle: { type: String, initial: true },
    seoDescription: { type: String, initial: true },
    seoKeywords: { type: String, initial: true },
    isActive: { type: Boolean, default: false }
});

function _generateMetaTag(seoTag, metaTagFormat, self) {
    if (seoTag) return seoTag;

    //TODO:  get disctinct values
    if (!metaTagFormat)
        return "";

    var result = metaTagFormat,
        matches = metaTagFormat.match(/{(.*?)}/g);

    if (!matches)
        return result;

    matches.forEach(function (param) {
        var paramName = param.match(/{(.*)}/).pop().split('.');
        var selfParam = self.toObject();

        for (var i = 0, length = paramName.length; i < length; i++) {
            if (selfParam && selfParam.hasOwnProperty(paramName[i]))
                selfParam = selfParam[paramName[i]];
            else {
                selfParam = "";
                break;
            }
        }

        result = result.replace(new RegExp(param, 'g'), selfParam);
    });

    return result;
}

BasePage.getSchema().methods.getMetaTags = function () {
    var metaTag = _.clone(dbUtils.getList(this['__t']).options.metaTags);

    metaTag.title = _generateMetaTag(this.seoTitle, metaTag.title, this);
    metaTag.description = _generateMetaTag(this.seoDescription, metaTag.description, this);
    metaTag.keywords = _generateMetaTag(this.seoKeywords, metaTag.keywords, this);
    metaTag.image = _generateMetaTag("", metaTag.image, this);

    return metaTag;
}

BasePage.getSchema().index({ nameUrl: 1, __t: 1 }, { unique: true });

BasePage.getSchema().pre('save', function (next, done) {
    var isNew = this.isNew;
    var id = this._id.toString();
    
    dbUtils.getList(this['__t']).model.find({ nameUrl: this.nameUrl }).exec(function (err, result) {
        if (!err && ((isNew && result.length) || (!isNew && (result.length > 1 || (result.length && result[0].id != id)))))
            return done(new Error("nameUrl must be unique"));

        next();
    })
});


BasePage.register();

module.exports = BasePage;