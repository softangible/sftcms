var schedule = require('node-schedule'),
    dbUtils = require('../core/dbUtils');

function _log(err,name){
    
    var ScheduledJobLog = dbUtils.getList('ScheduledJobLog').model,
        log = new ScheduledJobLog({
            jobName : name,
            isSuccess: !err,
            exception : (err) ? JSON.stringify(err) : ''
        });
        
    log.save();
}

function scheduleJob(name, rule, fn){
    schedule.scheduleJob(rule,function(){
        try{
            fn(function(err){
                _log(err,name)
            });
        }
        catch(err){
            _log(err, name)
        }
    })
}

module.exports = {
    RecurrenceRule: schedule.RecurrenceRule,
    scheduleJob : scheduleJob
}