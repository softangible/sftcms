var ImageHandler = require('../core/image-handler'),
    SftMediaConfig = require('../core/config/config').get('SftMedia'), 
	config = SftMediaConfig ? SftMediaConfig.ImageHandler : null;
    

module.exports = function imageHandler(req, res) {
       if ((typeof req.params[0] == 'undefined') || (req.params[0].trim() == '')) {
		res.notfound();
		return;
	}
	
	ImageHandler.generate(config.originUri + '/' + req.params[0], req.params.options, function(err, stdout, mime) {
		if (err) {
			res.notfound();
			return;
		}
        
        res.setHeader('Cache-Control', 'public, max-age=' + config.cachePeriod || 604800);
		res.writeHead(200, {'Content-Type': mime});
		res.end(stdout, 'binary');
	});
};