var knox= require('knox-s3'),
    StorageConfig = require('../../core/config/config').get('Storage'),
    client;

if (StorageConfig && StorageConfig.S3Config)
    client = knox.createClient(StorageConfig.S3Config);


function _checkClient() {
    if (!client)
        throw "No S3 Configuration defined";
}

function UploadFile(filePath, remoteKey, cb) {
    _checkClient();
    client.putFile(filePath, remoteKey, function (err, res) {
        cb(err, res);
    });
}

function UploadBuffer(buffer, remoteKey, headers, cb) {
    _checkClient();
    client.putBuffer(buffer, remoteKey, headers, function (err, res) {
        cb(err, res);
    });
}

function DeleteFile(remoteKey, cb) {
    _checkClient();
    client.deleteFile(remoteKey, function (err, res) {
        cb(err, res);
    });
}

function ListAll(prefix, delimiter, cb) {
    _checkClient();
    delimiter = delimiter || '/';
    if (prefix.substr(-1) != '/') prefix += '/';

    var data = {
        CommonPrefixes: [],
        Contents: []
    }

    _listRecursive(prefix, delimiter, data, cb);
}

function List(prefix, delimiter, cb) {
    _checkClient();
    if (prefix.substr(-1) != '/') prefix += '/';
    var search = { prefix: prefix, delimiter: delimiter || '/' };
    _list(search, cb);
}

function Copy(originKey, destKey, cb) {
    _checkClient();
    _copy(originKey, destKey, cb)
}

function Move(originKey, destKey, cb) {
    _checkClient();
    _copy(originKey, destKey, function (err, res) {
        if (err) 
            return cb(err);
        if (res && res.statusCode && res.statusCode !== 200)
            return cb(new Error('Amazon returned Http Code: ' + res.statusCode));
        
        _removeMultiple(originKey,function(err,res2){
            if (err) return cb(err);
            if (res2 && res2.statusCode && res2.statusCode !== 200)
                return cb(new Error('Amazon returned Http Code: ' + res2.statusCode));
            
            return cb(null, {status : 'ok'});
        })
    })
}

function Delete(keys, cb) {
    _checkClient();
    _removeMultiple(keys, cb);
}

function _removeMultiple(keys, cb) {
    if ((typeof keys).toLowerCase() == 'string') {
        keys = [keys];
    }
    client.deleteMultiple(keys, cb);
}

function _copy(originKey, destKey, cb) {
    client.copyFile(originKey, destKey, cb);
}

function _list(search, cb) {
    client.list(search, cb);
}

function _listRecursive(prefix, delimiter, data, cb) {
    var search = { prefix: prefix, delimiter: delimiter };
    if (data && data.IsTruncated) {
        search.marker = data.Contents[data.Contents.length - 1].Key
    }

    _list(search, function (err, data2) {
        if (data && data2.CommonPrefixes && data.CommonPrefixes)
            data.CommonPrefixes = data.CommonPrefixes.concat(data2.CommonPrefixes)

        if (data && data2.Contents && data.Contents)
            data.Contents = data.Contents.concat(data2.Contents)

        data2.CommonPrefixes = data.CommonPrefixes;
        data2.Contents = data.Contents;

        if (data2.IsTruncated)
            _listRecursive(prefix, data2, cb);
        else
            cb(err, data2);
    });
}

module.exports = {
    UploadFile: UploadFile,
    DeleteFile: DeleteFile,
    UploadBuffer: UploadBuffer,
    ListAll: ListAll,
    List: List,
    Copy : Copy,
    Move : Move,
    Delete : Delete
}