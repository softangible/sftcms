var device = require('device');

function _getDevice(req, options){
    return device(req.get('user-agent'),options)
}

function isMobile (req,options){
    var device = _getDevice(req,options);
    return device.type == 'phone';
}

function isTablet (req,options){
    var device = _getDevice(req,options);
    return device.type == 'tablet';
}

function isDesktop (req,options){
    var device = _getDevice(req,options);
    return device.type == 'desktop';
}

module.exports={
    isMobile : isMobile,
    isTablet : isTablet,
    isDesktop : isDesktop
}