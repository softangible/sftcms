var keystone = require('keystone'),
    _ = require('lodash')
    utils = {
        device : require('./device'),
        enumToSelectOptions : require('./enum-to-select-options'),
        isFirstWorkerThread : require('./is-first-worker-thread')
    };
    
_.assignIn(keystone.utils,utils);

module.exports = keystone.utils;