var clusterConf = require('../core/config/config').getPluginConfig("Cluster");

module.exports = function () {
    return (
        (clusterConf && !clusterConf.active) || 
        parseInt(process.env.ThreadIndex)===0
        );
}