module.exports = function (obj) {
    var options = [];
    for (var key in obj){
        options.push({
            value : obj[key].id,
            label : obj[key].label || key
        })
    }
    return options;
}