var keystone = require('keystone');
var contextService = require('../context');
var multilingualConfig = require('../core/config/config').getPluginConfig('Multilingual') || {};
//var geoip = require('geoip-country-only');
var geoip = require('geoip-lite');
var config, langs, defaultLang;
var REDIRECT_CODE = 301

module.exports = function (req, res, next) {

    if (!config) {
        var excludePaths = multilingualConfig.excludePaths || [];
        excludePaths.push(keystone.get('admin path'))

        config = {
            allowDefaultLanguageRedirect: multilingualConfig.allowDefaultLanguageRedirect || false,
            excludePaths: excludePaths,
        }
    }

    if (!langs) {
        langs = keystone.get('langs').map(lang => {
            var dashIdx = lang.indexOf('-');
            var returnObj = { original: lang };
            if (dashIdx == -1) {
                returnObj.lang = lang;
                return returnObj;
            }

            returnObj.lang = lang.substring(0, dashIdx).toLowerCase();
            returnObj.region = lang.substring(dashIdx + 1, lang.length).toLowerCase();
            return returnObj;
        })
    }

    if (req.query.lang)
        return next();


    // If have a first param path
    var pathParts = req.path.split('/');

    if (pathParts.length > 1 && pathParts[1]) {
        if (config.excludePaths.indexOf(pathParts[1]) != -1) return next();

        var reqLocale = pathParts[1];

        for (var i = 0, length = langs.length; i < length; i++) {
            if (langs[i].original.toLowerCase() == reqLocale.toLowerCase()) {
                // set current request locale
                contextService.setLocale(langs[i].original);

                pathParts.splice(1, 1);

                // Continue reuqest using all parts after the locale path
                req.url = pathParts.join('/') || '/';
                return next();
            }
        }
    }

    // If auto redirect disabled continue;
    if (!config.allowDefaultLanguageRedirect)
        return next();

    // if (pathParts.length > 2 || (pathParts.length == 2 && pathParts[1]))
    //     // if request is not to root continue
    //     return next();

    // root path request redirect to preffered language locale or to default one 

    if (pathParts[1]) {
        var dashIdx = pathParts[1].indexOf('-');
        var tmpLang = (dashIdx == -1 ? pathParts[1] : pathParts[1].substr(0, dashIdx)).toLowerCase();

        for (var i = 0, length = langs.length; i < length; i++) {
            if (langs[i].lang == tmpLang) {
                return res.redirect(REDIRECT_CODE, '/' + langs[i].original + req.originalUrl);
                // return res.redirect(REDIRECT_CODE,  req.originalUrl.replace(pathParts[1], langs[i].original));
            }
        }

        return res.redirect(REDIRECT_CODE, '/' + langs[0].original + req.originalUrl);
        // return res.redirect(REDIRECT_CODE, req.originalUrl.replace(pathParts[1], langs[0].original));
    }


    var lang,
        acceptedLangs = req.acceptsLanguages();
    var geo = geoip.lookup(req.ip);



    if (acceptedLangs && acceptedLangs.length) {
        lang = acceptedLangs[0];
        var dashIndex = lang.indexOf('-');
        if (dashIndex != -1) {
            lang = lang.substr(0, dashIndex)
        }
    }

    req.lang = lang || '';
    req.region = geo.region ? geo.country : '';

    var compatibleLangs = [], compatibleLocales = [];
    langs.forEach(langObj => {
        if (langObj.lang == req.lang.toLowerCase()) {
            compatibleLangs.push(langObj);
            if (langObj.region && langObj.region == req.region.toLowerCase())
                compatibleLocales.push(langObj);
        }
    })

    var locale = compatibleLocales.length ? compatibleLocales[0] : (compatibleLangs.length ? compatibleLangs[0] : langs[0]);

    return res.redirect(REDIRECT_CODE, '/' + locale.original + req.originalUrl);
}