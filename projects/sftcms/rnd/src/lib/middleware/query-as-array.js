module.exports = function (req, res, next) {
    if (!req.query) return next();
    req.queryAsArray = {};

    for (var key in req.query) {
        req.queryAsArray[key] = Array.isArray(req.query[key]) ? req.query[key] : [req.query[key]];
    }

    next();
}