var keystone = require('keystone'),
    router = require('../router'),
    bodyParser = require('body-parser'),
    multer = require('multer');

module.exports = function(){
    var bodyParserParams = {};

    if (keystone.get('file limit')) {
		bodyParserParams.limit = keystone.get('file limit');
	}

    router.setAppUse(bodyParser.json(bodyParserParams))
    bodyParserParams.extended = true;
	router.setAppUse(bodyParser.urlencoded(bodyParserParams));
    
    router.setAppUse(multer({
		includeEmptyFields: true
	}));
}