var keystone = require('keystone');

var middleware = keystone.middleware;

middleware.auth = keystone.session.keystoneAuth;

middleware.langs = require('./langs');
middleware.queryAsArray = require('./query-as-array');

module.exports = middleware;