var fs = require('fs-extra'),
    path = require('path'),
    keystone = require('keystone'),
    keystoneDir = path.parse(require.resolve('keystone')).dir,
    keystoneStylesDir = path.join(keystoneDir, 'admin/public/styles'),
    keystoneLayoutsDir = path.join(keystoneDir, 'templates/layout');

function loadAdminLayout(fullPath){
    var destPath = path.join(keystoneLayoutsDir,'base.jade');
    fs.copySync(fullPath, destPath);
}

function loadAdminCss(fullPath){
    var destPath = path.join(keystoneStylesDir,path.parse(fullPath).base);
    fs.copySync(fullPath, destPath);
}

function overriteFile(originalPath, keystoneRelative){
    var destPath = path.join(keystoneDir,keystoneRelative);
    fs.copySync(originalPath, destPath);
}

module.exports = {
    loadAdminCss : loadAdminCss,
    loadAdminLayout : loadAdminLayout,
    overriteFile : overriteFile
}