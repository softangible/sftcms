var config = require('../core/config/config'),
    request = require('request');

function SendCrm(provider, inputs, cb) {
    
    if(!config.isPluginActive("Crm")){
        return cb({err:"Crm plugin is not active"})
    }
    
    var options = { url: provider.url, method: provider.method.toUpperCase() };
    if (provider.defaultHeader)
        options.headers = JSON.parse(provider.defaultHeader);

    if (provider.method == 'get')
        options.qs = inputs;
    else if (provider.method == 'post'){
        if (provider.bodyType && provider.bodyType.toLowerCase() == 'json')
            options.json = inputs
        else
            options.form = inputs
    }

    var requestDate = new Date(),
        req = request(options, requestCb);
        
        
    function requestCb(err, httpResponse, body){
        cb(err, req, requestDate, httpResponse, body);
    }
}

module.exports = {
    SendCrm : SendCrm
}