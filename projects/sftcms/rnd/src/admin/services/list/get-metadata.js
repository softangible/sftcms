var _ = require('underscore');
var keystone = require('keystone');

module.exports = function(l){
    var list = _.clone(_.isString(l) ? keystone.list(l) : l);

	for (var key in list.fields) {
		delete list.fields[key].list;
		delete list.fields[key].options.type;
		delete list.fields[key]._path;
		delete list.fields[key]._underscoreMethods;
	}
	delete list.model;
	delete list.pagination;
	delete list.uiElements;
	delete list.underscoreMethods;
	delete list.schema;

	delete list.options.inherits;
	delete list.options.schema;
	
    list.options = JSON.parse(JSON.stringify(list.options));

	if (list._defaultColumns){
		list._defaultColumns.forEach(function (col) {
			delete col.field;
		})
	}

	return list;
}