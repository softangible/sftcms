/*
 *  Altair Admin angularjs
 *  controller
 */
angular.module('sftCms')
    .controller('mainCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) { }])
    .controller('main_headerCtrl', ['$timeout', '$scope', '$window', '$state', 'AdminAPI', 'authorization',
        function ($timeout, $scope, $window, $state, AdminAPI, authorization) {

            $scope.user_data = {
                name: "Lue Feest",
                avatar: "img/avatars/avatar_11_tn.png",
                alerts: [
                    {
                        "title": "Hic expedita eaque.",
                        "content": "Nemo nemo voluptatem officia voluptatum minus.",
                        "type": "warning"
                    },
                    {
                        "title": "Voluptatibus sed eveniet.",
                        "content": "Tempora magnam aut ea.",
                        "type": "success"
                    },
                    {
                        "title": "Perferendis voluptatem explicabo.",
                        "content": "Enim et voluptatem maiores ab fugiat commodi aut repellendus.",
                        "type": "danger"
                    },
                    {
                        "title": "Quod minima ipsa.",
                        "content": "Vel dignissimos neque enim ad praesentium optio.",
                        "type": "primary"
                    }
                ],
                messages: [
                    {
                        "title": "Reiciendis aut rerum.",
                        "content": "In adipisci amet nostrum natus recusandae animi fugit consequatur.",
                        "sender": "Korbin Doyle",
                        "color": "cyan"
                    },
                    {
                        "title": "Tenetur commodi animi.",
                        "content": "Voluptate aut quis rerum laborum expedita qui eaque doloremque a corporis.",
                        "sender": "Alia Walter",
                        "color": "indigo",
                        "avatar": "img/avatars/avatar_07_tn.png"
                    },
                    {
                        "title": "At quia quis.",
                        "content": "Fugiat rerum aperiam et deleniti fugiat corporis incidunt aut enim et distinctio.",
                        "sender": "William Block",
                        "color": "light-green"
                    },
                    {
                        "title": "Incidunt sunt.",
                        "content": "Accusamus necessitatibus officia porro nisi consectetur dolorem.",
                        "sender": "Delilah Littel",
                        "color": "blue",
                        "avatar": "img/avatars/avatar_02_tn.png"
                    },
                    {
                        "title": "Porro ut.",
                        "content": "Est vitae magni eum expedita odit quisquam natus vel maiores.",
                        "sender": "Amira Hagenes",
                        "color": "amber",
                        "avatar": "img/avatars/avatar_09_tn.png"
                    }
                ]
            };

            $scope.alerts_length = $scope.user_data.alerts.length;
            $scope.messages_length = $scope.user_data.messages.length;


            $('#menu_top').children('[data-uk-dropdown]').on('show.uk.dropdown', function () {
                $timeout(function () {
                    $($window).resize();
                }, 280)
            });

            // autocomplete
            $('.header_main_search_form').on('click', '#autocomplete_results .item', function (e) {
                e.preventDefault();
                var $this = $(this);
                $state.go($this.attr('href'));
                $('.header_main_search_input').val('');
            })

            $scope.logout = function () {
                AdminAPI.SignOut();
            }

            $scope.goToProfile = function () {
                authorization.GoToProfile();
            }

        }
    ])
    .controller('main_sidebarCtrl', ['$translate','$timeout', '$scope', '$rootScope', 'AdminAPI', 'authorization', 'MenuService',
        function ($translate, $timeout, $scope, $rootScope, AdminAPI, authorization, MenuService) {
            var opened = false;
            $scope.$on('onLastRepeat', function (scope, element, attrs) {
                if (!opened) {
                    MenuService.ShowCurrent();
                    opened = true;
                }
            });

            // language switcher
            $scope.langSwitcherModel = 0;
            var langData = $scope.langSwitcherOptions = [
                { id: 0, title: 'Hebrew', value: 'IL', lang:'he' },
                { id: 1, title: 'English', value: 'US', lang:'en' }
            ];

            $scope.langSwitcherConfig = {
                maxItems: 1,
                render: {
                    option: function (langData, escape) {
                        return '<div class="option">' +
                            '<i class="item-icon flag-' + escape(langData.value).toUpperCase() + '"></i>' +
                            '<span>' + escape(langData.title) + '</span>' +
                            '</div>';
                    },
                    item: function (langData, escape) {
                        return '<div class="item"><i class="item-icon flag-' + escape(langData.value).toUpperCase() + '"></i></div>';
                    }
                },
                valueField: 'id',
                labelField: 'title',
                searchField: 'title',
                create: false,
                onInitialize: function (selectize) {
                    $('#lang_switcher').next().children('.selectize-input').find('input').attr('readonly', true);
                },
                onChange : function(value){
                    var chosenLang = $scope.langSwitcherOptions[parseInt(value)];
                    if (!chosenLang) return;
                    $translate.use(chosenLang.lang);
                }
            };

            $scope.dashboardSection = {
                id: 0,
                icon: '&#xE871;',
                link: 'restricted.dashboard'
            };

            $scope.profileSection = {
                    id: 6,
                    icon: '&#xE87C;',
                    click: function () {
                        authorization.GoToProfile();
                    },
                    isActive: function () {
                        return authorization.isCurrentEdit($scope.state.current.name, $scope.state.params);
                    }
                }
            
            // menu entries
            $scope.sections = [];
           
            function getAllInstances(listName, node) {
                return function (e, item) {
                    e.preventDefault();
                    e.stopPropagation();

                    if (node.gotInstances) {
                        MenuService.SubmenuToggle(e);
                        item.icon = node.isOpen ? 'keyboard_arrow_left' : 'keyboard_arrow_down'
                        node.isOpen = !node.isOpen;
                        return;
                    }

                    item.gotInstances = true;
                    item.loading = true;

                    AdminAPI.GetAllInstances(listName).then(function (res) {
                        var items = res.data.data.items;
                        items = items.map(function (item) {
                            return {
                                title: item.name,
                                id: item.id,
                                link: 'restricted.item({list: "' + listName + '", itemId: "' + item.id + '"})',
                                isActive : function(){
                                    return MenuService.isCurrentView($scope.state, listName, item.id);
                                }
                            }
                        });
                        item.loading = false;
                        item.submenu = (node.submenu ? node.submenu : []).concat(items);
                        item.icon = 'keyboard_arrow_down';
                        
                        $timeout(function () {
                            node.isOpen = true;
                            MenuService.SubmenuToggle(e);
                        })
                    });
                    console.log('trying to get all ' + listName);
                }
            }

            function getParentInstances(listName, node) {
                return function (e, item) {
                    e.preventDefault();
                    e.stopPropagation();

                    if (node.gotChilds) {
                        MenuService.SubmenuToggle(e);
                        node.isOpen = !node.isOpen;
                        item.icon = node.isOpen ? 'keyboard_arrow_down' : 'keyboard_arrow_left';
                        return;
                    }
                    node.gotChilds = true;
                    node.loading = true;

                    AdminAPI.GetByParent(listName, node.id).then(function (res) {
                        var items = res.data.data.items;
                        items = items.map(function (item) {
                            return {
                                title: item.name,
                                id: item.id,
                                tree: true,
                                icon: 'keyboard_arrow_left',
                                link: 'restricted.item({list: "' + listName + '", itemId: "' + item.id + '"})',
                                iconClick: getParentInstances(listName, item),
                                isActive: function () {
                                    return authorization.isCurrentEdit($scope.state.current.name, $scope.state.params);
                                }
                            }
                        });
                        node.loading = false;
                        item.submenu = (item.submenu || []).concat(items);
                        $timeout(function () {
                            node.isOpen = true;
                            if (!items.length) item.icon = '&#xE85D';
                            else item.icon = 'keyboard_arrow_down';
                            MenuService.SubmenuToggle(e);
                        })
                    });
                }
            }


            function formatList(list) {
                list.icon = list.tree ? 'keyboard_arrow_left' : "&#xE8D2;"
                list.isActive = function(){
                    return MenuService.isCurrentView($scope.state, list.path);        
                }
                    
                if (list.children) {
                    list.submenu = [];
                    if (list.children) {
                        list.children.forEach(function (child) {
                            formatList(child);
                        })
                        list.submenu = list.submenu.concat(list.children);
                    }
                } else {
                    list.link = 'restricted.list({list: "' + list.path + '", page: 1})';
                }

                if (list.tree || list.includeInstances) {
                    
                    list.hideSubmenu = true;

                    if (list.includeInstances) {
                        list.icon = "keyboard_arrow_left";
                        list.isOpen = false;
                        list.iconClick = getAllInstances(list.path, list);
                    }

                    if (list.tree) {
                        list.iconClick = getParentInstances(list.path, list);
                    }
                }
            }

            AdminAPI.GetNav().then(function (res) {
                var newSections = res.data.data.Content;
                newSections.submenu.forEach(function (section) {
                    if (section.submenu)
                        section.submenu.forEach(function (list) {
                            formatList(list);
                        });
                    else 
                        formatList(section);
                })

                $scope.sections = $scope.sections.concat(newSections);

                if(res.data.data.External){
                    var externalsSection = res.data.data.External;
                    externalsSection.submenu.forEach(function(item){
                        item.link = 'restricted.external({url: "' + item.path + '"})';
                        item.linkOpts = '{reload: true}';
                        item.isActive = function(){
                            return MenuService.isCurrentExternal($scope.state, item.path);        
                        }
                    })
                    $scope.sections.push(externalsSection)
                }
            });

        }
    ])
    ;
