/*
*  Altair Admin AngularJS
*/
;"use strict";

var sftCmsAdmin = angular.module('sftCms', [
    'ui.tinymce',
    'ngSanitize',
    'ngRetina',
    'ngMaterial',
    'ngMessages',
    'dndLists',
    'mdColorPicker',
    'sftCms.Services',
    'sftCms.Directives',
    'sftCms.Fields'
]);

/* Run Block */
sftCmsAdmin.run(['$rootScope','$state','$stateParams','$window','AdminAPI','uiGmapGoogleMapApiManualLoader','Settings',
        function ($rootScope, $state, $stateParams,$window,AdminAPI,uiGmapGoogleMapApiManualLoader,Settings) {

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            // fastclick (eliminate the 300ms delay between a physical tap and the firing of a click event on mobile browsers)
            FastClick.attach(document.body);

            $rootScope.appVer = "1.0.0";

            // modernizr
            $rootScope.Modernizr = Modernizr;

            // get window width
            var w = angular.element($window);
            $rootScope.largeScreen = w.width() >= 1220;

            w.on('resize', function() {
                return $rootScope.largeScreen = w.width() >= 1220;
            });

            // show/hide main menu on page load
            $rootScope.primarySidebarOpen = ($rootScope.largeScreen) ? true : false;

            $rootScope.pageLoading = true;
            
            AdminAPI.Load().then(function(config){
                angular.GoogleMapApiProviders.configure({
                    china: false,
                    key: config.google.ApiKey,
                    libraries: 'places',
                    language:'he'
                });

                document.title = config.global.AdminTitle || Settings.pageTitleBase;
                //uiGmapGoogleMapApiManualLoader.load();
            });
            
            // wave effects
            $window.Waves.init();
        }
])
