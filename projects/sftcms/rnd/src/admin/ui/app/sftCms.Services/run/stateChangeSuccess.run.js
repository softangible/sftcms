angular.module('sftCms.Services').run(['$rootScope','$window', '$timeout','MenuService', 
    function ($rootScope,$window, $timeout,MenuService) {

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        // scroll view to top
        $("html, body").animate({
            scrollTop: 0
        }, 200);

        $timeout(function () {
            $rootScope.pageLoading = false;
            // reinitialize uikit components
            $.UIkit.init($('body'));

            $($window).resize();
        }, 300);

        if(fromState.name){
            MenuService.ShowCurrent();
        }

        $timeout(function () {
            $rootScope.pageLoaded = true;
            $rootScope.appInitialized = true;
            // wave effects
            $window.Waves.attach('.md-btn-wave,.md-fab-wave', ['waves-button']);
            $window.Waves.attach('.md-btn-wave-light,.md-fab-wave-light', ['waves-button', 'waves-light']);

            // IE fixes
            if (typeof window.isLteIE9 != 'undefined') {
                $('svg,canvas,video').each(function () {
                    var $this = $(this),
                        height = $(this).attr('height');
                    if (height) {
                        $this.css('height', height);
                    }
                    if ($this.hasClass('peity')) {
                        $this.prev('span').peity()
                    }
                });
            }

        }, 600);

    });
}]);