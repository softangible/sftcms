angular.module('sftCms.Services').service('Notifications', ['$window',function($window) {
    var w = angular.element($window);
    
    function Show(msg, status, timeout,group, pos, cb){
        var thisNotify = UIkit.notify({
                            message: msg,
                            status: status || '',
                            timeout: timeout || 5000,
                            group: group || '',
                            pos: pos || 'top-center',
                            onClose: function() {
                                $('body').find('.md-fab-wrapper').css('margin-bottom','');
                                clearTimeout(thisNotify.timeout);

                                if(cb) {
                                    if( angular.isFunction(cb()) ) {
                                        scope.$apply(cb());
                                    } else {
                                        console.log('Callback is not a function');
                                    }
                                }
                            }
                        });
                        if(
                            ( (w.width() < 768) && (
                                (pos == 'bottom-right')
                                || (pos == 'bottom-left')
                                || (pos == 'bottom-center')
                            ) )
                            || (pos == 'bottom-left')
                        ) {
                            var thisNotify_height = $(thisNotify.element).outerHeight(),
                                spacer = (w.width() < 768) ? -6 : 8;
                            $('body').find('.md-fab-wrapper').css('margin-bottom',thisNotify_height + spacer);
                        }
    }
    
    return {
        Show : Show
    };
}]);