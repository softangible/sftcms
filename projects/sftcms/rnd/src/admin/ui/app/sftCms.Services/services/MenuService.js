angular.module('sftCms.Services').service('MenuService', ['$rootScope', '$timeout', 'variables','$state',
    function ($rootScope, $timeout, variables, $state) {

        return {
            ShowCurrent: ShowCurrent,
            SubmenuToggle: SubmenuToggle,
            isCurrentView : isCurrentView,
            isCurrentExternal : isCurrentExternal
        };

        //////////////////////////////////////////////////////////////////////////////

        function isCurrentView(state, list, id){
            return (
                (state.params.list==list && id && state.params.itemId == id) ||
                (state.params.list==list && !id)
            )
        }

        function isCurrentExternal(state, url){
            return (
                (state.params.url==url)
            )
        }

        function SubmenuToggle($event) {
            $event.preventDefault();
            var $this = $($event.currentTarget);
            $this.next("ul").length || ($this = $this.closest("a"));
            var $sidebar_main = $("#sidebar_main")
                , slideToogle = $this.next("ul").is(":visible") ? "slideUp" : "slideDown";
            $this.next("ul").velocity(slideToogle, {
                duration: 400,
                easing: variables.easing_swiftOut,
                begin: function () {
                    "slideUp" == slideToogle ? $(this).closest(".submenu_trigger").removeClass("act_section") : ($rootScope.menuAccordionMode && $this.closest("li").siblings(".submenu_trigger").each(function () {
                        $(this).children("ul").velocity("slideUp", {
                            duration: 500,
                            easing: variables.easing_swiftOut,
                            begin: function () {
                                $(this).closest(".submenu_trigger").removeClass("act_section")
                            }
                        })
                    }),
                        $(this).closest(".submenu_trigger").addClass("act_section"))
                },
                complete: function () {
                    if ("slideUp" !== slideToogle) {
                        var scrollContainer = $sidebar_main.find(".scroll-content").length ? $sidebar_main.find(".scroll-content") : $sidebar_main.find(".scrollbar-inner");
                        $this.closest(".act_section").velocity("scroll", {
                            duration: 500,
                            easing: variables.easing_swiftOut,
                            container: scrollContainer
                        })
                    }
                }
            })
        }

        function ShowCurrent() {
            $timeout(function () {
                if (!$rootScope.miniSidebarActive) {
                    var anchors = $('.act_item').parents('.submenu_trigger').find('>a');
                    for (var i = 0, length = anchors.length; i < length; i++) {
                        var $anchor = $(anchors[i]);
                        if (!$anchor.next('ul').is(':visible'))
                            $anchor.trigger('click');
                    }

                    var mainContentBranch = $('.menu_section>ul>li:not(".current_section")>a:contains("Content")');
                    if (!mainContentBranch.next('ul').is(':visible'))
                        mainContentBranch.trigger('click');

                } else {
                    // add tooltips to mini sidebar
                    var tooltip_elem = $('#sidebar_main').find('.menu_tooltip');
                    tooltip_elem.each(function () {
                        var $this = $(this);

                        $this.attr('title', $this.find('.menu_title').text());
                        UIkit.tooltip($this, {
                            pos: 'left'
                        });
                    });
                }
            })
        }
    }]);