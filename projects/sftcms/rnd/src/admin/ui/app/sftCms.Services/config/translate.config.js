angular.module('sftCms.Services').config(['$translateProvider',
    function ($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix: 'i18n/',
            suffix: '.json'
        });

        // Since you've now registered more then one translation table, angular-translate has to know which one to use.
        // This is where preferredLanguage(langKey) comes in.
        $translateProvider.preferredLanguage('he');

        // Store the language in the local storage
        //$translateProvider.useLocalStorage();
    }]);