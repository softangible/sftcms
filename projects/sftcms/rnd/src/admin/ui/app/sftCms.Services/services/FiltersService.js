angular.module('sftCms.Services').service('FiltersService', [function() {
    
    function escapeRegExp (str) {
        if (str && str.toString) str = str.toString();
        if (!angular.isString(str) || !str.length) return '';
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    };
    
    function GetStringFilter(mode, text){
        if(mode.id==2 && !text)
            return { $in: ['', null] };

        var value = escapeRegExp(text);
        if (mode.id === 2) {
            return value;
        }
                        
        return { "$regex": value.toString(), "$options": "i" };
    }
    
    return {
        GetStringFilter: GetStringFilter
    };
}]);