angular.module('sftCms.Services').factory('utils', ['$rootScope', function ($rootScope) {
    return {
        // Util for finding an object by its 'id' property among an array
        findByItemId: findByItemId,
        // serialize form
        serializeObject: serializeObject,
        // high density test
        isHighDensity: isHighDensity,
        // touch device test
        isTouchDevice: isTouchDevice,
        // local storage test
        lsTest: lsTest,
        // show/hide card
        card_show_hide: card_show_hide,
        preventLeave : preventLeave
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function preventLeave() {
        $rootScope.pageLoading = false;
        $rootScope.pageLoaded = true;
    }

    function findByItemId(a, id) {
        for (var i = 0; i < a.length; i++) {
            if (a[i].item_id == id) return a[i];
        }
        return null;
    }

    function serializeObject(form) {
        var o = {};
        var a = form.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }

    function isHighDensity() {
        return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 1.3));
    }

    function isTouchDevice() {
        return !!('ontouchstart' in window);
    }

    function lsTest() {
        var test = 'test';
        try {
            localStorage.setItem(test, test);
            localStorage.removeItem(test);
            return true;
        } catch (e) {
            return false;
        }
    }

    function card_show_hide(card, begin_callback, complete_callback, callback_element) {
        $(card).velocity({
            scale: 0,
            opacity: 0.2
        }, {
                duration: 400,
                easing: [0.4, 0, 0.2, 1],
                // on begin callback
                begin: function () {
                    if (typeof begin_callback !== 'undefined') {
                        begin_callback(callback_element);
                    }
                },
                // on complete callback
                complete: function () {
                    if (typeof complete_callback !== 'undefined') {
                        complete_callback(callback_element);
                    }
                }
            }).velocity('reverse');
    }

}]);