angular.module('sftCms.Services').service('AdminAPI', ['$http', '$state', 'API', '$q', function ($http, $state, API, $q) {
    var config;

    function Load() {
        return $http.get(API.Load).then(function (res) {
            config = res.data.data;
            return res.data.data;
        });
    }

    function GetConfig(key) {
        if (config[key])
            return config[key];

        console.log('No ' + key + ' key in the config');
    }

    function SignIn(model) {
        return $http.post(API.SignIn, model);
    }

    function SignOut() {
        $http.post(API.SignOut).then(function () {
            $state.go('login');
        });
    }

    function isAuthenticated() {
        return $http.get(API.IsAuthenticated);
    }

    function GetNav() {
        return $http.get(API.GetNav);
    }

    function GetListFull(listName, pageNumber, filters, sort) {
        var url = 'fullList/' + listName + '/' + (pageNumber || 1) + '?';
        if (sort) url += 'sort=' + (sort.inv ? '-' + sort.path : sort.path)
        return $http.post(url, { filters: filters });
    }

    function GetListMetaData(listName) {
        var url = 'listMeatData/' + listName;
        return $http.get(url).then(function (res) {
            return res.data.data;
        });
    }

    function GetAllInstances(listName) {
        return $http.get(listName + '/getAllInstances');
    }

    function GetByParent(listName, parent) {
        var url = listName + '/parent';
        if (parent)
            url += '?parentId=' + parent;
        return $http.get(url);
    }

    function GetListPage(listName, pageNumber, filters, sort, search) {
        var url = 'getPage/' + listName + '/' + (pageNumber || 1) + '?';
        if (sort) url += 'sort=' + (sort.inv ? '-' + sort.path : sort.path) + '&'
        if (search) url += 'search=' + search;

        return $http.post(url, { filters: filters });
    }

    function DownloadList(listName, filters, sort, cols) {
        var url = 'download/' + listName;
        if (sort) url += '?sort=' + (sort.inv ? '-' + sort.path : sort.path);

        $http.post(url, { filters: filters, cols: cols }).then(function (res) {
            var contentDisParts = res.headers('Content-Disposition').split('='),
                fileName = contentDisParts[contentDisParts.length - 1].replace(new RegExp('"', 'g'), '');


            var blob = new Blob(['\ufeff' + res.data], { encoding: "UTF-8", type: 'text/csv;charset=UTF-8' });
            if (window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(blob, fileName);
            }
            else {
                var anchor = angular.element('<a/>');

                anchor.attr({
                    href: window.URL.createObjectURL(blob),
                    target: '_blank',
                    download: fileName
                })[0].click();
            }
        });
    }

    function GetItem(listName, itemId) {
        return $http.get(listName + '/' + itemId);
    }

    function RevertToPublished(listName, itemId) {
        return $http.get(listName + '/' + itemId + '/revert-to-published');
    }

    function GetManyItemsBasic(listName, ids) {
        return $http.post(listName + '/getMultiple', { ids: ids });
    }

    function SaveOrUpdateItem(action, listName, data, relation, isForce) {

        var url = listName + '/saveOrUpdate';
        var query = [];

        if (relation) {
            query.push({ key: 'related', value: relation.refPath })
        }

        if (isForce) {
            query.push({ key: 'force', value: true })
        }

        if (query.length) {
            url += '?';
            query.forEach(function (q) {
                url += q.key + '=' + q.value;
            })
        }

        return $http.post(url, { action: action, data: data });
    }

    function GetNamesById(listName, ids) {
        if (!angular.isArray(ids)) ids = [ids];

        return $http.post(listName + '/getNames/' + itemId);
    }

    function DeleteItems(listName, ids) {
        return $http.post(listName + '/delete', { ids: ids });
    }

    function getRelatedItem(refPath, listPath, id) {
        return $q(function (resolve, reject) {
            $http.get('getRelation/' + listPath + '/' + id).then(function (res) {
                resolve({
                    refPath: refPath,
                    val: res.data.data
                })
            }, function (err) {
                reject(res);
            })
        });
    }


    function FilterRelationShipAutocomplete(listName, originList, field, query, existIds, showAll, filters) {
        query = query || '';

        var url = listName + '/autocomplete';
        var params = {
            q: encodeURI(query),
            limit: 10,
            page: 1,
            list: originList,
            context: 'relationship',
            field: field
        };

        if (filters)
            url += '?' + filters;

        var options = {
            url: url,
            method: "GET",
            params: params
        }

        // remove already selected ids
        if (existIds) {
            if (!Array.isArray(existIds))
                existIds = [existIds];
            options.params.existIds = existIds;
        }

        if (showAll)
            options.params.showAll = 'true';

        return $http(options);
    }

    return {
        Load: Load,
        GetConfig: GetConfig,
        SignIn: SignIn,
        SignOut: SignOut,
        GetNav: GetNav,
        GetListMetaData: GetListMetaData,
        GetListFull: GetListFull,
        GetListPage: GetListPage,
        DeleteItems: DeleteItems,
        DownloadList: DownloadList,
        GetAllInstances: GetAllInstances,
        GetByParent: GetByParent,
        GetItem: GetItem,
        getRelatedItem: getRelatedItem,
        GetManyItemsBasic: GetManyItemsBasic,
        SaveOrUpdateItem: SaveOrUpdateItem,
        RevertToPublished: RevertToPublished,
        FilterRelationShipAutocomplete: FilterRelationShipAutocomplete,
        isAuthenticated: isAuthenticated
    };
}]);