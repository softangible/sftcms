angular.module('sftCms.Services').run(['$rootScope','$window', 'authorization','$state', function ($rootScope,$window, authorization,$state) {
    
    $rootScope.state = $state;
    
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        // main search
        $rootScope.mainSearchActive = false;
        // single card
        $rootScope.headerDoubleHeightActive = false;
        // top bar
        $rootScope.toBarActive = false;
        // page heading
        $rootScope.pageHeadingActive = false;
        // top menu
        $rootScope.topMenuActive = false;
        // full header
        $rootScope.fullHeaderActive = false;
        // full height
        $rootScope.page_full_height = false;
        // secondary sidebar
        $rootScope.sidebar_secondary = false;
        $rootScope.secondarySidebarHiddenLarge = false;
        // footer
        $rootScope.footerActive = false;

        if ($($window).width() < 1220) {
            // hide primary sidebar
            $rootScope.primarySidebarActive = false;
            $rootScope.hide_content_sidebar = false;
        }
        if (!toParams.hasOwnProperty('hidePreloader')) {
            $rootScope.pageLoading = true;
            $rootScope.pageLoaded = false;
        }

        

        if (toState.name.indexOf('restricted') != -1) {
            
            authorization.Authorize();
        }
    });
}]);