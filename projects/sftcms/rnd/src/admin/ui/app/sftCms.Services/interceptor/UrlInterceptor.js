angular.module('sftCms.Services').factory('UrlInterceptor', ['Settings','$q',function UrlInterceptor(Settings,$q) {
    var csrf = '';
    var service = {
      request: function(config) {
        if (_.endsWith(config.url,'.html') || _.endsWith(config.url,'.json')|| _.endsWith(config.url,'.svg')) return config;
        
        config.headers['x-csrf-token'] = csrf;
        config.url = config.url[0]=='/' ? config.url : Settings.BaseApiUrl + config.url
        return config;
      },

      responseError:function(rejection){
        return $q.reject(rejection);
      },

      response:function(res){
        return $q(function(resolve, reject) {
          if (res.data && res.data.csrf)
            csrf = res.data.csrf;
          //  reject(res.data.response.message);
          //else
          resolve(res);
        });
      }
    };
    return service;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
}]);