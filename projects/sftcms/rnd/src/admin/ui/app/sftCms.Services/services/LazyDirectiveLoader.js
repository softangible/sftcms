angular.module('sftCms.Services').service('LazyDirectiveLoader', ['$rootScope', '$q',function($rootScope, $q) {
    
    function _load(filePath) {
        var deferred = $q.defer();
        var directiveFile = filePath;

        // download the javascript file
        var script = document.createElement('script');
        script.src = directiveFile;
        script.onload = function() {
            $rootScope.$apply(deferred.resolve);
        };
        document.getElementsByTagName('head')[0].appendChild(script);

        return deferred.promise;
    };
    
    return {
        load: _load,
    };
}]);