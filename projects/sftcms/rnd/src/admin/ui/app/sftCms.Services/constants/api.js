angular.module('sftCms.Services').constant("API", {
    Load : 'load',
    SignIn : 'signin',
    SignOut : 'signout',
    IsAuthenticated : 'isAuthenticated',
    GetNav : 'getNav'
  });