/*
*  Altair Admin AngularJS
*/
;"use strict";

angular.module('sftCms.Services', [
    'ui.router',
    'ncy-angular-breadcrumb',
    'uiGmapgoogle-maps',
    'oc.lazyLoad',
    'ngStorage',
    'ngMaterial',
    'pascalprecht.translate'
]);
