angular.module('sftCms.Services').config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
    $urlRouterProvider
        .when('/dashboard', '/')
        .otherwise('/');

    $stateProvider
        // -- ERROR PAGES --
        .state("error", {
            url: "/error",
            templateUrl: 'app/views/error.html',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'lazy_uikit'
                    ]);
                }]
            }
        })
        .state("error.404", {
            url: "/404",
            templateUrl: 'app/components/pages/error_404View.html'
        })
        .state("error.500", {
            url: "/500",
            templateUrl: 'app/components/pages/error_500View.html'
        })
        // -- LOGIN PAGE --
        .state("login", {
            url: "/login",
            templateUrl: 'app/sftCms.Web/login/loginView.html',
            controller: 'loginCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'lazy_uikit',
                        'lazy_iCheck'
                    ]);
                }]
            }
        })
        // -- RESTRICTED --
        .state("restricted", {
            abstract: true,
            url: "",
            views: {
                'main_header': {
                    templateUrl: 'app/shared/header/headerView.html',
                    controller: 'main_headerCtrl'
                },
                'main_sidebar': {
                    templateUrl: 'app/shared/main_sidebar/main_sidebarView.html',
                    controller: 'main_sidebarCtrl'
                },
                '': {
                    templateUrl: 'app/views/restricted.html'
                }
            },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'lazy_uikit',
                        'lazy_selectizeJS',
                        'lazy_switchery',
                        'lazy_prismJS',
                        'lazy_autosize',
                        'lazy_iCheck',
                        'lazy_themes',
                        'lazy_style_switcher'
                    ]);
                }],
                authorize: ['authorization', function (authorization) {
                    return authorization.Authorize();
                }]
            }
        })
        // -- DASHBOARD --
        .state("restricted.dashboard", {
            url: "/",
            templateUrl: 'app/sftCms.Web/dashboard/dashboardView.html',
            controller: 'dashboardCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        // ocLazyLoad config (app/app.js)
                        'lazy_countUp',
                        'lazy_charts_peity',
                        'lazy_charts_easypiechart',
                        'lazy_charts_metricsgraphics',
                        'lazy_charts_chartist',
                        'lazy_weathericons',
                        'lazy_google_maps',
                        'lazy_clndr'
                    ], { serie: true });
                }]
            },
            data: {
                pageTitle: 'Dashboard'
            },
            ncyBreadcrumb: {
                label: 'Home'
            }
        })
        .state("restricted.external", {
            url: "/external/:url",
            templateUrl: 'app/sftCms.Web/external/externalView.html',
            controller: 'externalCtrl'
        })
        .state("restricted.list", {
            url: "/:list/{page:[0-9]{1,8}}",
            templateUrl: 'app/sftCms.Web/list/listView.html',
            controller: 'listCtrl',
            params: {
                filters: null,
                page: "1"
            },
            resolve: {
                list: ['$stateParams', '$q', 'AdminAPI', '$localStorage',
                function ($stateParams, $q, AdminAPI,$localStorage) {
                    var deferred = $q.defer();

                    var sort;
                    if ($localStorage[$stateParams.list])
                        sort = $localStorage[$stateParams.list].currentSort;

                    AdminAPI.GetListFull($stateParams.list, $stateParams.page, $stateParams.filters,sort).then(function (res) {
                        deferred.resolve(res.data.data);
                    });

                    return deferred.promise;
                }]
            }
        })
        .state("restricted.item", {
            url: "/:list/:itemId",
            templateUrl: 'app/sftCms.Web/item/itemView.html',
            controller: 'itemCtrl',
            resolve: {
                list: ['$stateParams', '$q', 'AdminAPI', function ($stateParams, $q, AdminAPI) {
                    return AdminAPI.GetListMetaData($stateParams.list);
                }],
                item: ['$stateParams', '$q', 'AdminAPI', function ($stateParams, $q, AdminAPI) {
                    var deferred = $q.defer();

                    AdminAPI.GetItem($stateParams.list, $stateParams.itemId).then(function (res) {
                        //if (!(res.data.data.fields.createdAt && res.data.data.fields.createdBy && res.data.data.fields.updatedAt && res.data.data.fields.updatedBy))
                            
                            
                        /*res.data.data.track = {};
                        if (res.data.data.fields.createdBy && res.data.data.fields.createdBy._id)
                            res.data.data.track.createdBy = res.data.data.fields.createdBy;
                            res.data.data.track.createdAt = res.data.data.fields.createdAt;
                            delete res.data.data.fields.createdBy;
                            delete res.data.data.fields.createdAt;
                        if (res.data.data.fields.updatedBy && res.data.data.fields.updatedBy._id)
                            res.data.data.track.updatedBy = res.data.data.fields.updatedBy;
                            res.data.data.track.updatedAt = res.data.data.fields.updatedAt;
                            delete res.data.data.fields.updatedBy;
                            delete res.data.data.fields.updatedAt;*/
                        deferred.resolve(res.data.data);
                    });

                    return deferred.promise;
                }]
            }
        })
}]);