angular.module('sftCms.Services').config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        debug: false,
        events: false,
        modules: [
            // ----------- UIKIT ------------------
            {
                name: 'lazy_uikit',
                files: [
                    'js/lazy/lazy_uikit.min.js'],
                serie: true
            },

            // ----------- FORM ELEMENTS -----------
            {
                name: 'lazy_autosize',
                files: ['js/lazy/lazy_autosize.min.js'],
                serie: true
            },
            {
                name: 'lazy_iCheck',
                files: ['js/lazy/lazy_iCheck.min.js'],
                serie: true
            },
            {
                name: 'lazy_selectizeJS',
                files: ['js/lazy/lazy_selectizeJS.min.js'],
                serie: true
            },
            {
                name: 'lazy_switchery',
                files: ['js/lazy/lazy_switchery.min.js'],
                serie: true
            },
            {
                name: 'lazy_ionRangeSlider',
                files: ['js/lazy/lazy_ionRangeSlider.min.js',
                    'css/lazy/lazy_ionRangeSlider.min.css'],
                serie: true
            },
            {
                name: 'lazy_masked_inputs',
                files: ['js/lazy/lazy_masked_inputs.min.js',
                    'css/lazy/lazy_masked_inputs.min.css'],
            },
            {
                name: 'lazy_character_counter',
                files: ['js/lazy/lazy_character_counter.min.js',
                    'css/lazy/lazy_character_counter.min.css'],
            },
            {
                name: 'lazy_parsleyjs',
                files: ['js/lazy/lazy_parsleyjs.min.js',
                    'css/lazy/lazy_parsleyjs.min.css'],
                serie: true
            },
            {
                name: 'lazy_wizard',
                files: ['js/lazy/lazy_wizard.min.js',
                    'css/lazy/lazy_wizard.min.css'],
                serie: true
            },
            {
                name: 'lazy_ckeditor',
                files: ['js/lazy/lazy_ckeditor.min.js',
                    'css/lazy/lazy_ckeditor.min.css'],
                serie: true
            },
            {
                name: 'lazy_tinymce',
                files: ['js/lazy/lazy_tinymce.min.js',
                    'css/lazy/lazy_tinymce.min.css'],
                serie: true
            },

            // ----------- CHARTS -----------
            {
                name: 'lazy_charts_chartist',
                files: ['js/lazy/lazy_charts_chartist.min.js',
                    'css/lazy/lazy_charts_chartist.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },
            {
                name: 'lazy_charts_easypiechart',
                files: ['js/lazy/lazy_charts_easypiechart.min.js'],
            },
            {
                name: 'lazy_charts_metricsgraphics',
                files: ['js/lazy/lazy_charts_metricsgraphics.min.js',
                    'css/lazy/lazy_charts_metricsgraphics.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },
            {
                name: 'lazy_charts_c3',
                files: ['js/lazy/lazy_charts_c3.min.js',
                    'css/lazy/lazy_charts_c3.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },
            {
                name: 'lazy_charts_peity',
                files: ['js/lazy/lazy_charts_peity.min.js'],
                serie: true
            },

            // ----------- COMPONENTS -----------
            {
                name: 'lazy_countUp',
                files: ['js/lazy/lazy_countUp.min.js'],
                serie: true
            },
            {
                name: 'lazy_clndr',
                files: ['js/lazy/lazy_clndr.min.js'],
                serie: true
            },
            {
                name: 'lazy_google_maps',
                files: ['js/lazy/lazy_google_maps.min.js'],
                serie: true
            },
            {
                name: 'lazy_weathericons',
                files: ['css/lazy/lazy_weathericons.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },
            {
                name: 'lazy_prismJS',
                files: ['js/lazy/lazy_prismJS.min.js'],
                serie: true
            },
            {
                name: 'lazy_dragula',
                files: ['js/lazy/lazy_dragula.min.js',
                    'css/lazy/lazy_dragula.min.css'],
            },
            {
                name: 'lazy_pagination',
                files: ['js/lazy/lazy_pagination.min.js',
                    'css/lazy/lazy_pagination.min.css'],
            },
            {
                name: 'lazy_diff',
                files: ['js/lazy/lazy_diff.min.js',
                    'css/lazy/lazy_diff.min.css'],
            },

            // ----------- PLUGINS -----------
            {
                name: 'lazy_fullcalendar',
                files: ['js/lazy/lazy_fullcalendar.min.js',
                    'css/lazy/lazy_fullcalendar.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },
            {
                name: 'lazy_codemirror',
                files: ['js/lazy/lazy_codemirror.min.js',
                    'css/lazy/lazy_codemirror.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },
            {
                name: 'lazy_datatables',
                files: ['js/lazy/lazy_datatables.min.js',
                    'css/lazy/lazy_datatables.min.css'],
                serie: true
            },
            {
                name: 'lazy_gantt_chart',
                files: ['js/lazy/lazy_gantt_chart.min.js',
                    'css/lazy/lazy_gantt_chart.min.css'],
                serie: true
            },
            {
                name: 'lazy_tablesorter',
                files: ['js/lazy/lazy_tablesorter.min.js',
                    'css/lazy/lazy_tablesorter.min.css'],
                serie: true
            },
            {
                name: 'lazy_vector_maps',
                files: ['js/lazy/lazy_vector_maps.min.js',
                    'css/lazy/lazy_vector_maps.min.css'],
                serie: true
            },
            {
                name: 'lazy_dropify',
                files: ['js/lazy/lazy_dropify.min.js',
                    'css/lazy/lazy_dropify.min.css'],
                insertBefore: '#main_stylesheet'
            },
            {
                name: 'lazy_tree',
                files: ['js/lazy/lazy_tree.min.js',
                    'css/lazy/lazy_tree.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },
            {
                name: 'lazy_idle_timeout',
                files: ['js/lazy/lazy_idle_timeout.min.js',
                    'css/lazy/lazy_idle_timeout.min.css'],
            },

            // ----------- KENDOUI COMPONENTS -----------
            {
                name: 'lazy_KendoUI',
                files: ['js/lazy/lazy_KendoUI.min.js',
                    'css/lazy/lazy_KendoUI.min.css'],
                insertBefore: '#main_stylesheet',
                serie: true
            },

            // ----------- UIKIT HTMLEDITOR -----------
            {
                name: 'lazy_htmleditor',
                files: ['js/lazy/lazy_htmleditor.min.js',
                    'css/lazy/lazy_htmleditor.min.css'],
                serie: true
            },

            // ----------- THEMES -------------------
            {
                name: 'lazy_themes',
                files: ['css/lazy/lazy_themes.min.css'],
            },

            // ----------- STYLE SWITCHER -----------
            {
                name: 'lazy_style_switcher',
                files: ['js/lazy/lazy_style_switcher.min.js',
                    'css/lazy/lazy_style_switcher.min.css'],
                insertBefore: '#main_stylesheet',
            }

        ]
    })
}
]);