angular.module('sftCms.Services').service('authorization', ['$rootScope', '$state','AdminAPI',
  function($rootScope, $state, AdminAPI) {
    
    var authData;
    
    function Authorize(toState) {
        return AdminAPI.isAuthenticated().then(function(res) {
            if (!res.data.data.isAuth)
                return $state.go('login');
                
            authData = res.data.data;
        });
    }
    
    function GetUserId(){
        return authData.userId;
    }
    
    
    function GoToProfile() {
        if (!authData || !authData.isAuth) return;
        
        var params = {
            list : authData.userModelPath,
            itemId : authData.userId
        };
        
        $state.go('restricted.item',params);
    }

    function isCurrentEdit(state, stateParams){
        return (state.name=='restricted.item' && stateParams.list==authData.userModelPath && stateParams.itemId == authData.userId)
    }
    
    
    
    return {
      Authorize : Authorize,
      GoToProfile : GoToProfile,
      GetUserId : GetUserId,
      isCurrentEdit : isCurrentEdit
    };
 
}]);