angular.module('sftCms.Services').service('Popups', ['$mdDialog', 'AdminAPI', function ($mdDialog, AdminAPI) {

    function DeleteItem(listName, itemId, event, deleteCb) {
        var confirm = $mdDialog.confirm()
            .title('Are you sure you want to delete?')
            .textContent('Deleting this item is not reversable and all your data will get lost')
            .ariaLabel('Delete Item')
            .ok('Delete')
            .cancel('Cancel');

        if (event)
            confirm.targetEvent(event);

        $mdDialog.show(confirm).then(function (result) {
            AdminAPI.DeleteItems(listName, [itemId]).then(function () {
                deleteCb();
            }, function (res) {
                DisplayErrors(event, res.data);
            });
        });
    }

    function DisplayErrors(e, errors) {
        $mdDialog.show({
            controller: 'errorsModalCtrl',
            templateUrl: 'app/sftCms.Web/item/errorsModal.html',
            parent: angular.element(document.body),
            targetEvent: e,
            clickOutsideToClose: false,
            fullscreen: false,
            flex: "50",
            locals: {
                errors: errors
            }
        });
    }


    function ShowInitial(e, list, relation, cb) {
        $mdDialog.show({
            controller: 'initialCtrl',
            templateUrl: 'app/sftCms.Web/modals/initial/initial.html',
            parent: angular.element(document.body),
            targetEvent: e,
            clickOutsideToClose: false,
            fullscreen: false,
            multiple: true,
            flex: "50",
            locals: {
                list: list,
                relation: relation
            }
        }).then(function (newItem) {
            if (cb) cb(newItem);
        });
    }

    function SavePopup(e, popup) {
        if (popup.type == 'confirm')
            return _confirm(popup, e);
        else if (popup.type == 'alert')
            return _alert(popup, e);
    }

    function _alert(popup, e) {
        var modal = $mdDialog.alert({
            title: popup.title,
            textContent: popup.content,
            ok: popup.okBtnText
        });

        return $mdDialog.show(modal);
    }

    function _confirm(popup) {
        var modal = $mdDialog.confirm()
            .title(popup.title)
            .textContent(popup.content)
            .ariaLabel('ConfirmPopup')
            .ok(popup.saveBtnText)
            .cancel(popup.cancelBtnText)

        return $mdDialog.show(modal);
    }

    return {
        DeleteItem: DeleteItem,
        DisplayErrors: DisplayErrors,
        ShowInitial: ShowInitial,
        SavePopup: SavePopup
    };
}]);