angular.module('sftCms.Services').service('Elfinder', ['$mdDialog',function($mdDialog) {
    
    function OpenSelector(event,isMultiple, selectCb){
        var dialogOptions = {
            controller: 'elfinderCtrl',
            templateUrl: 'app/sftCms.Web/elfinder/elfinder.html',
            parent: angular.element(document.body),
            skipHide: true,
            clickOutsideToClose:true,
            fullscreen: false,
            flex:"80",
            locals: {
                options : {
                    isMultiple : isMultiple
                }
            }
        };
        
        if (event)
            dialogOptions.targetEvent = event

        $('body').addClass('elfinder-dialog-open');

        $mdDialog.show(dialogOptions)
        .then(function(data) {
            $('body').removeClass('elfinder-dialog-open');
            var result;
            if (isMultiple) 
                result = data.map(function (image) {
                    return {
                        src: image.relativeUrl,
                        title: ''
                    };
                });
            else {
                result = {
                    src : data.relativeUrl
                }
            }

            if (selectCb && angular.isFunction(selectCb))
                selectCb(result)
        }, function(){
            $('body').removeClass('elfinder-dialog-open');
        });
    }
    
    return {
        OpenSelector : OpenSelector
    };
}]);