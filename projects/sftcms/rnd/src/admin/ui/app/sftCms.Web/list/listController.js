angular.module('sftCms').controller('listCtrl', [
    '$scope', '$stateParams', '$state', '$mdDialog', 'AdminAPI', 'list', 'Popups', '$timeout', '$localStorage',
    function ($scope, $stateParams, $state, $mdDialog, AdminAPI, list, Popups, $timeout, $localStorage) {
        $scope.list = list;
        $scope.items = list.items;
        $scope.columns = list.columns;

        $scope.clearFilters = clearFilters;
        $scope.removeFilter = removeFilter;
        $scope.download = download;
        $scope.runFilter = runFilter;
        $scope.goToItem = goToItem;
        $scope.openInitial = openInitial;
        $scope.deleteItem = deleteItem;
        $scope.isActivePage = isActivePage;
        $scope.getListPage = getListPage;
        $scope.setSort = setSort;

        $scope.currentSort;

        $scope.availableColumns = [];
        $scope.availableFilters = [];
        $scope.availableSorts = [];

        $scope.filter = {
            searchText: '',
            filterObj: {}
        }

        $scope.$storage = $localStorage;
        $scope.storageKey = $stateParams.list;

        // Set Localstorage save
        if (!$scope.$storage[$scope.storageKey])
            $scope.$storage[$scope.storageKey] = {
                filters: {
                    currentFilters: [],
                    selectedFilters: []
                },
                selectedColumns: $scope.columns.map(function (col) {
                    return col.path;
                })
            }


        for (var key in list.list.fields) {
            if(list.list.fields[key].options && list.list.fields[key].options.lang){
                list.list.fields[key].label = list.list.fields[key].label+' ('+ list.list.fields[key].options.lang +')';
            }
            if (!list.list.fields[key].options.hidden){
                $scope.availableColumns.push(list.list.fields[key]);
                if (!list.list.fields[key].options.nofilter)
                    $scope.availableFilters.push(list.list.fields[key]);
            }
            if (list.sort.path == list.list.fields[key].path && !$scope.$storage[$scope.storageKey].currentSort)
                $scope.$storage[$scope.storageKey].currentSort = {
                    path: list.list.fields[key].path,
                    label: list.list.fields[key].label,
                    inv: list.sort.inv
                };
        }

        $timeout(function () {
            $(".filters .selectize-input.items").off('mousedown');
        })

        function setSort(field) {
            var inv = false;
            if ($scope.$storage[$scope.storageKey].currentSort && $scope.$storage[$scope.storageKey].currentSort.path == field.path)
                inv = !$scope.$storage[$scope.storageKey].currentSort.inv;

            $scope.$storage[$scope.storageKey].currentSort = {
                path: field.path,
                label: field.label,
                inv: inv
            }

            getListPage(1);
        }

        function clearFilters() {
            $scope.$storage[$scope.storageKey].filters = {
                currentFilters: [],
                selectedFilters: []
            };
            $scope.filter = {
                searchText: '',
                filterObj: {}
            }

            getListPage(1);
        }

        function getListPage(page, index) {
            if (!page) {
                page = $scope.items.currentPage;
            } else if ((typeof page).toLowerCase() == 'string' && page.trim() == '...')
                page = index ? $scope.items.totalPages : 1

            AdminAPI.GetListPage($scope.list.list.path, page, $scope.filter.filterObj, $scope.$storage[$scope.storageKey].currentSort, $scope.filter.searchText).then(function (res) {
                if (res.data.data.redirect) {
                    return moveToItem(res.data.data.redirect);
                }
                $scope.items = res.data.data;
                var params = $stateParams;
                params.page = page;
                $state.transitionTo($state.current, params, { notify: false });
            })
        }

        function isActivePage(p) {
            return p == $scope.items.currentPage;
        }

        function download(e) {

            if ($scope.items.total > 5000)
            {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(false)
                        .title('ייצוא לקובץ CSV')
                        .htmlContent('באפשרותך לייצא עד 5000 רשומות.<br>נא בצע חיפוש מדויק יותר בכדי לייצא את הנתונים.')
                        .ariaLabel('ייצוא לקובץ CSV')
                        .ok('OK')
                        .targetEvent(e)
                );
                return;
            }

            var cols = $scope.columns.map(function (col) { return col.path; })
            AdminAPI.DownloadList($scope.list.list.path, $scope.filter.filterObj, $scope.$storage[$scope.storageKey].currentSort, cols);
        }

        function removeFilter(path, index) {
            $scope.$storage[$scope.storageKey].filters.currentFilters.splice(index, 1);
            $scope.$storage[$scope.storageKey].filters.selectedFilters.splice(index, 1);
            delete $scope.filter.filterObj[path];
        }

        function runFilter() {
            getListPage(1);
        }

        function moveToItem(itemId, list) {
            var params = {
                list: list || $stateParams.list,
                itemId: itemId
            };

            $state.go('restricted.item', params);
        }

        function goToItem(item) {
            moveToItem(item._id,item['__t']);
        }

        function openInitial(e) {
            Popups.ShowInitial(e, $scope.list.list, null, function (newItem) {
                var params = {
                    list: newItem.listPath,
                    itemId: newItem.data.id
                };

                $state.go('restricted.item', params);
            });
        }

        function deleteItem(itemId, e) {
            Popups.DeleteItem($scope.list.list.path, itemId, e, function () {
                getListPage();
            });
        }

        /// Filters Config

        function onFilterChipRemoved(value) {
            for (var i = 0, length = $scope.$storage[$scope.storageKey].filters.currentFilters.length; i < length; i++) {
                if ($scope.$storage[$scope.storageKey].filters.currentFilters[i].path == value) {
                    delete $scope.filter.filterObj[$scope.$storage[$scope.storageKey].filters.currentFilters[i].path];
                    $scope.$storage[$scope.storageKey].filters.currentFilters.splice(i, 1);
                    break;
                }
            }
        }

        function onFilterChipAdd(value, $item) {
            var exists = $scope.$storage[$scope.storageKey].filters.currentFilters.filter(function (col) {
                return col.path == value;
            });
            if (!exists.length)
                $scope.$storage[$scope.storageKey].filters.currentFilters.push($scope.availableColumns.filter(function (col) {
                    return col.path == value;
                })[0]);
        }

        $scope.selectize_filters_config = {
            placeholder: "Select Filters",
            valueField: 'path',
            labelField: 'label',
            searchField: 'label',
            closeAfterSelect : true,
            create: false,
            render: {
                option: function (column, escape) {
                    return '<div class="option">' +
                        '<span class="title">' + escape(column.label) + '</span>' +
                        '</div>';
                },
                item: function (column, escape) {
                    return '<div class="item">' + escape(column.label) + '</div>';
                }
            },
            onItemAdd: onFilterChipAdd,
            onItemRemove: onFilterChipRemoved
        };

        /// Columns Config

        function onColumnChipRemoved(value) {
            for (var i = 0, length = $scope.columns.length; i < length; i++) {
                if ($scope.columns[i].path == value) {
                    $scope.columns.splice(i, 1);
                    break;
                }
            }
        }

        function onColumnChipAdd(value, $item) {
            var exists = $scope.columns.filter(function (col) {
                return col.path == value;
            });
            if (!exists.length)
                $scope.columns.push($scope.availableColumns.filter(function (col) {
                    return col.path == value;
                })[0]);
        }



        $scope.selectize_columns_config = {
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            placeholder: "Select Columns",
            valueField: 'path',
            labelField: 'label',
            searchField: 'label',
            create: false,
            render: {
                option: function (column, escape) {
                    return '<div class="option">' +
                        '<span class="title">' + escape(column.label) + '</span>' +
                        '</div>';
                },
                item: function (column, escape) {
                    return '<div class="item">' + escape(column.label) + '</div>';
                }
            },
            onItemAdd: onColumnChipAdd,
            onItemRemove: onColumnChipRemoved
        };

    }]);