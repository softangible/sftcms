angular.module('sftCms').controller('initialCtrl', [
    '$scope', '$mdDialog', 'AdminAPI', 'list', 'relation', '$state','$translate',
    function ($scope, $mdDialog, AdminAPI, list, relation, $state,$translate) {
        $scope.viewModes = {
            ChooseInherited: 1,
            Creation: 2
        };

        $scope.list = list;
        $scope.initialFields = [];

        $scope.model = {};

        $scope.close = $mdDialog.cancel;
        $scope.create = create
        $scope.setInherited = setInherited;

        activate();
        //////////////////////////////////////////////

        function activate() {
            if ($scope.list.options.abstract) {
                $scope.viewMode = $scope.viewModes.ChooseInherited;
                $scope.modalTitle = $translate.instant('initialModal.abstractTitle') + $scope.list.options.inheritedLabel;
                return;
            }

            initiateCreate();
        }

        function initiateCreate() {
            $scope.viewMode = $scope.viewModes.Creation;
            $scope.modalTitle = $translate.instant('initialModal.newTitle') + ($scope.list.options.singular || $scope.list.options.label);
            if (relation) {
                $scope.model[relation.refPath] = relation.val;
            }

            for (var key in $scope.list.fields) {
                if ((!$scope.list.fields[key].options.hidden && $scope.list.fields[key].options.initial) || key == "name" || (relation && key == relation.refPath)) {
                    if (relation && key == relation.refPath)
                        $scope.list.fields[key].options.noedit = true;

                    $scope.initialFields.push($scope.list.fields[key]);
                    if ($scope.list.fields[key].type == 'html')
                        $scope.model[$scope.list.fields[key].path] = '';
                }
            }
        }


        function create() {
            if ($scope.editForm.$invalid) return;

            AdminAPI.SaveOrUpdateItem('save', $scope.list.path, $scope.model, relation).then(function (res) {
                $mdDialog.hide({data: res.data.data, listPath : $scope.list.path});
            }, function (res) {
                $scope.errors = res.data;
            });
        }

        function setInherited(inheritedList) {
            AdminAPI.GetListMetaData(inheritedList.path).then(function(list){
                $scope.list = list.list;
                initiateCreate();
            })
        }

    }]);