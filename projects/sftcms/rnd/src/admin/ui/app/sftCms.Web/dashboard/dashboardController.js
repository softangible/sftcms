angular
    .module('sftCms')
    .controller('dashboardCtrl', ['$scope',
        function ($scope) {
            
        }
    ])
    // google maps
    .controller('userMapsCtrl', [
        '$rootScope',
        '$scope',
        '$timeout',
        'utils',
        function($rootScope,$scope, $timeout,utils) {

            // marker icon
            $scope.marker_icon = {
                url: utils.isHighDensity() ? 'img/md-images/ic_location_history_black_48dp.png' : 'img/md-images/ic_location_history_black_24dp.png',
                size: utils.isHighDensity() ? [48, 48] : [24, 24],
                scaledSize: [24, 24]
            };

            var map;
            $scope.$on('mapInitialized', function(evt, evtMap) {
                map = evtMap;
            });

            // show info window on marker click
            $scope.showInfo = function(evt, id) {
                $scope.user = $scope.gmap_users[id];
                $scope.showInfoWindow.apply(this, [evt, 'infoWindow']);
            };

            // show marker
            $scope.showMarker = function(evt,id) {
                $scope.user = $scope.gmap_users[id];
                $scope.showInfoWindow.apply(this, [evt, 'infoWindow', $scope.map.markers[id]]);
                $scope.map_center = [$scope.user.lat,$scope.user.lon];
                $scope.map_zoom = '14';
            };


            // users
            $scope.gmap_users = [
                {
                    name: "Jackson Heathcote",
                    company: "Larson, Langosh and Fritsch",
                    avatar: "img/avatars/avatar_01_tn.png",
                    lat: "37.406267",
                    lon: "-122.06742"
                },
                {
                    name: "Lelah Leffler",
                    company: "Rowe PLC",
                    avatar: "img/avatars/avatar_02_tn.png",
                    lat: "37.379267",
                    lon: "-122.02148"
                },
                {
                    name: "Lawson Kiehn",
                    company: "Legros Inc",
                    avatar: "img/avatars/avatar_03_tn.png",
                    lat: "37.410267",
                    lon: "-122.11048"
                },
                {
                    name: "Jacques VonRueden",
                    company: "Legros, Balistreri and Adams",
                    avatar: "img/avatars/avatar_04_tn.png",
                    lat: "37.397267",
                    lon: "-122.084417"
                },{
                    name: "Neoma Littel",
                    company: "Conn-Ferry",
                    avatar: "img/avatars/avatar_05_tn.png",
                    lat: "37.372267",
                    lon: "-122.090417"
                }
            ];

        }
    ])
;