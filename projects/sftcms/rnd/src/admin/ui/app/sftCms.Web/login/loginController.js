angular
    .module('sftCms')
    .controller('loginCtrl', ['$scope','$state','AdminAPI','$location',
        function ($scope,$state,AdminAPI,$location) {
            $scope.model = {};
            
            AdminAPI.isAuthenticated().then(function(res){
                if(res.data.data.isAuth)
                    $state.go('restricted.dashboard');
            })
            
            
            $scope.signin = function(){
                if ($scope.loginForm.$invalid) return;
                AdminAPI.SignIn($scope.model).then(function(res){
                    if ($location.$$search && $location.$$search.returnUrl)
                        window.location.replace($location.$$search.returnUrl);
                    else
                        $state.go('restricted.dashboard');
                },function(){
                    $scope.signinFail = true;
                });
            }

            $scope.hideFailMsg=function(){
                $scope.signinFail = false;
            }
        }
    ]);