angular.module('sftCms').controller('itemCtrl', [
    '$translate', '$scope', '$state', 'AdminAPI', 'list', 'item', 'Notifications', 'Popups', '$timeout', '$window', '$mdDialog', 'utils', '$rootScope',
    function ($translate, $scope, $state, AdminAPI, list, item, Notifications, Popups, $timeout, $window, $mdDialog, utils, $rootScope) {

        var selfRelatedPromises = {},
            visitedTabs = {},
            bindingsCounter = 0,
            originalModel,
            $itemFooter;

        $scope.list = list;
        $scope.bindings = {};
        $scope.bindedProperties = {};
        $scope.tabs = {};
        $scope.langs = AdminAPI.GetConfig('defaultLangs');

        $scope.bindingModes = {
            OVERRIDE: 'override',
            BINDS: 'binds'
        }

        $scope.multiselect = {
            texts: { buttonDefaultText: 'Overrided', dynamicButtonTextSuffix: ':Binded' },
            settings: {
                displayProp: 'label',
                showUncheckAll: false,
                showCheckAll: false
            }
        }

        $scope.openInitial = openInitial;
        $scope.setCurrentTab = setCurrentTab;
        $scope.update = update;
        $scope.deleteItem = deleteItem;
        $scope.revertToPublished = revertToPublished;
        $scope.deleteRelatedItem = deleteRelatedItem;
        $scope.goToItem = goToItem;
        $scope.resetChanges = resetChanges;
        $scope.hideField = hideField;
        $scope.openBindings = openBindings;
        $scope.removeRelationship = removeRelationship;
        $scope.getFieldBindOptions = getFieldBindOptions;

        $scope.$on('$stateChangeStart', stateChangeStart);
        $scope.$on('bind:add', addNewBind);
        $scope.$on('$destroy', cleanUp);
        angular.element($window).on('resize', onResize);
        angular.element($window).on('scroll', onScroll);

        activate();

        /////////////////////////////////////////////////////////////////////////////////////

        function activate() {
            var generalKey = $translate.instant('itemView.general') || 'General';

            if ($scope.langs && $scope.list.list.options.langs !== false) {
                $scope.currentLang = $scope.list.list.options.defaultLang || $scope.langs[0];
            }

            initModel(item);

            var generalNum = '';
            if (!list.list.schemaFields) {
                addTab(generalKey, list.list.fields);
            }
            else {
                for (var i = 0, length = list.list.schemaFields.length; i < length; i++) {
                    if (i == 0 && angular.isObject(list.list.schemaFields[i])) {
                        var added = addTab(generalKey, list.list.schemaFields[i]);
                        if (added) generalNum = 2;
                    }
                    else if (angular.isString(list.list.schemaFields[i])) {
                        addTab(list.list.schemaFields[i], list.list.schemaFields[i + 1]);
                        i++;
                    }
                    else if (angular.isObject(list.list.schemaFields[i])) {
                        addTab(generalKey + generalNum, list.list.schemaFields[i]);
                        generalNum = generalNum ? generalNum + 1 : 2;
                    }
                }
            }

            setCurrentTab(Object.keys($scope.tabs)[0]);
        }

        function initModel(item) {
            if (list.list.options.track) {
                item.track = {};
                if (item.fields.createdBy && item.fields.createdBy._id) {
                    item.track.createdBy = item.fields.createdBy;
                    item.track.createdAt = item.fields.createdAt;
                    delete item.fields.createdBy;
                    delete item.fields.createdAt;
                }
                if (item.fields.updatedBy && item.fields.updatedBy._id) {
                    item.track.updatedBy = item.fields.updatedBy;
                    item.track.updatedAt = item.fields.updatedAt;
                    delete item.fields.updatedBy;
                    delete item.fields.updatedAt;
                }

                var format_string = 'Do MMM YY, h:mm:ss a';

                $scope.showTrack = true;
                $scope.createdDate = moment(item.track.createdAt).format(format_string);
                $scope.updatedDate = moment(item.track.updatedAt).format(format_string);
            }

            $scope.item = item;
            $scope.model = item.fields
            $scope.model.id = item.id;

            if ($scope.model._bindingMap) {
                $scope.model._bindingMap = JSON.parse($scope.model._bindingMap);
                for (var key in $scope.model._bindingMap) {
                    if ($scope.model._bindingMap[key].mode != 'binds') continue;

                    for (var i = 0, length = $scope.model._bindingMap[key].binds.length; i < length; i++) {
                        var bind = $scope.model._bindingMap[key].binds[i];
                        if (bind.modelId) continue;
                        if ($scope.model[bind.fieldPath] && $scope.model[bind.fieldPath][bind.modelIndex])
                            $scope.model._bindingMap[key].binds[i].modelId = $scope.model[bind.fieldPath][bind.modelIndex]['_id'];
                    }
                }
            }

            if ($scope.list.list.options.bind) {
                $scope.list.list.options.bind.forEach(function (bind) {
                    $scope.bindedProperties[bind.bindingPath] = $scope.model[bind.bindingPath];
                });
            }

            $timeout(onResize);
            $timeout(function () {
                originalModel = angular.copy($scope.model);
                $scope.originalModelName = originalModel[$scope.list.list.nameFieldPath];
            }, 300)

            applyBindignsOnFields();
        }

        function addNewBind(e, data) {
            e.stopPropagation();

            var bind;
            for (var i = 0, length = $scope.list.list.options.bind.length; i < length; i++) {
                if ($scope.list.list.options.bind[i].bindingPath == data.fieldPath) {
                    bind = $scope.list.list.options.bind[i];
                    break;
                }
            }
            if (!bind) return;

            var model = data.data;
            for (var key in $scope.bindings) {
                var fieldType = $scope.bindings[key].type;
                var fieldLang = $scope.bindings[key].lang;

                for (var innerFieldPath in bind.list.fields) {
                    var innerField = bind.list.fields[innerFieldPath];
                    if (fieldType == innerField.type) {
                        if (!fieldLang || !innerField.options.lang || (fieldLang == innerField.options.lang)) {
                            addAvailableBind(key, data.data, bind, innerField);
                        }
                    }
                }

                $scope.$broadcast('optionsChanged', { path: key, options: $scope.bindings[key].availableBindings });
            }
        }

        function addAvailableBind(path, model, bind, innerField, modelIndex) {
            var _modelIndex = modelIndex;
            if (!_modelIndex && _modelIndex !== 0) {
                if ($scope.model[bind.bindingPath] && $scope.model[bind.bindingPath].length)
                    _modelIndex = $scope.model[bind.bindingPath].length - 1
                else
                    _modelIndex = 0
            }

            var bindData = {
                id: bindingsCounter,
                modelIndex: _modelIndex,
                fieldPath: bind.bindingPath,
                label: model.value + ' - ' + innerField.label,
                modelName: model.value,
                innerPath: innerField.path,
                bindList: bind.list,
                modelId: model._id,
                type: innerField.type
            };
            bindingsCounter++;
            try {
                eval('bindData.data = model["' + bindData.innerPath.split('.').join('"]["') + '"]');
            } catch (err) {
                bindData.data = undefined;
            }
            $scope.bindings[path].availableBindings.push(bindData);
        }

        function applyBindignsOnFields() {



            var bindings = $scope.item.bindings
            if (!bindings || !bindings.length) return;

            bindings.forEach(function (bind) {
                if (!$scope.list.list.fields[bind.bindedTo]) return;

                if (!$scope.list.list.fields[bind.bindedTo].binds)
                    $scope.list.list.fields[bind.bindedTo].binds = []

                $scope.list.list.fields[bind.bindedTo].binds.push(bind.binded);
            })

        }

        function openBindings(bindings, e) {
            $mdDialog.show({
                controller: 'bindingsModalCtrl',
                templateUrl: 'app/sftCms.Web/modals/bindings/bindings.html',
                parent: angular.element(document.body),
                targetEvent: e,
                clickOutsideToClose: true,
                fullscreen: false,
                flex: "50",
                locals: {
                    bindings: bindings
                }
            })
        }

        function hideField(fieldPath) {
            var field = $scope.list.list.fields[fieldPath];
            if (field.options.hidden) return true;

            if (!$scope.langs || ($scope.langs && $scope.list.list.options.langs === false) || field.options.langs === false)
                return false;

            return field.options.lang != $scope.currentLang;
        }

        function stateChangeStart(event, toState, toParams, fromState, fromParams) {
            if ($rootScope.forceStateChange) {
                $rootScope.forceStateChange = false;
                return;
            }

            if (_.isEqual(angular.copy($scope.model), originalModel)) return;

            event.preventDefault();
            utils.preventLeave();

            var confirm = $mdDialog.confirm()
                .title('Unsaved Changes')
                .textContent("You haven't saved you latest changes, would you like to save them?")
                .ariaLabel('SaveChanges')
                .ok('Leave Anyway')
                .cancel('Stay');

            $mdDialog.show(confirm).then(function () {
                $rootScope.forceStateChange = true;
                $state.go(toState.name, toParams);
            });
        }

        function onResize() {
            if (!$itemFooter)
                $itemFooter = $('.item-footer');
            $itemFooter.width($('[ng-form="editForm"]').width());
            $itemFooter.css('margin-left', $('[ng-form="editForm"]').offset().left);
        }

        function onScroll() {
            if (!$itemFooter)
                $itemFooter = $('.item-footer');

            var $$window = $(window),
                pageHeight = $('#page_content').height(),
                windowScroll = $$window.scrollTop(),
                windowHeihgt = $$window.height();

            if (pageHeight - windowScroll < windowHeihgt + 80)
                $itemFooter.parent().removeClass('fixed');
            else {
                $itemFooter.parent().addClass('fixed');
                $itemFooter.css('margin-left', $('[ng-form="editForm"]').offset().left);
                $itemFooter.width($('[ng-form="editForm"]').width());
            }

        }

        function cleanUp() {
            angular.element($window).off('resize', onResize);
            angular.element($window).off('scroll', onScroll);
        };

        function setCurrentTab(key) {
            var current = $scope.currentTab;
            $scope.currentTab = key;

            if (current != key) {
                var params = {
                    key: key,
                    visited: visitedTabs[key]
                };

                $scope.$broadcast('item-tab-changed', params);
                visitedTabs[key] = true;
                setTimeout(onScroll, 450);
            }
        }

        function resetChanges() {
            if (_.isEqual(angular.copy($scope.model), originalModel)) return;

            $rootScope.forceStateChange = true;
            $state.reload();
        }

        function update(e, isPublish, isForce) {
            var action = isPublish ? 'update-and-publish' : 'update'
            AdminAPI.SaveOrUpdateItem(action, $scope.list.list.path, $scope.model,undefined,isForce).then(function (res) {
                if (res.data.data.revId) {
                    $scope.item.revId = res.data.data.revId;
                    if (isPublish)
                        $scope.item.publishedRevId = res.data.data.revId;
                }

                Notifications.Show("נשמר בהצלחה", "success");
                originalModel = angular.copy($scope.model);
                $scope.originalModelName = originalModel[$scope.list.list.nameFieldPath];
                selfRelatedPromises = {};
                $scope.$broadcast('itemUpdated');
            }, function (res) {
                if (res.data.popup)
                    Popups.SavePopup(e, res.data.popup).then(function(res){
                        update(e,isPublish,true);
                    })
                else            
                    Popups.DisplayErrors(e, res.data);
            });
        }

        function revertToPublished(e) {
            AdminAPI.RevertToPublished($scope.list.list.path, $scope.model.id).then(function (res) {
                // Notifications.Show("שוחזר בהצלחה", "success");
                // originalModel = angular.copy($scope.model);
                // $scope.originalModelName = originalModel[$scope.list.list.nameFieldPath];
                // selfRelatedPromises = {};
                // initModel(res.data.data);
                // $scope.$broadcast('itemUpdated');
                $rootScope.forceStateChange = true;
                $state.reload();
            }, function (res) {
                Popups.DisplayErrors(e, res.data);
            });
        }

        function deleteItem(event) {
            Popups.DeleteItem($scope.list.list.path, $scope.model.id, event, function () {
                var params = {
                    list: $scope.list.list.path
                }
                $state.go('restricted.list', params);
            });
        }

        function addTab(name, fields) {
            var add = false;

            for (var key in fields) {
                if (!fields[key].hidden && !$scope.list.list.fields[key].options.hidden) {
                    add = true;
                    break;
                }
            }

            if (add)
                $scope.tabs[name] = fields;

            return add;
        }

        function goToItem(relationship, item) {
            var params = {
                list: item['__t'] || relationship.list.path,
                itemId: item.id
            };

            $state.go('restricted.item', params);
        }

        function deleteRelatedItem(relationship, itemId, index, e) {
            Popups.DeleteItem(relationship.list.path, itemId, e, function () {
                relationship.items.results.splice(index, 1);
            });
        }

        function openInitial(relationship, e) {

            if (!selfRelatedPromises[relationship.refPath])
                selfRelatedPromises[relationship.refPath] = AdminAPI.getRelatedItem(relationship.refPath, $scope.list.list.path, originalModel.id);

            Popups.ShowInitial(e, relationship.list, selfRelatedPromises[relationship.refPath], function (newItem) {
                relationship.items.results.push(newItem.data);
            })
        }

        function addMultiselectEvent(path) {
            var path = path;
            return {
                path: path,
                onOverride: function () {
                    if ($scope.model._bindingMap[path].binds && $scope.model._bindingMap[path].binds.length) {
                        $scope.model._bindingMap[path].binds.forEach(function (bind) {
                            if ($scope.list.list.fields[bind.fieldPath].bindings)
                                delete $scope.list.list.fields[bind.fieldPath].bindings[path];
                        })
                    }


                    $scope.bindings[path].selectedModels = [];
                    $scope.model._bindingMap[path].binds = [];
                    $scope.model._bindingMap[path].mode = $scope.bindingModes.OVERRIDE;
                },
                onItemSelect: function (item) {
                    $scope.model._bindingMap[path].mode = $scope.bindingModes.BINDS;

                    var bind;
                    for (var i = 0, length = $scope.bindings[path].availableBindings.length; i < length; i++) {
                        if ($scope.bindings[path].availableBindings[i].id == item.id) {
                            bind = $scope.bindings[path].availableBindings[i];
                            break;
                        }
                    }
                    if (!bind) return;

                    $scope.bindings[path].selectedModels.push(bind);
                    $scope.model._bindingMap[path].binds.push({
                        modelIndex: bind.modelIndex,
                        modelId: bind.modelId,
                        innerPath: bind.innerPath,
                        fieldPath: bind.fieldPath
                    })

                    addBindToRelation(path, bind);
                },
                onItemDeselect: function (item) {
                    for (var i = 0, length = $scope.bindings[path].selectedModels.length; i < length; i++) {
                        if ($scope.bindings[path].selectedModels[i].id == item.id) {

                            var indexOfBind;
                            for (var j = 0, length2 = $scope.model._bindingMap[path].binds.length; j < length2; j++) {
                                var bindingMapBind = $scope.model._bindingMap[path].binds[j];
                                var bindingSelectedModel = $scope.bindings[path].selectedModels[i];
                                if (bindingMapBind.modelIndex == bindingSelectedModel.modelIndex && bindingMapBind.innerPath == bindingSelectedModel.innerPath) {
                                    indexOfBind = j;
                                    break;
                                }
                            }

                            removeBindfromRelation(path, $scope.model._bindingMap[path].binds[indexOfBind]);
                            $scope.model._bindingMap[path].binds.splice(indexOfBind, 1)
                            $scope.bindings[path].selectedModels.splice(i, 1);
                            break;
                        }
                    }
                    if (!$scope.bindings[path].selectedModels.length)
                        $scope.model._bindingMap[path].mode = $scope.bindingModes.OVERRIDE;
                }
            }
        }

        function removeBindfromRelation(path, bind) {
            var relationBinds = $scope.list.list.fields[bind.fieldPath].bindings[path];
            for (var i = 0, length = relationBinds.length; i < length; i++) {
                if (relationBinds[i].fieldPath == bind.fieldPath &&
                    relationBinds[i].modelId == bind.modelId &&
                    relationBinds[i].innerPath == bind.innerPath &&
                    relationBinds[i].modelIndex == bind.modelIndex) {
                    relationBinds.splice(i, 1);
                    return;
                }
            }
        }

        function addBindToRelation(path, bind) {
            if (!$scope.list.list.fields[bind.fieldPath].bindings)
                $scope.list.list.fields[bind.fieldPath].bindings = {}
            if (!$scope.list.list.fields[bind.fieldPath].bindings[path])
                $scope.list.list.fields[bind.fieldPath].bindings[path] = [];

            $scope.list.list.fields[bind.fieldPath].bindings[path].push({
                innerPath: bind.innerPath,
                fieldPath: bind.fieldPath,
                modelId: bind.modelId,
                modelIndex: bind.modelIndex
            })
        }

        function getFieldBindOptions(fieldData, path) {
            if (!fieldData.bindable) return;
            if (!$scope.model._bindingMap[path]) {
                $scope.model._bindingMap[path] = {
                    mode: $scope.bindingModes.OVERRIDE,
                    binds: []
                }
            }

            var fieldLang = fieldData.lang;
            var fieldType = $scope.list.list.fields[path].type;
            if (!$scope.bindings[path]) {
                $scope.bindings[path] = {
                    selected: [],
                    selectedModels: [],
                    availableBindings: [],
                    events: addMultiselectEvent(path),
                    lang: fieldLang,
                    type: fieldType
                }
            } else {
                return $scope.bindings[path].availableBindings;
            }

            var bindOptions = [];
            // Binds Metadata (path and list)
            var binds = $scope.list.list.options.bind;


            binds.forEach(function (bind) {
                for (var innerFieldPath in bind.list.fields) {
                    var innerField = bind.list.fields[innerFieldPath];
                    if (fieldType == innerField.type) {
                        if (!fieldLang || !innerField.options.lang || (fieldLang == innerField.options.lang)) {
                            $scope.bindedProperties[bind.bindingPath].forEach(function (model, index) {
                                addAvailableBind(path, model, bind, innerField, index);
                            })
                        }
                    }
                }
            });

            var bindingMap = $scope.model._bindingMap[path];
            if (bindingMap.mode == $scope.bindingModes.BINDS) {
                var relaventBinds = $scope.bindings[path].availableBindings.filter(function (availableBind) {
                    for (var i = 0, length = bindingMap.binds.length; i < length; i++) {
                        if (bindingMap.binds[i].modelId == availableBind.modelId && bindingMap.binds[i].fieldPath == availableBind.fieldPath && bindingMap.binds[i].modelIndex == availableBind.modelIndex && bindingMap.binds[i].innerPath == availableBind.innerPath) {
                            addBindToRelation(path, availableBind);
                            return true;
                        }
                    }
                    return false;
                })

                $scope.bindings[path].selectedModels = relaventBinds;
                $scope.bindings[path].selected = relaventBinds.map(function (relBind) { return { id: relBind.id } });
            }

            return $scope.bindings[path].availableBindings;
        }

        function areBindsEqual(bind, bind2) {
            return bind.modelIndex == bind2.modelIndex && bind.modelId == bind2.modelId && bind.fieldPath == bind2.fieldPath;
        }

        function clearAndReduceIndex(arr, bind2, onMatch) {
            for (var i = arr.length - 1; i >= 0; i--) {
                if (areBindsEqual(arr[i], bind2)) {
                    if (onMatch)
                        onMatch(i, arr[i]);

                    arr.splice(i, 1);
                }
                else if (arr[i].modelIndex > bind2.modelIndex)
                    arr[i].modelIndex--;
            }
        }

        function removeRelationship(fieldPath, modelId, modelIndex) {
            var bind2 = {
                modelIndex: modelIndex,
                modelId: modelId,
                fieldPath: fieldPath
            };

            for (var key in $scope.bindings) {
                var currentBinding = $scope.bindings[key];
                // remove select options from dropdown
                clearAndReduceIndex(currentBinding.availableBindings, bind2);

                // remove all relavent select options
                clearAndReduceIndex(currentBinding.selectedModels, bind2, function (index, bind) {
                    for (var i = 0, length = currentBinding.selected.length; i < length; i++) {
                        if (currentBinding.selected[i].id == bind.id) {
                            currentBinding.selected.splice(i, 1);
                            break;
                        }
                    }
                })

                // remove selected from binding map;
                if ($scope.model._bindingMap[key].binds && $scope.model._bindingMap[key].binds.length) {
                    clearAndReduceIndex($scope.model._bindingMap[key].binds, bind2);
                }

                if (!$scope.model._bindingMap[key].binds || !$scope.model._bindingMap[key].binds.length) {
                    $scope.model._bindingMap[key].mode = $scope.bindingModes.OVERRIDE;
                }

                $scope.$broadcast('optionsChanged', { path: key, options: currentBinding.availableBindings });
            }
        }

    }]);