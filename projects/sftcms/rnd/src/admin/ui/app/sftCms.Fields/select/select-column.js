/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('selectColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '=',
                    field : '&'
                },
                template:'<span ng-bind="value"></span>',
                replace : true,
                link: function (scope) {
                    var opt = scope.field().options.options.filter(function(opt){
                        return (opt.value == scope.ngModel || opt==scope.ngModel);
                    })[0];
                    
                    scope.value = opt ? (opt.label || opt) : '';
                }
            };
        }
    ]);