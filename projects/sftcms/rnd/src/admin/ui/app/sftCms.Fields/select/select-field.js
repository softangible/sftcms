/*
*  Altair Admin AngularJS
*  directives
*/
; 'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('selectField', [
        function () {
            return {
                restrict: 'A',
                scope: {
                    ngModel: '=',
                    field: '&',
                    form: '='
                },
                templateUrl:"app/sftCms.Fields/select/select-field.html",
                link : function(scope){
                    scope.field=scope.field();
                    if (!scope.ngModel)
                        scope.ngModel = null
                }
            };
        }
    ]);