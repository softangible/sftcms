/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('htmlFilter', ['FiltersService',
        function (FiltersService) {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/html/filter/html-filter.html",
                scope : {
                    ngModel : '='
                },
                link: function (scope, elem, attrs) {
                    scope.types = [
                        {key:'contains', id:1},
                        {key:'exact match', id:2}
                    ];
                    
                    scope.currentType = scope.types[0];
                    
                    scope.setType = function(type){
                        scope.currentType = type;
                    }
                    
                    scope.$watchGroup(['value','currentType'],function(newValues, oldValues, scope){
                        scope.ngModel = FiltersService.GetStringFilter(scope.currentType, scope.value);
                    })
                    
                }
            };
        }
    ]);