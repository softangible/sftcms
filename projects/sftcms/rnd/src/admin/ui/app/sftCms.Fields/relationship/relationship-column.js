/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    .directive('relationshipColumn', ['$state',
        function ($state) {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '='
                },
                template:'<div class="relationshipColumn"><a style="line-height:45px;" ng-repeat="relation in values" ng-click="goToItem($event,relation)" ng-bind="::relation.value"></a></div>',
                replace : true,
                link: function (scope, el, attrs) {
                    if (angular.isArray(scope.ngModel))
                        scope.values = scope.ngModel;
                    else if (scope.ngModel)
                        scope.values = [scope.ngModel];

                    if (attrs.hasOwnProperty('isInitial'))
                        scope.ngModel = angular.isArray(scope.ngModel) ? scope.ngModel.map(function(item){return item._id}) : scope.ngModel._id;

                    scope.goToItem = goToItem;

                    function goToItem(e, relation){
                        e.stopPropagation();
                        e.preventDefault();
                        $state.go('restricted.item', {list:(relation.__t || relation.listPath), itemId:relation._id});
                    }
                }
            };
        }
    ]);