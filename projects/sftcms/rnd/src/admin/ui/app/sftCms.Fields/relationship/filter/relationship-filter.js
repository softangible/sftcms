/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('relationshipFilter', ['AdminAPI',
        function (AdminAPI) {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/relationship/filter/relationship-filter.html",
                scope : {
                    ngModel : '=',
                    col : '=',
                    list: '='
                },
                link: function (scope, elem, attrs) {
                    
                    scope.selectedItem = null;
                    
                    scope.getMatches = function(searchText){
                        return AdminAPI.FilterRelationShipAutocomplete(scope.col.options.ref, scope.list.key, scope.col.path, searchText,scope.col.options.showAll).then(function(res){
                          return res.data.items;  
                        });
                    };
                    
                    scope.updateFilter = function(){
                       if (scope.col.many)
                       {
                            if (scope.selectedItem)
                                scope.ngModel = { $in: [scope.selectedItem.id] };
                            else
                                scope.ngModel = { $size: 0 };
                       } else {
                           if (scope.selectedItem)
                                scope.ngModel = scope.selectedItem.id;
                            else
                                scope.ngModel = null;
                       }
                    }
                }
            };
        }
    ]);