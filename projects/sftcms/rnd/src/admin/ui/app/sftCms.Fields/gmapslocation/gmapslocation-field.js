/*
*  Altair Admin AngularJS
*  directives
*/
; 'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('gmapslocationField', ['$timeout', 'uiGmapIsReady',
        function ($timeout, uiGmapIsReady) {
            var GmapsTimeout = 200;
            function getGmapsCords(cords) {
                return {
                    latitude: cords && cords.coordinates ? cords.coordinates[1] || 32.2867521 : 32.2867521,
                    longitude: cords && cords.coordinates ? cords.coordinates[0] || 34.8494215 : 34.8494215
                }
            }

            return {
                restrict: 'A',
                templateUrl: "app/sftCms.Fields/gmapslocation/gmapslocation-field.html",
                scope: {
                    ngModel: '=',
                    field: '&',
                    form: '=',
                    tabKey: '='
                },
                link: {
                    pre: function (scope) {

                        scope.resetField = resetField;

                        function initData(clear) {
                            if (!scope.ngModel)
                                clear = true;
                            var cords = getGmapsCords(scope.ngModel ? scope.ngModel.cords : null);

                            var marker = {
                                id: 0,
                                options: { draggable: false }
                            };

                            if (!scope.ngModel || !Object.keys(scope.ngModel).length) {
                                scope.ngModel = {
                                    address: {},
                                    cords: {
                                        coordinates: [34.8494215, 32.2867521],
                                        type: "Point"
                                    }
                                };
                                scope.fullAddress = '';
                                scope.neighborhood = '';
                            } else {
                                if (scope.ngModel.address) {
                                    scope.fullAddress = scope.ngModel.address.formattedAddress;
                                    scope.neighborhood = scope.ngModel.address.neighborhoodName;
                                } else {
                                    scope.fullAddress = '';
                                    scope.neighborhood = '';
                                }
                            }

                            if (scope.ngModel && scope.ngModel.cords && scope.ngModel.cords.coordinates && !clear)
                                marker.cords = angular.copy(cords);


                            scope.map = {
                                center: cords,
                                events: {
                                    click: mapClick
                                },
                                control: {},
                                zoom: 17,
                                marker: marker
                            };

                            scope.cords = (!clear) ? angular.copy(cords) : {};
                        }

                        initData();

                        uiGmapIsReady.promise().then(function (google) {
                            scope.google = window.google;
                            scope.geocoder = new scope.google.maps.Geocoder();
                        }).catch(function (err) { console.log(err) });

                        scope.getGoogleMaps= function() {
                            if (scope.google && scope.google.maps)
                                return scope.google.maps;
                            else if (window.google && window.google.maps) {
                                scope.google = window.google;
                                scope.geocoder = new scope.google.maps.Geocoder();
                                return scope.google.maps;
                            }
                        }

                        scope.geocode = function (type, setCenter) {
                            if (!scope.getGoogleMaps()) return;
                            scope.geocoder.geocode(type, function (results, status) {
                                if (status == scope.google.maps.GeocoderStatus.OK) {
                                    scope.updatePlace(results[0], false, setCenter);
                                }
                            });
                        }

                        function mapClick(map, type, e) {
                            scope.geocode({ latLng: e[0].latLng }, false);
                        }

                        function resetField() {
                            scope.ngModel = {};
                            initData(true);
                        }

                    },
                    post: function (scope, elem, attrs) {
                        

                        scope.$watch('fullAddress', function () {
                            if (angular.isObject(scope.fullAddress))
                                scope.updatePlace(scope.fullAddress, false, true);
                        })

                        scope.getNewLocation = function () {
                            var maps = scope.getGoogleMaps();
                            if (!maps) return;

                            var latlng = new maps.LatLng(scope.cords.latitude, scope.cords.longitude);
                            scope.geocode({ latLng: latlng }, true);
                        }

                        scope.$watch('neighborhood', function () {
                            if (angular.isObject(scope.neighborhood))
                                scope.updatePlace(scope.neighborhood, true);
                        })

                        scope.updatePlace = function (place, isNeighborhood, setCenter) {
                            if (isNeighborhood) {
                                var hasNeighborhood = false;
                                for (var i = 0, length = place.address_components.length; i < length; i++) {
                                    if (place.address_components[i].types.indexOf('neighborhood') != -1) {
                                        scope.ngModel.address.neighborhoodName = place.address_components[i].long_name;
                                        hasNeighborhood = true;
                                        break;
                                    }
                                }
                                if (!hasNeighborhood)
                                    scope.neighborhood = '';
                                return;
                            }

                            scope.ngModel.cords.coordinates = [
                                place.geometry.location.lng(),
                                place.geometry.location.lat()
                            ]

                            delete scope.ngModel.address.streetNum;
                            delete scope.ngModel.address.streetName;
                            delete scope.ngModel.address.cityName;

                            scope.ngModel.address.formattedAddress = scope.fullAddress = place.formatted_address

                            place.address_components.forEach(function (address_component) {

                                if (address_component.types.indexOf('street_number') != -1) {
                                    scope.ngModel.address.streetNum = address_component.long_name;
                                }
                                else if (address_component.types.indexOf('route') != -1) {
                                    scope.ngModel.address.streetName = address_component.long_name;
                                }
                                else if (address_component.types.indexOf('neighborhood') != -1) {
                                    scope.ngModel.address.neighborhoodName = address_component.long_name;
                                }
                                else if (address_component.types.indexOf('locality') != -1) {
                                    scope.ngModel.address.cityName = address_component.long_name;
                                }
                            });

                            var cords = getGmapsCords(scope.ngModel.cords);
                            if (setCenter)
                                scope.map.center = cords;
                            scope.map.marker.cords = angular.copy(cords);
                            scope.cords = angular.copy(cords);

                            if (!scope.$$phase){
                                $timeout(function(){
                                    scope.$apply();
                                })
                            }
                        }

                        function refreshMap() {
                            $timeout(function () {
                                uiGmapIsReady.promise().then(function (map_instances) {
                                    scope.map.control.refresh(scope.map.center);
                                }).catch(function (err) { scope.map.control.refresh(scope.map.center) });
                            }, GmapsTimeout);
                        }

                        refreshMap();

                        scope.$on('item-tab-changed', function () {
                            if (arguments[1].visited || (scope.tabKey.toLowerCase() != arguments[1].key.toLowerCase())) return;
                            refreshMap();
                        })

                    }
                }
            }
        }
    ]);