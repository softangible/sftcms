/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('nameField', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/name/name-field.html",
                scope:{
                  ngModel : '=',
                  field : '&'  
                },
                link: function (scope, elem, attrs) {
                    if (!scope.ngModel)
                        scope.ngModel = {};
                }
            };
        }
    ]);