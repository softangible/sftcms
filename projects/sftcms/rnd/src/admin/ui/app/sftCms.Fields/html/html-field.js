/*
*  Altair Admin AngularJS
*  directives
*/
; 'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('htmlField', ['AdminAPI','$sce','$timeout',
        function (AdminAPI,$sce,$timeout) {
            return {
                restrict: 'A',
                priority: 10,
                templateUrl: "app/sftCms.Fields/html/html-field.html",
                scope: {
                    ngModel: '=',
                    field: '&'
                },
                link: function (scope) {
                    scope.blurred = false;
                    scope.html = {
                        val: scope.ngModel || ''
                    };

                    scope.displayVal = $sce.trustAsHtml(
                        scope.html.val.replace(/(<([^>]+)>)|(&nbsp;)|(<(br)>)|("\n")|(\r?\n|\r)|()/ig , '').trim() ? 
                        scope.html.val : 
                        '<em class="no-content">Click here to edit</em>');
                    scope.generateWysiwyg = false;

                    function onBlur(){
                        scope.blurred = true;
                        scope.displayVal = $sce.trustAsHtml(
                            scope.html.val.replace(/(<([^>]+)>)|(&nbsp;)|(<(br)>)|("\n")|(\r?\n|\r)|()/ig , '').trim() ? 
                            scope.html.val : 
                            '<em class="no-content">Click here to edit</em>');
                    }

                    scope.edit = function(){
                        if(scope.generateWysiwyg){
                            scope.blurred = false;
                            scope.generateWysiwyg.focus();
                            return;
                        }
                        
                        scope.generateWysiwyg = true;
                        scope.$watch('html.val', function () {
                            scope.ngModel = scope.html.val;
                        })

                        scope.tinymce_options = AdminAPI.GetConfig('wysiwyg');
                        scope.tinymce_options.directionality = 'rtl';
                        scope.tinymce_options.skin_url = 'js/tinymce/';
                        scope.tinymce_options.language_url = 'js/tinymce/he_IL.js';
                        scope.tinymce_options.language = 'he_IL';
                        scope.tinymce_options.height = 270;
                        scope.tinymce_options.file_browser_callback = elFinderBrowser;
                        scope.tinymce_options.setup =  function(editor) {
                            scope.generateWysiwyg = editor;
                            $timeout(function(){ 
                                try{
                                    editor.focus(); 
                                }catch(err){}
                            });
                            editor.on("blur", onBlur);
                        }

                        function elFinderBrowser(field_name, url, type, win) {
                            tinymce.activeEditor.windowManager.open({
                                file: 'elfinder/elfinderTinyMCE.html',// use an absolute path!
                                title: 'בחר תמונה',
                                width: 1134,
                                height: 732,
                                resizable: 'yes'
                            }, {
                                setUrl: function (url) {
                                    win.document.getElementById(field_name).value = url;
                                }
                            });
                            return false;
                        }
                    }
                }
            };
        }
    ]);