/*
*  Altair Admin AngularJS
*  directives
*/
; 'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('colorField', [
        function () {
            return {
                restrict: 'A',
                templateUrl: "app/sftCms.Fields/color/color-field.html",
                scope: {
                    ngModel: '=',
                    field: '&',
                    form: '='
                },
                link: function ($scope, e, attrs) {
                    var field = $scope.field();

                    if (!$scope.ngModel)
                        $scope.ngModel = '';

                    $scope.options = {
                        label: "Choose a color",
                        icon: "brush",
                        default: "#f00",
                        genericPalette: false,
                        history: false
                    };
                }
            };
        }
    ]);