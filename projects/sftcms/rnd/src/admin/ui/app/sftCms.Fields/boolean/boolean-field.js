/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('booleanField', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '='
                },
                template:'<md-checkbox aria-label="checkbox" md-no-ink ng-model="ngModel" class="md-primary"></md-checkbox>',
                link: function (scope) {
                    if (!scope.ngModel && scope.ngModel!==false)
                        scope.ngModel = false;
                }
            };
        }
    ]);