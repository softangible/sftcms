/*
*  Altair Admin AngularJS
*  directives
*/
; 'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('colorColumn', [
        function () {
            return {
                restrict: 'A',
                scope: {
                    ngModel: '=',
                    field: '&'
                },
                template: '<div><div style="width:20px; height:20px;" ng-style="::colorStyle"></div><span ng-style="inputStyle" ng-bind="::value"></span></div>',
                replace: true,
                link: function (scope) {
                    scope.value = scope.ngModel;
                    scope.inputStyle = {
                        'word-break': 'break-word',
                        'direction': 'ltr',
                        'display': 'inline-block'
                    };
                    var field = scope.field();
                    if (field.options && field.options.direction && field.options.direction.toLowerCase() == 'ltr') {
                        scope.inputStyle.direction = 'ltr';
                        scope.inputStyle.display = 'block';
                    }

                    scope.colorStyle = {
                        'background-color': scope.value,
                        'width': '30px',
                        'height': '30px',
                        'display': 'inline-block',
                        'margin-left': '20px',
                        'border-radius': '50%'
                    }
                }
            };
        }
    ]);