/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('htmlColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '&',
                    field : '&',
                    doc : '&'
                },
                template:'<span ng-bind-html="::value"></span>',
                replace : true,
                link: function (scope) {
                    var val = scope.ngModel();
                    if (val===undefined || val===null){
                        var doc = scope.doc();
                        var field = scope.field();

                        if(field.path.indexOf('.') != -1){
                            try{
                                eval('val = doc.' + field.path);
                            }catch(err){}
                        }
                    }
                
                    scope.value = (val ? String(val).replace(/<[^>]+>/gm, '') : '').substring(0,100);
                }
            };
        }
    ]);