/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('emailColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '='
                },
                template:'<a href="mailto:{{value}}" ng-bind="value"></a>',
                replace : true,
                link: function (scope) {
                    scope.value = scope.ngModel;
                }
            };
        }
    ]);