/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('datetimeColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '=',
                    field : '&'
                },
                template:'<span>{{value}}</span>',
                replace : true,
                link: function (scope) {
                    var field = scope.field();
                    var timeString =  (field.options.format || (field.formatString || 'DD/MM/YYYY HH:mm'));
                    scope.value = moment(scope.ngModel).format(timeString);
                }
            };
        }
    ]);