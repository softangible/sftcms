/*
*  Altair Admin AngularJS
*  directives
*/
; 'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('relationshipField', ['AdminAPI','$timeout','$q',
        function (AdminAPI,$timeout,$q) {
            return {
                restrict: 'A',
                templateUrl: "app/sftCms.Fields/relationship/relationship-field.html",
                scope: {
                    field: '=',
                    list: '&',
                    form: '=',
                    ngModel: '=',
                    model:"="
                },
                compile: function (element, attrs) {
                    return {
                        pre: function (scope, element, attrs) {
                            scope.fieldData = {
                                path: scope.field.path,
                                label: scope.field.label,
                                placeholder : 'None'
                            }
                        },
                        post: function (scope, elem, attrs, ngModel) {

                            var field = scope.field,
                                list = scope.list(),
                                $itemScope;
                            
                            scope.searchText = '';
                            scope.selectedOptions = [];

                            function validate() {
                                var invalid = !scope.ngModel  || (field.options.many && !scope.ngModel.length);
                            }

                            function activate(){

                                if (scope.ngModel){
                                    if (field.options.many && scope.ngModel.length){
                                        scope.fieldData.placeholder = '';
                                        scope.ngModel = scope.ngModel.map(function(item){
                                            return item._id;
                                        })
                                        AdminAPI.GetManyItemsBasic(field.options.ref, scope.ngModel).then(function(res) {
                                            scope.selectedOptions = res.data.data.map(function(item){
                                                return {
                                                    name : field.options.relationDisplayKey ? item.fields[field.options.relationDisplayKey] : item.name,
                                                    id : item.id
                                                };
                                            })
                                        });
                                    }
                                    else if (!field.options.many){
                                        scope.fieldData.placeholder = '';
                                        scope.searchText = scope.ngModel.value;
                                        scope.ngModel = scope.ngModel._id;
                                    }

                                }

                                $timeout(function(){
                                    validate();
                                })
                            }

                            function getItem(itemId){
                                return AdminAPI.GetItem(field.options.ref,itemId);
                            }

                            function clear(){
                                scope.ngModel = null;
                                scope.fieldData.placeholder = 'None'
                            }

                            function buildFilters(){
                                var parts = [];

                                if (!scope.field.filters) return null;

                                for (key in scope.field.filters){
                                    var filterVal;
                                    if (typeof scope.field.filters[key] == 'string' && scope.field.filters[key][0] == ':'){
                                        var fieldName = scope.field.filters[key].slice(1);

                                        var val = scope.model[fieldName];
                                        if (val) {
                                            filterVal = val;
                                        }else if (fieldName === ':_id' && scope.model.id) {
                                            filterVal = scope.model.id;
                                        }

                                    } else {
                                        filterVal = scope.field.filters[key];
                                    }

                                    if (filterVal)
                                        parts.push('filters[' + key + ']=' + encodeURIComponent(filterVal));
                                }
                                return parts.join('&');
                            }

                            activate();
                            
                            scope.autocompleteRequireMatch = []

                            scope.searchChanged = function(selectedItem, searchText){
                                if(!selectedItem || selectedItem.name != searchText || !searchText)
                                    clear();
                            }

                            function chipRemoveable(index){
                                for (var bindPath in scope.field.bindings){
                                    for (var i=scope.field.bindings[bindPath].length-1; i>=0; i--){
                                        var bind = scope.field.bindings[bindPath][i];
                                        if (bind.modelIndex == index)
                                            scope.field.bindings[bindPath].splice(i,1);
                                        else if (bind.modelIndex > index)
                                            bind.modelIndex--;
                                    }
                                }
                                
                                return true;
                            }

                            function removeBindOptions(chip, index){
                                $itemScope = $itemScope || angular.element('#page_content_inner').scope();

                                if (!$itemScope.removeRelationship) return;
                                
                                $itemScope.removeRelationship(scope.field.path, chip.id, index);
                            }

                            scope.chipRemoved = function (chip, index) {
                                var realIndex = scope.ngModel.indexOf(chip.id);

                                chipRemoveable(realIndex);
                                removeBindOptions(chip, realIndex);

                                if (field.options.many) {
                                    for (var i = 0, length = scope.ngModel.length; i < length; i++) {
                                        if (chip.id == scope.ngModel[i]) {
                                            scope.ngModel.splice(i, 1);
                                            break;
                                        }
                                    }
                                    if (!scope.ngModel.length) 
                                        clear();
                                } else{
                                    clear();
                                }

                                validate();
                            }

                            scope.chipAdded = function (chip) {
                                if (!scope.ngModel)
                                    scope.ngModel = field.options.many ? [] : null


                                if (field.options.many)
                                    scope.ngModel.push(chip.id);
                                else
                                    scope.ngModel = chip.id;
                                
                                if (scope.fieldData.placeholder)
                                    scope.fieldData.placeholder = '';

                                validate();

                                if (chip.data){
                                    chip.data.value = chip.name;
                                    scope.$emit('bind:add',{
                                        fieldPath : field.path,
                                        data : chip.data
                                    })
                                }
                            }

                            scope.selectedChanged = function(selectedItem){
                                if (!selectedItem) return;
                                scope.chipAdded(selectedItem);
                            }

                            scope.getMatches = function (searchText) {
                                if (field.options.required && !field.options.many && scope.searchText != searchText){
                                    scope.form[field.path].$setDirty();
                                    scope.form[field.path].$setValidity('required', false);
                                }

                                var filters = buildFilters();

                                return AdminAPI.FilterRelationShipAutocomplete(field.options.ref, list.key, field.path, searchText, scope.ngModel ,field.options.showAll,filters).then(function (res) {
                                    return res.data.items;
                                });
                            }
                        }
                    };
                }

            };
        }
    ]);