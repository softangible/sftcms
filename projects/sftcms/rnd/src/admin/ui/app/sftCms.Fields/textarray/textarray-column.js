/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('textarrayColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '&'
                },
                template:'<span ng-bind="::value"></span>',
                replace : true,
                link: function (scope) {
                    var model = scope.ngModel();
                    scope.value = (model && model.length) ? model.join(', ') : '';
                }
            };
        }
    ]);