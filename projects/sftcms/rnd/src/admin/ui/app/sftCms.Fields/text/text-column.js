/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('textColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '&',
                    field : '&',
                    doc : '&'
                },
                template:'<span ng-style="inputStyle" ng-bind="::value"></span>',
                replace : true,
                link: function (scope) {
                    scope.value = scope.ngModel();
                    if (scope.value===undefined || scope.value===null){
                        var doc = scope.doc();
                        var field = scope.field();

                        if(field.path.indexOf('.') != -1){
                            try{
                                eval('scope.value = doc.' + field.path);
                            }catch(err){}
                        }
                    }
                    
                    scope.inputStyle = {
                        'word-break' : 'break-word'
                    };
                    var field = scope.field();
                    if (field.options && field.options.direction && field.options.direction.toLowerCase()=='ltr') {
                        scope.inputStyle.direction = 'ltr';
                        scope.inputStyle.display= 'block';
                    }
                }
            };
        }
    ]);