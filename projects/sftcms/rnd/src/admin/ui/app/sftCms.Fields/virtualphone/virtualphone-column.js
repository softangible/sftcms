/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('virtualphoneColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '='
                },
                template:'<span ng-bind="value"></span>',
                replace : true,
                link : function (scope, element, attrs){
                    scope.value = scope.ngModel.virtualPhone || scope.ngModel.phone; 
                }
            };
        }
    ]);