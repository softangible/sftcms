/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('emailField', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/email/email-field.html",
                scope : {
                    ngModel : '=',
                    field:'=',
                    form:'='
                }
            };
        }
    ]);