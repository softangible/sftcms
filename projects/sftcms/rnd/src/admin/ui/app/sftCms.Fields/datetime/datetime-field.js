/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('datetimeField', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/datetime/datetime-field.html",
                scope:{
                    ngModel : '='
                },
                link: function (scope, elem, attrs) {
                    if (scope.ngModel){
                        var dateMoment = moment(scope.ngModel);
                        scope.dateVal = dateMoment.format('DD.MM.YYYY');
                        scope.timeVal = dateMoment.format('HH:mm');
                    }
                    
                    scope.$watchGroup(['timeVal','dateVal'],function(){
                        var m = moment(scope.dateVal+' '+ scope.timeVal,'DD.MM.YYYY HH:mm')
                        var p1 = m.format('YYYY-MM-DD');
                        var p2 = m.format('HH:mm:00.000');

                        scope.ngModel = new Date(p1+'T'+p2+'Z');
                    })

                    scope.now=function(){
                        var m = new moment();
                        scope.dateVal = m.format('DD.MM.YYYY');
                        scope.timeVal = m.format('HH:mm');
                    }
                }
            };
        }
    ]);