/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('textarrayField', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/textarray/textarray-field.html",
                scope : {
                    ngModel : '=',
                    field: '=',
                    form:'='
                },
                link : function(scope,e,attrs){
                    if (!scope.ngModel)
                        scope.ngModel = [];
                    
                    scope.remove = remove;
                    scope.add = add;

                    function remove(index){
                        scope.ngModel.splice(index,1);
                    }

                    function add(){
                        scope.ngModel.push('');
                    }
                }
            };
        }
    ]);