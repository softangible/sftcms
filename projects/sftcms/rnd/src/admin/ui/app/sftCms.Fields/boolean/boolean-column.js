/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('booleanColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '='
                },
                template:'<md-checkbox aria-label="checkbox" md-no-ink ng-disabled="true" ng-model="ngModel" class="md-primary"></md-checkbox>',
               // replace : true
            };
        }
    ]);