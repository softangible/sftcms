/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('selectFilter', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/select/filter/select-filter.html",
                scope : {
                    ngModel : '=',
                    col : '&'
                },
                link: function (scope, elem, attrs) {
                    scope.ngModel = { $in: ['', null] };
                    scope.colOptions = scope.col().options.options;
                    
                    scope.$watch('selectedItem',function(){
                        if (!scope.selectedItem)
                            scope.ngModel = { $in: ['', null] };
                        else
                            scope.ngModel = scope.selectedItem.value;
                    })
                }
            };
        }
    ]);