/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('numberFilter', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/number/filter/number-filter.html",
                scope : {
                    ngModel : '='
                },
                link: function (scope, elem, attrs) {
                    scope.numbers = {};
                    
                    scope.types = [
                        {key:'exactly', id:1},
                        {key:'greater than', id:2},
                        {key:'less than', id:3},
                        {key:'between', id:4}
                    ];
                    
                    scope.currentType = scope.types[0];
                    
                    scope.setType = function(type){
                        scope.currentType = type;
                    }
                    
                    scope.$watchGroup(['numbers.startNum','numbers.endNum','currentType'],function(newValues, oldValues, scope){
                        if (scope.currentType.id === 1 && !scope.numbers.startNum) {
                            scope.ngModel = { $in: ['', 0, null] };
                            return;
                        }
                        if (scope.currentType.id === 4) {
                            var min = scope.numbers.startNum;
                            var max = scope.numbers.endNum;
                            if (!isNaN(min) && !isNaN(max)) {
                                scope.ngModel = {
                                    $gte: min,
                                    $lte: max
                                };
                            }
                            return;
                        }
                        var value = scope.numbers.startNum;
                        if (!isNaN(value)) {
                            if (scope.currentType.id === 2) {
                                scope.ngModel = { $gt: value };
                            }
                            else if (scope.currentType.id === 3) {
                                scope.ngModel = { $lt: value };
                            }
                            else {
                                scope.ngModel = value;
                            }
                        }
                    });
                    
                }
            };
        }
    ]);