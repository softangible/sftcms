/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('booleanFilter', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/boolean/filter/boolean-filter.html",
                scope:{
                    ngModel : '='
                },
                link: function (scope, elem, attrs) {
                    scope.types = [
                        {key:'checked', id:1, value:true},
                        {key:'not checked', id:2,value:false}
                    ];
                    
                    scope.currentType = scope.types[0];
                    
                    scope.setType = function (type) {
                        scope.ngModel = type.value;
                        scope.currentType = type;
                    }
                    
                    scope.$watch('currentType',function(){
                        if (scope.currentType.id==1)
                            scope.ngModel = true;
                        else
                            scope.ngModel = { $ne: true };
                    });
                }
            };
        }
    ]);