/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('datetimeFilter', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/datetime/filter/datetime-filter.html",
                scope:{
                    ngModel : '='
                },
                link: function (scope, elem, attrs) {
                    var MOMENT_FORMAT_STRING = 'DD.MM.YYYY';
                    scope.types = [
                        {key:'on', id:1},
                        {key:'after', id:2},
                        {key:'before', id:3},
                        {key:'between', id:4},
                    ];
                    
                    scope.dates = {};
                    
                    scope.currentType = scope.types[0];
                    
                    scope.setType = function(type){
                        scope.currentType = type;
                    }
                    
                    scope.$watchGroup(['dates.endDate','dates.startDate','currentType'],function(newValues, oldValues, scope){
                        if (scope.currentType.id === 4) {
                            if (scope.dates.startDate && scope.dates.endDate) {
                                var start = moment(scope.dates.startDate,MOMENT_FORMAT_STRING),
                                    end = moment(scope.dates.endDate,MOMENT_FORMAT_STRING);
                                if (start.isValid() && end.isValid()) {
                                    scope.ngModel = {
                                        $gte: start.startOf('day').toDate(),
                                        $lte: end.endOf('day').toDate()
                                    };
                                }
                            }
                        } else if (scope.dates.startDate) {
                            var afterMomtent = moment(scope.dates.startDate,MOMENT_FORMAT_STRING);
                            var beforeMomtent = moment(scope.dates.startDate,MOMENT_FORMAT_STRING);
                            if (beforeMomtent.isValid() && afterMomtent.isValid()) {
                                var after = afterMomtent.startOf('day').toDate();
                                var before = beforeMomtent.endOf('day').toDate();
                                if (scope.currentType.id === 2) {
                                    scope.ngModel = { $gte: after };
                                } else if (scope.currentType.id === 3) {
                                    scope.ngModel = { $lte: before };
                                } else {
                                    scope.ngModel = { $gte: after, $lte: before };
                                }
                            }
                        }
                    });
                    
                }
            };
        }
    ]);