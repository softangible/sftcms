/*
*  Altair Admin AngularJS
*  directives
*/
; 'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('userpasswordField', [
        function () {
            return {
                restrict: 'A',
                templateUrl: "app/sftCms.Fields/user-password/userpassword-field.html",
                scope: {
                    ngModel: '=',
                    field: '=',
                    form: '='
                },
                link: function (scope, e, attrs) {
                    if (attrs.hasOwnProperty('isInitial')) {
                        scope.showNew = true;
                        scope.ngModel = {
                                password: '',
                                password_confirm: ''
                        };  
                    } else {
                        scope.ngModel = null;
                    }

                    scope.showEdit = function(){
                        scope.showNew = true;
                    }

                }
            };
        }
    ]);