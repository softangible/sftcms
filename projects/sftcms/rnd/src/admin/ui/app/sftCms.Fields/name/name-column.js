/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('nameColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '='
                },
                template:'<div ng-bind="value"></div>',
                replace : true,
                link: function (scope) {
                    scope.value = scope.ngModel.first + ' ' + scope.ngModel.last;
                }
            };
        }
    ]);