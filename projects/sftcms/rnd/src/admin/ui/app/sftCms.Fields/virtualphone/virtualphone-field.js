/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('virtualphoneField', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/virtualphone/virtualphone-field.html",
                scope:{
                  ngModel : '=',
                  field : '&',
                  form : '='
                },
                link: function (scope, elem, attrs) {
                    
                    var field = scope.field();

                    scope.isRequired = field.options.required;
                    scope.path = field.path;
                    
                    scope.field = scope.field()
                    
                    if (!scope.ngModel)
                        scope.ngModel = {};
                }
            };
        }
    ]);