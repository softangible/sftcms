/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('objectidColumn', [
        function () {
            return {
                restrict: 'A',
                scope:{
                    ngModel : '='
                },
                template:'<span ng-bind="ngModel"></span>',
                replace : true
            };
        }
    ]);