/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('textField', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/text/text-field.html",
                scope : {
                    ngModel : '=',
                    field: '=',
                    form:'='
                },
                link : function(scope,e,attrs){
                    scope.inputStyle = {};
                    if (scope.field.options && 
                        scope.field.options.direction && 
                        scope.field.options.direction.toLowerCase()=='ltr')
                        scope.inputStyle.direction = 'ltr';
                    
                    if (!scope.ngModel)
                        scope.ngModel = '';
                }
            };
        }
    ]);