/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('imagepickerField', ['Elfinder','AdminAPI',
        function (Elfinder,AdminAPI) {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/imagepicker/imagepicker-field.html",
                scope : {
                    ngModel : '=',
                    field: '&',
                    form:'='
                },
                link : function(scope,e,attrs){
                    var field = scope.field();

                    scope.isMultiple = field.options.multiple;
                    
                    var PICK_SINGLE = 'בחר תמונה';
                    var PICK_MULTI = 'בחר תמונות';
                    var UPDATE_SINGLE = 'שנה תמונה';
                    var UPDATE_MULTI = 'הוסף תמונות';
                    
                    
                    scope.baseImageUrl = AdminAPI.GetConfig('elfinder').baseImgUrl;
                    
                    setBtnText();
                    
                    scope.openPicker = function($event){
                        Elfinder.OpenSelector($event,field.options.multiple,scope.setData);
                    }
                    
                    scope.removeImage = function(index){
                        if (index || index===0)
                            scope.ngModel.splice(index,1);
                        else if (!field.options.multiple)
                            scope.ngModel = null;
                            
                        setBtnText();
                    }
                    
                    scope.setData = function(data){
                        if (!field.options.multiple)
                        {
                            if (!scope.ngModel)
                               scope.ngModel = {src : data.src, title:'', alt:'', credit:''}
                            else
                               scope.ngModel.src = data.src;
                        }else
                            scope.ngModel = !scope.ngModel ? data : scope.ngModel.concat(data);
                            
                        setBtnText();
                    }
                    
                    
                    function setBtnText(){
                        var text;
                        
                        if (!scope.ngModel)
                            text = field.options.multiple ? PICK_MULTI : PICK_SINGLE;
                        else if (!field.options.multiple && !scope.ngModel.src)
                            text = PICK_SINGLE;
                        else
                            text = field.options.multiple ? UPDATE_MULTI : UPDATE_SINGLE;
                        
                        scope.btnText = text;
                    }
                }
            };
        }
    ]);