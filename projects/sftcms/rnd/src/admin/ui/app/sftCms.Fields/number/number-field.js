/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('numberField', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/number/number-field.html",
                scope : {
                    ngModel : '=',
                    field: '&'
                },
                link : function(scope){
                    if (!scope.ngModel && scope.ngModel !== 0)
                        scope.ngModel = null;
                }
            };
        }
    ]);