/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Fields')
    // add width/height properities to Image
    .directive('virtualphoneFilter', [
        function () {
            return {
                restrict: 'A',
                templateUrl:"app/sftCms.Fields/virtualphone/filter/virtualphone-filter.html",
                link: function (scope, elem, attrs) {
                    scope.types = [
                        {key:'checked', id:1, value:true},
                        {key:'not checked', id:2,value:false}
                    ];
                    
                    scope.currentType = scope.types[0];
                    scope.ngModel = scope.currentType.value;
                    
                    scope.setType = function (type) {
                        scope.ngModel = type.value;
                        scope.currentType = type;
                    }
                }
            };
        }
    ]);