/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Directives')
    .directive('dynamicDirective', [
        '$compile',
        function ($compile) {
            return {
                restrict: 'A',
                replace: true,
                template:'',
                //terminal: true, 
                priority: 1000, 
                link: function (scope, element, attrs) {
                    element.attr(scope.$eval(attrs.dynamicDirective),"");//add dynamic directive

                    element.removeAttr("dynamic-directive"); //remove the attribute to avoid indefinite loop

                    $compile(element)(scope);
                }
            };
        }
    ]);