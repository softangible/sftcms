/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

// callback on last element in ng-repeat
angular.module('sftCms.Directives').directive('onLastRepeat',['$timeout',function ($timeout) {
        return function (scope, element, attrs) {
            if (scope.$last) {
                $timeout(function () {
                    scope.$emit('onLastRepeat', element, attrs);
                })
            }
        };
}]);