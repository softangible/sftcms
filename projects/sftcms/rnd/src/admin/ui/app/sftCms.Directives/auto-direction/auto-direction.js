/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Directives')
    // add width/height properities to Image
    .directive('autoDirection', [
        '$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, elem, attrs) {
                    
                    function isUnicode(str) {
                        var letters = [];
                        for (var i = 0; i <= str.length; i++) {
                            letters[i] = str.substring((i - 1), i);
                            if (letters[i].charCodeAt() > 255) { return true; }
                        }
                        return false;
                    }

                    var $elem = $(elem);

                    var tagName = $elem.prop('tagName').toLowerCase();

                    function set(e){
                        if (isUnicode($elem.text())) {
                            if(tagName == 'span'){ $elem.attr('dir','rtl');}
                            $elem.css('direction', 'rtl');
                        }
                        else {
                            if(tagName == 'span'){ $elem.attr('dir','ltr');}
                            $elem.css('direction', 'ltr');
                        }
                    }

                    $timeout(set);
                }
            };
        }
    ]);