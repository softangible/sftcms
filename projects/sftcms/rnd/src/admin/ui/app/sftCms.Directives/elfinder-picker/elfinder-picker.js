/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Directives')
    // card close
    .directive('elfinderPicker', [
        'AdminAPI',
        function (AdminAPI) {
            return {
                restrict: 'E',
                scope: {
                    onSelect : '=',
                    options : '='
                },
                link: function (scope, el, attrs) {
                    var opts = AdminAPI.GetConfig('elfinder');
                        opts.getFileCallback = scope.onSelect;
                        opts.useBrowserHistory = false;
                        opts.commandsOptions.getfile.multiple = scope.options.isMultiple || false;
                        
                    $(el).elfinder(opts);
                }
            }
        }
    ]);