/*
*  Altair Admin AngularJS
*/
;"use strict";

angular.module('sftCms.Directives', [
    'ui.tinymce',
    'oc.lazyLoad',
    'ncy-angular-breadcrumb',
    'ngMaterial',
    'ngMessages',
    'uiGmapgoogle-maps',
    'dndLists'
]);
