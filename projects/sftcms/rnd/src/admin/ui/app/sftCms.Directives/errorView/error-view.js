/*
*  Altair Admin AngularJS
*  directives
*/
;'use strict';

angular.module('sftCms.Directives')
    // add width/height properities to Image
    .directive('errorView', [function () {
            return {
                restrict: 'E',
                scope : {
                    errors : '=?'
                },
                templateUrl:"app/sftCms.Directives/errorView/error-view.html",
                link : function(scope, elm, attrs){
                    scope.hideTitle = attrs.hasOwnProperty('hideTitle');
                }
            };
        }
    ]);