'use strict';

var gulp = require('gulp'),
    del = require('del');

// clean release folder
gulp.task('clean', function() {
    return del.sync(
        [ './public/**', './assets/**' ],
        { force: true });
});