// -------------------- MINIFY JSON --------------------
'use strict';

var gulp = require('gulp'),
    plugins = require("gulp-load-plugins")({
        pattern: ['gulp-*', 'gulp.*'],
        replaceString: /\bgulp[\-.]/
    }),
    // chalk error
    chalk = require('chalk'),
    chalk_error = chalk.bold.red,
    // versionin config
    versionConfig = {
        'value': '%MDS%',
        'append': {
            'key': 'v',
            'to': ['css', 'js'],
        }
    };

gulp.task('html_copy', function() {
    return gulp.src([
            'index.html'
        ])
        .pipe(plugins.versionNumber(versionConfig))
        .pipe(gulp.dest('public/'));
});

gulp.task('tinymce_copy', function() {
    return gulp.src([
            'external/libs/tinymce/skin/**/*.*',
            'external/libs/tinymce/skin/*.*',
            'external/libs/tinymce/langs/he_IL.js'
        ])
        .pipe(gulp.dest('public/js/tinymce'));
});

gulp.task('themes_copy', function() {
    return gulp.src([
            'external/libs/js/themes/**/*.*',
            'external/libs/js/themes/*.*'
        ])
        .pipe(gulp.dest('public/js/themes'));
});

gulp.task('fonts_copy', function() {
    return gulp.src([
            'external/fonts/*.*'
        ])
        .pipe(gulp.dest('public/fonts'));
});

gulp.task('icons_copy', function() {
    return gulp.src([
            'external/icons/**/*.*',
            'app/icons/*.*'
        ])
        .pipe(gulp.dest('public/icons'));
});

gulp.task('img_copy', function() {
    return gulp.src([
            'external/img/**/*.*',
            'external/img/*.*'
        ])
        .pipe(gulp.dest('public/img'));
});