// -------------------- MINIFY/CONCATENATE JS FILES --------------------
'use strict';

var gulp = require('gulp'),
    plugins = require("gulp-load-plugins")({
        pattern: ['gulp-*', 'gulp.*'],
        replaceString: /\bgulp[\-.]/
    }),
    // chalk error
    chalk = require('chalk'),
    path = require('path'),
    chalk_error = chalk.bold.red;

// commmon
gulp.task('js_common', function () {
    return gulp.src([
            "bower_components/jquery/dist/jquery.min.js",
            "bower_components/modernizr/modernizr.js",
            // moment
            "bower_components/moment/moment.js",
            // fastclick (touch devices)
            "bower_components/fastclick/lib/fastclick.js",
            // custom scrollbar
            "bower_components/jquery.scrollbar/jquery.scrollbar.min.js",
            // create easing functions from cubic-bezier co-ordinates
            "bower_components/jquery-bez/jquery.bez.min.js",
            // Get the actual width/height of invisible DOM elements with jQuery
            "bower_components/jquery.actual/jquery.actual.min.js",
            // waypoints
            "bower_components/waypoints/lib/jquery.waypoints.min.js",
            // velocityjs (animation)
            "bower_components/velocity/velocity.min.js",
            "bower_components/velocity/velocity.ui.min.js",
            // advanced cross-browser ellipsis
            "bower_components/jquery.dotdotdot/src/js/jquery.dotdotdot.min.js",
            // hammerjs
            "bower_components/hammerjs/hammer.min.js",
            // scrollbar width
            "assets/js/custom/jquery.scrollbarWidth.js",
            // jquery.debouncedresize
            "bower_components/jquery.debouncedresize/js/jquery.debouncedresize.js",
            // screenfull
            "bower_components/screenfull/dist/screenfull.min.js",
            // waves
            "bower_components/Waves/dist/waves.min.js",
            //jquery ui
            'bower_components/jquery-ui/jquery-ui.min.js',
            //md-color-picker
            'bower_components/tinycolor/dist/tinycolor-min.js',
            //elfinder
            'external/libs/elfinder/js/elfinder.full.js',
            'external/libs/elfinder/js/i18n/elfinder.he.js'
        ])
        .pipe(plugins.concat('common.js'))
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('assets/js/'))
        .pipe(plugins.uglify({
           mangle: false
        }))
        .pipe(plugins.rename('common.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('public/js/'));
});

gulp.task('ng_template_cache', function () {
  return gulp.src([
            'app/shared/**/*.html',
            'app/shared/*.html',
            'app/routes/**/*.html',
            'app/routes/*.html',
            'app/views/**/*.html',
            'app/views/*.html',
            'app/sftCms.Directives/**/*.html',
            'app/sftCms.Directives/*.html',
           'app/sftCms.Fields/**/*.html',
            'app/sftCms.Fields/*.html',
             'app/sftCms.Web/**/*.html',
            'app/sftCms.Web/*.html'
    ])
    .pipe(plugins.angularTemplatecache({
        module : 'sftCms',
        base: './',
        transformUrl: function(url) {
            var base = path.sep + 'app' + path.sep;
            return url.substring(url.indexOf(base)+1);
        }
    }))
    .pipe(gulp.dest('assets/templates'));
});


// angular common js
gulp.task('js_angular_common', function () {
    return gulp.src([
            "bower_components/lodash/dist/lodash.min.js",
            "bower_components/angular/angular.min.js",
            "bower_components/angular-sanitize/angular-sanitize.min.js",
            "bower_components/angular-ui-router/release/angular-ui-router.min.js",
            "bower_components/angular-animate/angular-animate.min.js",
            "bower_components/angular-aria/angular-aria.min.js",
            "bower_components/angular-messages/angular-messages.min.js",
            "bower_components/angular-cookies/angular-cookies.min.js",
            "bower_components/angular-translate/angular-translate.min.js",
            "bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js",
            "external/libs/md-color-picker/mdColorPicker.js",
            "bower_components/angular-simple-logger/dist/angular-simple-logger.light.min.js",
            "bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js",
            "bower_components/angular-google-places-autocomplete/dist/autocomplete.min.js",
            "bower_components/angular-google-maps/dist/angular-google-maps.min.js",
            "bower_components/oclazyload/dist/ocLazyLoad.min.js",
            "bower_components/tinymce/tinymce.min.js",
            "bower_components/tinymce/plugins/**/plugin.min.js",
            "app/modules/angular-tinymce.js",
            "bower_components/angular-material/angular-material.min.js",
            "app/modules/angular-retina.js",
            "bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js",
            "bower_components/ngstorage/ngStorage.min.js"
        ])
        .pipe(plugins.concat('angular_common.js'))
        .pipe(gulp.dest('assets/js/'))
        // .pipe(plugins.uglify({
        //    mangle: false
        // }))
        .pipe(plugins.rename('angular_common.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('public/js/'));
});

// angular app minify
gulp.task('js_app_minify', function () {
    return gulp.src([
            "app/sftCms.Services/*.js",
            "app/sftCms.Services/**/*.js",
            "app/sftCms.Directives/*.js",
            "app/sftCms.Directives/**/*.js",
            "app/sftCms.Fields/*.js",
            "app/sftCms.Fields/**/*.js",
            "app/**/*.js",
            "assets/templates/templates.js",
            "!app/**/*.min.js"
        ],{base: './'})
        .pipe(plugins.uglify({
            mangle: false
        }))
        .pipe(plugins.rename({
            extname: ".min.js"
        }))
        .pipe(gulp.dest('./'));
});

// angular app minify
gulp.task('js_app_modules_minify', function () {
    return gulp.src([
            "app/modules/*.js",
            "!app/modules/*.min.js"
        ],{base: './'})
        .pipe(plugins.uglify({
            mangle: false
        }))
        .pipe(plugins.rename({
            extname: ".min.js"
        }))
        .pipe(gulp.dest('./'));
});

// app js
gulp.task('js_app', function () {
    return gulp.src([
             "app/sftCms.Services/*.js",
            "app/sftCms.Services/**/*.js",
            "app/sftCms.Directives/*.js",
            "app/sftCms.Directives/**/*.js",
            "app/sftCms.Fields/*.js",
            "app/sftCms.Fields/**/*.js",
            "app/app.js",
            "assets/templates/templates.js",
            "app/sftCms.Web/*.js",
            "app/sftCms.Web/**/*.js",
            "app/app.filters.js",
            "app/app.controller.js",
        ])
        .pipe(plugins.concat('sftcms_app.js'))
        .pipe(gulp.dest('assets/js/'))
        .pipe(plugins.uglify({
                mangle: false
            }).on('error', function(e) { 
                console.log('\x07',e.message + ':'+e.lineNumber); return this.end(); }
        ))
        .pipe(plugins.rename('sftcms_app.min.js'))
        .pipe(plugins.size({
            showFiles: true
        }))
        .pipe(gulp.dest('public/js/'));
});

// common/custom functions
gulp.task('js_minify', function () {
    return gulp.src([
            'assets/js/custom/*.js',
            '!assets/js/**/*.min.js'
        ])
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.rename({
            extname: ".min.js"
        }))
        .pipe(gulp.dest(function(file) {
            return file.base;
        }));
});