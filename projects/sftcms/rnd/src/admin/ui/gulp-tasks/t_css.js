// -------------------- MINIFY JSON --------------------
'use strict';

var gulp = require('gulp'),
    plugins = require("gulp-load-plugins")({
        pattern: ['gulp-*', 'gulp.*'],
        replaceString: /\bgulp[\-.]/
    }),
    // chalk error
    chalk = require('chalk'),
    chalk_error = chalk.bold.red;

gulp.task('css_minify', function() {
    return gulp.src([
            'bower_components/angular-material/angular-material.min.css',
            'bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css',
            'external/libs/md-color-picker/mdColorPicker.min.css'
        ])
        .pipe(plugins.concat('plugins.css'))
        //.pipe(plugins.cleanCss({compatibility: 'ie8'}))
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('assets/css/'))
        .pipe(plugins.rename('plugins.min.css'))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('theme_copy', function() {
    return gulp.src([
            'assets/css/*.css',
            'external/libs/uikit/uikit.rtl.css'
        ])
        .pipe(gulp.dest('public/css/'));
});

gulp.task('elfinder_css_minify', function() {
    return gulp.src([
            'bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css',
            'external/libs/elfinder/css/elfinder.min.css',
            'external/libs/elfinder/css/theme.css',
        ])
        .pipe(plugins.concat('elfinder.css'))
        //.pipe(plugins.cleanCss({compatibility: 'ie8'}))
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('assets/elfinder/'))
        .pipe(plugins.rename('elfinder.min.css'))
        .pipe(gulp.dest('public/elfinder/'));
});

gulp.task('elfinder_images_move', function() {
    return gulp.src([
            'bower_components/jquery-ui/themes/smoothness/images/*.*',
        ])
        .on('error', function(err) {
            console.log(chalk_error(err.message));
            this.emit('end');
        })
        .pipe(gulp.dest('public/elfinder/images/'));
});


gulp.task('elfinder_copy', function() {
    return gulp.src([
            'external/libs/elfinder/**/*.*',
            'external/libs/elfinder/*.*',
        ])
        .pipe(gulp.dest('public/elfinder/'));
});