/**
 * Returns an Express Router with bindings for the Admin UI static resources,
 * i.e files, less and browserified scripts.
 *
 * Should be included before other middleware (e.g. session management,
 * logging, etc) for reduced overhead.
 */

var express = require('keystone').express;
var router = express.Router();
var path = require('path');
var browserify = require('./browserify');

router.use(express.static(path.join(__dirname, '../ui/public')));

module.exports = function(fieldFiles){
	var customFields = browserify(fieldFiles);

	customFields.build();
	router.get('/js/custom-fields.js', customFields.serve);
	
	return router
} ;
