var basePopup = require('./base-popup')

function AlertPopup(title, content, okBtnText, direction) {
    this.direction = direction || 'ltr';
    this.title = title || 'Confirm Title';
    this.content = content || 'Confirm Content';
    this.okBtnText = okBtnText || 'Ok';
    
    this.type = 'alert';
    this.stack = (new Error()).stack;
}

AlertPopup.prototype = Object.create(basePopup.prototype);

module.exports = AlertPopup;