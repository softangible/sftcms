function Popup(message) {
    this.stack = (new Error()).stack;
}
Popup.prototype = new Error;

module.exports = Popup;