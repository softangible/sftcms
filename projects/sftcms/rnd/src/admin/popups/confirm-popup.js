var basePopup = require('./base-popup')

function ConfirmPopup(title, content, saveBtnText, cancelBtnText, direction) {
    this.direction = direction || 'ltr';
    this.title = title || 'Confirm Title';
    this.content = content || 'Confirm Content';
    this.saveBtnText = saveBtnText || 'Save';
    this.cancelBtnText = cancelBtnText || 'Cancel';
    
    this.type = 'confirm';
    this.stack = (new Error()).stack;
}

ConfirmPopup.prototype = Object.create(basePopup.prototype);

module.exports = ConfirmPopup;