var keystone = require('keystone');
var router = require('../../lib/router'),
	middlewares = require('./middleware'),
	initList = middlewares.InitList(),
	initListRespectHidden = middlewares.InitList(true);


function getAdminRoutes(adminBase) {
	
	var API = adminBase + '/api',
		SESSION = API + '/session',
		LIST = API + '/:list';

	return {
		ROOT : adminBase,
		ALL : adminBase + '*',
		API : API,
		ANY_OTHER : adminBase + '/*',
		SESSION : SESSION,
		SESSION_SIGNIN : SESSION + '/signin',
		SESSION_SIGNOUT : SESSION + '/signout',
		IS_AUTH : API + '/isAuthenticated',
		LOAD : API + '/load',
		NAV : API + '/getNav',
		SIGNIN : API + '/signin',
		SIGNOUT : API + '/signout',
		LIST_FULL : API + '/fullList/:list/:page([0-9]{1,5})?',
		LIST_PAGE : API + '/getPage/:list/:page([0-9]{1,5})?',
		LIST_METADATA : API + '/listMeatData/:list',
		LIST_DOWNLOAD : API + '/download/:list',
		LIST_AUTOCOMPLETE : LIST + '/autocomplete',
		LIST_ALL_INSTANCES : LIST + '/getAllInstances',
		LIST_PARENT : LIST + '/parent',
		LIST_ITEM_DELETE : LIST + '/delete',
		LIST_GET_RELATION : API + '/getRelation/:list/:id',
		LIST_ITEM_SAVE_OR_UPDATE : LIST + '/saveOrUpdate',
		LIST_GET_MULTIPLE : LIST + '/getMultiple',
		LIST_ITEM_REVERT_TO_PUBLISHED : LIST + '/:id/revert-to-published',
		LIST_ITEM_GET : LIST + '/:id',
	}
}


/**
 * Adds bindings for the admin routes
 *
 * ####Example:
 *
 *     var app = express();
 *     app.use(...); // middleware, routes, etc. should come before admin is initialised
 *     sft.routes(app);
 *
 * @param {Express()} app
 * @api public
 */

function routes(sft, app) {
	sft.app = app;
	var routes = getAdminRoutes('/'+sft.get('admin path'));

	// load custom field types provided by the developer
	app.use(routes.ROOT, require('../app/static')(sft.Types.getFieldTypesFiles()));

	// Init API request helpers
	app.use(routes.API, middlewares.ApiError)

	// Cache compiled view templates if we are in Production mode
	sft.set('view cache', sft.get('env') === 'production');

	// Session API
	app.post(routes.SESSION_SIGNIN, middlewares.CheckCSRF, require('./api/session/signin'));
	app.post(routes.SESSION_SIGNOUT, require('./api/session/signout'));
	
	// Bind auth middleware (generic or custom) to /{admin-base}* routes, allowing
	// access to the generic signin page if generic auth is used
	if (keystone.get('auth') === true) {
		
		if (!keystone.nativeApp || !keystone.get('session')) {
			app.all(routes.ALL, keystone.session.persist);
		}
		
		app.get(routes.IS_AUTH,require('./api/auth/is-authenticated'));
		app.get(routes.LOAD, require('./api/load-settings'));
		
		app.post(routes.SIGNIN, require('./api/auth/signin'));
		app.all(routes.SIGNOUT, require('./api/auth/signout'));
		
		app.all(routes.ALL, middlewares.UserAuth(routes.ROOT));
		
	} else if ('function' === typeof keystone.get('auth')) {
		app.all(routes.ALL, keystone.get('auth'));
	}
    
    if (sft.get('admin routes')){
        sft.get('admin routes')(app);
    }

	
	// Add custom user admin routes.
	router.addAdminRoutes(app);
	
	// Get the admin client navigation data
	app.get(routes.NAV, require('./api/get-nav'))
	
	// Generic Lists API
	app.all(routes.LIST_FULL, initListRespectHidden, require('./api/list/get-list')(true));
	app.all(routes.LIST_PAGE, initListRespectHidden, require('./api/list/get-list')(false));
	app.get(routes.LIST_METADATA, initListRespectHidden, require('./api/list/get-list-metadata'));

	app.all(routes.LIST_DOWNLOAD, initListRespectHidden, require('./api/download'));
	app.all(routes.LIST_AUTOCOMPLETE, initList, require('./api/list/autocomplete'));
	app.all(routes.LIST_ALL_INSTANCES, initList, require('./api/list/get-all-instances'));
	app.get(routes.LIST_PARENT, initList, require('./api/list/get-by-parent'));
	// TODO : Check why csrf fails
	// app.post(routes.LIST_ITEM_DELETE, middlewares.CheckCSRF, initList , require('./api/list/delete'));
	app.post(routes.LIST_ITEM_DELETE, initList , require('./api/list/delete'));
		
	app.get(routes.LIST_GET_RELATION, initList, require('./api/item/get-one-relation'));

	// TODO : check why csrf fails
	// app.post(routes.LIST_ITEM_SAVE_OR_UPDATE, middlewares.CheckCSRF, initList, require('./api/list/save-or-update'));
	app.post(routes.LIST_ITEM_SAVE_OR_UPDATE, initList, require('./api/list/save-or-update'));
	app.post(routes.LIST_GET_MULTIPLE, initList, require('./api/item/get-multiple'));
	
	app.get(routes.LIST_ITEM_REVERT_TO_PUBLISHED, initList, require('./api/item/revert-to-published'));
	app.get(routes.LIST_ITEM_GET, initList, require('./api/item/get'));
	
	// In any other case under /{admin-base} return 404.
	app.get(routes.ANY_OTHER, middlewares.notFound);
}

module.exports = routes;