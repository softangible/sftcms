var response = require('../base-response')

module.exports = function (req, res) {
    var query = req.list.model.findById(req.params.id);
    query.exec().then(result=>{
        if (!result) return res.status(404).json({ err: 'not found', id: req.params.id });

        var refData = result;

        try {
            result = result.toObject();
        } catch (err) { }

        result.value = refData[req.list.options.viewField || req.list.namePath];
        result.listPath = req.list.path;

        response(req, res, result);
        
    }).catch(err=>{
        res.status(500).json({ err: 'database error', detail: err });
    })
}