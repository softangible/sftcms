var _ = require('underscore');
var async = require('async');
var keystone = require('keystone');
var response = require('../base-response');
var itemsFormat = require('../utils').item.format;
var dbUtils = require('../../../../lib/core/dbUtils');

module.exports = function (req, res) {

	var query = req.list.model.findById(req.params.id);
	var adminPath = keystone.get('admin path');

	var fields = req.query.fields;
	if (req.query.basic !== undefined) {
		fields = false;
	}

	function addRelationshipsToQuery(list, query) {
		var populate = {};
		for (var key in list.fields) {
			if (list.fields[key].type === 'relationship') {
				var refList = dbUtils.getList(list.fields[key].options.ref);
				var populateField = { path: list.fields[key].path, subpath: list.fields[key].refList.namePath }
				if (!populate[populateField.path]) {
					populate[populateField.path] = [];
				}
				populate[populateField.path].push(populateField.subpath);
				if (refList.options && refList.options.viewField)
					populate[populateField.path].push(refList.options.viewField);
			}
		}
		for (var path in populate) {
			var populatePath = path;
			
			if (populate.hasOwnProperty(path)) {
				if (list.options.bind && list.options.bind.indexOf(path) == -1)
					query.populate(populatePath, populate[path].join(' '));
				else
					query.populate(populatePath);
			}
		}
		return query;
	}

	addRelationshipsToQuery(req.list, query);

	query.then(item=>{

		if (!item){
			res.status(404).json({ err: 'not found', id: req.params.id });
			return null;
		};

		return item.save().then(result=>{
            return item;
        });

	}).then(item=>{
        
        response(req, res, {success : true});

		// var tasks = [];
		// var drilldown;
		// var relationships;
		// var bindings = [];

		// /* Drilldown (optional, provided if ?drilldown=true in querystring) */
		// if (req.query.drilldown === 'true' && req.list.get('drilldown')) {
		// 	drilldown = {
		// 		def: req.list.get('drilldown'),
		// 		items: []
		// 	};

		// 	tasks.push(function (cb) {

		// 		// TODO: proper support for nested relationships in drilldown

		// 		// step back through the drilldown list and load in reverse order to support nested relationships
		// 		drilldown.def = drilldown.def.split(' ').reverse();

		// 		async.eachSeries(drilldown.def, function (path, done) {

		// 			var field = req.list.fields[path];

		// 			if (!field || field.type !== 'relationship') {
		// 				throw new Error('Drilldown for ' + req.list.key + ' is invalid: field at path ' + path + ' is not a relationship.');
		// 			}

		// 			var refList = field.refList;

		// 			if (field.many) {
		// 				if (!item.get(field.path).length) {
		// 					return done();
		// 				}
		// 				refList.model.find().where('_id').in(item.get(field.path)).limit(4).exec(function (err, results) {
		// 					if (err || !results) {
		// 						done(err);
		// 					}
		// 					var more = (results.length === 4) ? results.pop() : false;
		// 					if (results.length) {
		// 						// drilldown.data[path] = results;
		// 						drilldown.items.push({
		// 							list: refList.getOptions(),
		// 							items: _.map(results, function (i) {
		// 								return {
		// 									label: refList.getDocumentName(i),
		// 									href: '/' + adminPath + '/' + refList.path + '/' + i.id
		// 								};
		// 							}),
		// 							more: (more) ? true : false
		// 						});
		// 					}
		// 					done();
		// 				});
		// 			} else {
		// 				if (!item.get(field.path)) {
		// 					return done();
		// 				}
		// 				refList.model.findById(item.get(field.path)).exec(function (err, result) {
		// 					if (result) {
		// 						// drilldown.data[path] = result;
		// 						drilldown.items.push({
		// 							list: refList.getOptions(),
		// 							items: [{
		// 								label: refList.getDocumentName(result),
		// 								href: '/' + adminPath + '/' + refList.path + '/' + result.id
		// 							}]
		// 						});
		// 					}
		// 					done(err);
		// 				});
		// 			}

		// 		}, function (err) {
		// 			// put the drilldown list back in the right order
		// 			drilldown.def.reverse();
		// 			drilldown.items.reverse();
		// 			cb(err);
		// 		});

		// 	});
		// }

		// /* Relationships () */
		// tasks.push(function (cb) {

		// 	relationships = _.values(_.compact(_.map(req.list.relationships, function (i) {
		// 		if (i.isValid) {
		// 			return _.clone(i);
		// 		} else {
		// 			keystone.console.err('Relationship Configuration Error', 'Relationship: ' + i.path + ' on list: ' + req.list.key + ' links to an invalid list: ' + i.ref);
		// 			return null;
		// 		}
		// 	})));

		// 	async.each(relationships, function (rel, done) {

		// 		// TODO: Handle invalid relationship config
		// 		var list = keystone.list(rel.ref),
		// 			listDefaultColumns = _.clone(list.defaultColumns);
		// 		rel.list = _.clone(list);
		// 		rel.sortable = (rel.list.get('sortable') && rel.list.get('sortContext') === req.list.key + ':' + rel.path);

		// 		// TODO: Handle relationships with more than 1 page of results
		// 		var q = rel.list.paginate({ page: 1, perPage: 100 })
		// 			.where(rel.refPath).equals(item.id);

		// 		if (rel.filter)
		// 			q = q.find(rel.filter);

		// 		q = q.sort(list.defaultSort);

		// 		addRelationshipsToQuery(rel.list, q);

		// 		// rel.columns = _.reject(rel.list.defaultColumns, function(col) { return (col.type == 'relationship' && col.refList == req.list) });
		// 		rel.columns = listDefaultColumns.map(function (col) {
		// 			delete col.field;
		// 			delete col.subField;
		// 			delete col.refList;
		// 			return col;
		// 		});

		// 		var defaultColumns = listDefaultColumns;
		// 		defaultColumns.push({
		// 			path: '__t'
		// 		})

		// 		rel.list.selectColumns(q, defaultColumns);
		// 		delete rel.list.defaultColumns;
		// 		delete rel.list._defaultColumns;
		// 		delete rel.list.uiElements;
		// 		delete rel.list.initialFields;


		// 		for (var key in rel.list.fields) {
		// 			delete rel.list.fields[key].list;
		// 			delete rel.list.fields[key]._path;
		// 			delete rel.list.fields[key]._properties;
		// 		}

		// 		delete rel.list.options.inherits;
		// 		delete rel.list.schema;
		// 		delete rel.list.schemaFields;

		// 		if (rel.list.inherited)
		// 			rel.list.inherited = rel.list.inherited.map(function (li) {
		// 				return {
		// 					key: li.key,
		// 					label: li.label,
		// 					path: li.path
		// 				}
		// 			});

		// 		q.exec(function (err, results) {
		// 			for (var i = 0, length = results.results.length; i < length; i++) {
		// 				var result = _.assign(list.getData(results.results[i], fields), {
		// 				});

		// 				result.fields['_id'] = result.id;
		// 				result['__t'] = results.results[i]['__t'] ? keystone.list(results.results[i]['__t']).path : rel.list.path;

		// 				result = itemsFormat(result, rel.list.fields);
		// 				results.results[i] = result;
		// 			}
		// 			rel.items = results;
		// 			done(err);
		// 		});

		// 	}, cb);
		// });

		// /* Bindings () */
		// tasks.push(function (cb) {

		// 	async.each(req.list.bindedLists || [], function (bind, done) {

		// 		// TODO: Handle invalid relationship config
		// 		var list = keystone.list(bind.list);

		// 		// TODO: Handle relationships with more than 1 page of results
		// 		var q = list.model.find()
		// 			.where(bind.key).equals(item.id);

		// 		q.exec(function (err, results) {
		// 			for (var i = 0, length = results.length; i < length; i++) {
		// 				var currentlist = results[i].__t ? keystone.list(results[i].__t) : list;

		// 				var result = _.assign(list.getData(results[i], fields), {
		// 				});

		// 				var bindingMap;
		// 				try {
		// 					bindingMap = JSON.parse(result.fields._bindingMap);
		// 				} catch (err) {
		// 					continue;
		// 				}

		// 				for (var bindKey in bindingMap) {
		// 					if (bindingMap[bindKey].mode != 'binds' || !bindingMap[bindKey].binds.length) continue;

		// 					bindingMap[bindKey].binds.forEach(function (bind) {
		// 						if (bind.modelId == item.id.toString()) {
		// 							var fieldLabel = (currentlist && currentlist.fields[bindKey]) ? currentlist.fields[bindKey].label : '';
		// 							bindings.push({
		// 								binded: {
		// 									id: result.id,
		// 									name: result.name,
		// 									fieldLabel: fieldLabel
		// 								},
		// 								bindedTo: bind.innerPath
		// 							})
		// 						}
		// 					})
		// 				}
		// 			}
		// 			done(err);
		// 		});

		// 	}, cb);
		// });


		// /* Process tasks & return */
		// async.parallel(tasks, function (err) {
		// 	if (err) {
		// 		return res.status(500).json({
		// 			err: 'database error',
		// 			detail: err
		// 		});
		// 	}

		// 	var revId = item.get('rev_id');

		// 	item = _.assign(req.list.getData(item, fields), {
		// 		drilldown: drilldown,
		// 		relationships: relationships,
		// 		bindings: bindings
		// 	});

		// 	item.id = req.params.id;
		// 	item = itemsFormat(item, req.list.fields);
		// 	item.revId = revId;
		// 	item.publishedRevId = revId;

		// 	response(req, res, item);
		// });
	}).catch(err=>{
		res.status(500).json({ err: 'database error', detail: err });
	})
};