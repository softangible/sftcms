var _ = require('underscore');
var async = require('async');
var keystone = require('keystone');
var response = require('../base-response');

module.exports = function(req, res) {

	var query = req.list.model.find({
		_id : { $in : req.body.ids }
	});

	var adminPath = keystone.get('admin path');
	
	var fields = req.query.fields;
	if (req.query.basic !== undefined) {
		fields = false;
	}

	if (req.list.tracking && req.list.tracking.createdBy) {
		query.populate(req.list.tracking.createdBy);
	}

	if (req.list.tracking && req.list.tracking.updatedBy) {
		query.populate(req.list.tracking.updatedBy);
	}

	query.exec().then(items=>{
		if (!items) return res.status(404).json({ err: 'not found', id: req.params.id });

		var tasks = [];
		
		/* Process tasks & return */
		async.parallel(tasks, function(err) {
			if (err) {
				return res.status(500).json({
					err: 'database error',
					detail: err
				});
			}
			response(req,res, items.map(function(item){return req.list.getData(item, fields)}));
		});
		return null;
	}).catch(err=>{
		res.status(500).json({ err: 'database error', detail: err });
	})
};
