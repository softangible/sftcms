var keystone = require('keystone'),
    _ = require('underscore'),
    CallResponse = require('./base-response');

module.exports = function (req, res) {
    var adminNav = JSON.parse(JSON.stringify(keystone.get('navigation')));


    function mapList(list) {
        var model, relationships, includeInstances, children;

        if (_.isString(list))
            model = keystone.list(list)
        else if (_.isObject(list)) {
            model = keystone.list(list.list);
            includeInstances = list.includeInstances;
            if (list.includeInstances) {
                relationships = list.relationships;
            }
            if (list.children && _.isArray(list.children)) {
                children = [];
                list.children.forEach(function (child) {
                    children.push(mapList(child));
                })
            }
        }

        if (!model)
            throw "Could not find list : " + list


        return {
            path: model.path,
            title: model.label,
            relationships: relationships,
            includeInstances: includeInstances,
            children: children,
            tree: model.options.tree
        };
    }

    adminNav = adminNav.map(function (section) {
        if (_.isObject(section)) {
            section.lists = section.lists.map(function (list) {
                return mapList(list);
            });

            return {
                title: section.label,
                submenu: section.lists
            };
        } else if (_.isString(section)) {
            return mapList(section);
        }
    })

    var result = {
        Content: {
            title: 'Content',
            icon: '&#xE85C;',
            submenu: adminNav
        }
    };

    if (keystone.get('external-links'))
        result.External = {
            title: 'שונות',
            icon: '&#xE157;',
            submenu: keystone.get('external-links')
        };

    CallResponse(req, res, result);
}