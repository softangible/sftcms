var RouteUtils = require('../utils'),
    itemsFormat = RouteUtils.item.formatMultiple,
    CallResponse = require('../base-response');

exports = module.exports = function(isFull) {
    return function(req, res) {
        var sort = RouteUtils.sort.formatter(req,res);
        var queryFilters = RouteUtils.filters.getQueryFilters(req.query.search, req.body.filters, req.list);

        var query = req.list.paginate({
            filters: queryFilters,
            page: req.params.page,
            perPage: req.list.get('perPage')
        }).sort(sort.by);

        var populate = {};
        for (var key in req.list.fields) {
            if (req.list.fields[key].type === 'relationship') {
                var populateField = {
                    path: req.list.fields[key].path,
                    subpath: req.list.fields[key].refList.options.viewField || req.list.fields[key].refList.namePath
                }
                if (!populate[populateField.path]) {
                    populate[populateField.path] = [];
                }
                populate[populateField.path].push(populateField.subpath);
            }
        }
        for (var path in populate) {
            if (populate.hasOwnProperty(path)) {
                query.populate(path, populate[path].join(' '));
            }
        }

        query.forceRegular = true;
		query.exec().then(items =>{
			// if there were results but not on this page, reset the page
            if (req.params.page && items.total && !items.results.length) {
                return res.sendStatus(400);
            }

            // go straight to the result if there was a search, and only one result
            if (req.query.search && items.total === 1 && items.results.length === 1)
                return CallResponse(req, res, {
                    redirect: items.results[0].id
                });

            items.results = itemsFormat(items.results, req.list.fields);

            if (!isFull)
                return CallResponse(req, res, items);

            var list = RouteUtils.list.jsonClear(req.list);
            var columns = RouteUtils.columns.formatter(req, list);

            CallResponse(req, res, {
                list: RouteUtils.list.formatter(req, list),
                sort: sort,
                search: req.query.search,
                columns: columns,
                items: items
            });
		}).catch(err=>{
			console.log(err);
            res.status(500).send('Error querying items:<br><br>' + JSON.stringify(err));
		})
    }
};