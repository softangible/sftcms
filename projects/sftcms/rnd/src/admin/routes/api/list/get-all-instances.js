var keystone = require('keystone'),
	CallResponse = require('../base-response');

exports = module.exports = function(req, res) {
	var query = req.list.model.find();
	
	query.exec().then(items=>{
		CallResponse(req,res,{
			items: items.map(function(i) {
				return {
					name: req.list.getDocumentName(i, false) || '(' + i.id + ')',
					id: i.id
				};
			})
		});
	}).catch(err=>{
		res.status(500).send('database error');
	})
};
