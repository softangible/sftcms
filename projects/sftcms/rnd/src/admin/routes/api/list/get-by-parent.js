var keystone = require('keystone'),
	CallResponse = require('../base-response');

exports = module.exports = function (req, res) {

	if (!req.list.options.tree)
		return res.status(400).send('Cannot get by parent for non tree model');

	var field;
	for (key in req.list.fields) {
		if (req.list.fields[key].type == 'relationship' && req.list.fields[key].options.ref == req.list.key) {
			field = req.list.fields[key];
			break;
		}
	}

	if (!field)
		return res.status(400).send('No parent field found!');

	var queryObj = {};
	queryObj[field.path] = req.query.parentId;

	var query = req.list.model.find(queryObj);

	query.exec().then(items=>{
		CallResponse(req, res, {
			items: items.map( item => {
				return {
					name: req.list.getDocumentName(item, false) || '(' + item.id + ')',
					id: item.id,
					tree : true
				};
			})
		});

	}).catch(err=>{
		res.status(500).send('database error');
	})

};
