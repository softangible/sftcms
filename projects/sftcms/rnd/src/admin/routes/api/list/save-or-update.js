var keystone = require('keystone'),
	_ = require('underscore'),
	UpdateHandler = require('../../../core/updateHandler'),
	CallResponse = require('../base-response'),
	errorCodes = require('../../../../lib/error-code'),
	singleItemFormat = require('../utils').item.format;

exports = module.exports = function (req, res) {
	var updateHandler;
	var data = req.body.data;
	if (data && data._bindingMap && !_.isString(data._bindingMap))
		data._bindingMap = JSON.stringify(data._bindingMap);
		
	var updateObj = {
		flashErrors: false,
		logErrors: false
	}

	if (!req.list.get('nocreate') && req.body.action == 'save') {

		item = new req.list.model();
		
		var updateHandler = new UpdateHandler(req.list, item, req);

		if (req.list.nameIsInitial) {
			if (!req.list.nameField.validateInput(req.body.data, true, item)) {
				updateHandler.addValidationError(req.list.nameField.path, req.list.nameField.label + ' is required.');
			}
			req.list.nameField.updateItem(item, req.body.data);
		}


		updateObj.fields = req.list.initialFields
		if (req.query.related)
			updateObj.fields.push(req.list.fields[req.query.related]);
		
		handle();
	}
	
	else if (!req.list.get('noedit') && (req.body.action == 'update' || req.body.action == 'update-and-publish')) {
		var itemQuery = req.list.model.findById(data.id).select();
		itemQuery.exec(function (err, item) {
			if (err)
				return res.status(500).send('A database error occurred.');

			if (!item)
				return res.status(500).send('Item ' + req.params.item + ' could not be found.');

			updateHandler = new UpdateHandler(req.list, item, req);
			handle();
		})
	} else {
		sendError();
	}

	function handle() {
		updateHandler.process(data, updateObj, function (err, item) {
			
			// Check if saved revision
			var isRevised = false;
			if (err){
				for (var key in err.errors){
					if (err.errors[key].message == errorCodes.SavedRevision){
						isRevised = true;
						break;
					}
				}
			}
			if (err && !isRevised) return res.status(500).json(err);

			if (!req.query.related)
				return CallResponse(req, res, {
					name: req.list.getDocumentName(item, false),
					id: item.id,
					_id: item.id,
					revId : item.get('rev_id')
				});

			var result = _.assign(req.list.getData(item), {});

			result['__t'] = item['__t'] ? keystone.list(item['__t']).path : req.list.path;
			result = singleItemFormat(result, req.list.fields);
			
			if (!result.fields._id) result.fields._id = result.id;

			result.revId = item.get('rev_id');

			return CallResponse(req, res, result);
		});
	}

	function sendError(err) {
		res.status(500).json(err);
	}
};