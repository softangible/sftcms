var _ = require('underscore');
var async = require('async');
var keystone = require('keystone');
var mongoose = require('mongoose');

exports = module.exports = function (req, res) {

	var sendResponse = function (status) {
		res.json(status);
	};

	var sendError = function (key, err, msg) {
		msg = msg || 'API Error';
		key = key || 'unknown error';
		msg += ' (' + key + ')';
		console.log(msg + (err ? ':' : ''));
		if (err) {
			console.log(err);
		}
		res.status(500);
		sendResponse({ error: key || 'error', detail: err ? err.message : '' });
	};

	var limit = parseInt(req.query.limit || 50);
	var page = parseInt(req.query.page || 1);
	var skip = limit * (page - 1);

	req.query.q = decodeURI(req.query.q);
	var filters = req.list.getSearchFilters(req.query.q);

	var alternateKey;
	var queryListField = keystone.list(req.query.list).fields[req.query.field];
	if (queryListField.options.relationDisplayKey)
		alternateKey = queryListField.options.relationDisplayKey;
	else if (req.list.options.relationDisplayKey)
		alternateKey = req.list.options.relationDisplayKey;

	if (alternateKey) {
		var filtersKeys = Object.keys(filters);
		var filterValue = filters[filtersKeys[0]];
		filters = {};
		filters[alternateKey] = filterValue;
	}

	// apply value filters set by developer
	if (queryListField && queryListField.options.filters){
		for (var key in queryListField.options.filters)
			filters[key] = queryListField.options.filters[key];
	}

	if (req.query.existIds) {
		if (!Array.isArray(req.query.existIds))
			req.query.existIds = [req.query.existIds];

		filters._id = {
			$nin: req.query.existIds.map(function (id) {
				return mongoose.Types.ObjectId(id)
			})
		};
	}
	

	//var count = req.list.model.count(filters);

	var query = req.list.model.find(filters);


	if (!req.query.showAll)
		query = query.limit(limit)
			.skip(skip)
			.sort(req.list.defaultSort);

	if (req.query.context === 'relationship') {
		var srcList = keystone.list(req.query.list);
		if (!srcList) return sendError('invalid list provided');

		var field = srcList.fields[req.query.field];
		if (!field) return sendError('invalid field provided');

		_.each(req.query.filters, function (value, key) {
			query.where(key).equals(value ? value : null);
			//count.where(key).equals(value ? value : null);
		});
	}

	query.exec().then(items => {
		var refList = keystone.list(req.query.list);
		var includeData = refList.options && refList.options.bind && refList.options.bind.indexOf(req.query.field) != -1;

		var mapper = (item) => {
			var name = (alternateKey) ? item[alternateKey] : (req.list.getDocumentName(item, false) || '(' + item.id + ')');
			var i = {
				name: name,
				id: item.id
			};

			if (includeData) i.data = item;

			return i;
		};

		sendResponse({
			items: items.map(mapper)
		});
	}).catch(err => {
		sendError('database error', err);
	})
};
