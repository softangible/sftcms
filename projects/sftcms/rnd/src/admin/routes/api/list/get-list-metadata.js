var _ = require('underscore'),
	RouteUtils = require('../utils'),
	CallResponse = require('../base-response');

var lists = {};

module.exports = function (req, res) {
	if (lists[req.list.path])
		return CallResponse(req, res, lists[req.list.path]);	
	
	var sort = RouteUtils.sort.formatter(req, res);
	var list = RouteUtils.list.jsonClear(req.list);
	var columns = RouteUtils.columns.formatter(req,list);

	var formattedList = RouteUtils.list.formatter(req, list)
	
	if (formattedList.options.bind){
		for (var i=0, length = formattedList.options.bind.length; i<length; i++){
			if(list.fields[formattedList.options.bind[i]].type != 'relationship')
				continue;
			
			var newBind = RouteUtils.bind.map(formattedList, formattedList.options.bind[i]);
			formattedList.options.bind[i] = newBind;
		}
	}

	lists[req.list.path] = {
		list: formattedList,
		sort: sort,
		search: req.query.search,
		columns: columns
	};

	CallResponse(req, res, lists[req.list.path]);
};