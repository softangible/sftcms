var keystone = require('keystone');
var session = keystone.session,
	CallResponse = require('../base-response');

exports = module.exports = function(req, res) {
		//if (!keystone.security.csrf.validate(req))
		//	return res.sendStatus(401, 'There was an error with your request, please try again.');
		
		if (!req.body.email || !req.body.password)
			return res.sendStatus(401, 'Please enter your email address and password.');
		
		var onSuccess = function (user) {
			if (!user.isAdmin){
				session.signout(req,res,function(){
					res.sendStatus(401);
				})
			}
			else CallResponse(req,res);
		};

		var onFail = function (err) {
			res.sendStatus(401);
		};

		session.signin(req.body, req, res, onSuccess, onFail);
};
