var CallResponse = require('../base-response'),
    keystone = require('keystone');

exports = module.exports = function(req, res) {
	var isAuth = req.user && req.user.canAccessKeystone;
    CallResponse(req,res,{
        isAuth : isAuth, 
        userId : req.user ? req.user.id : null,
        userModelPath : keystone.list(keystone.get('user model')).path
    });
};
