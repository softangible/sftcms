var keystone = require('keystone');
var session = keystone.session,
	CallResponse = require('../base-response');

exports = module.exports = function(req, res) {

	session.signout(req, res, function() {
		CallResponse(req,res);
	});

};
