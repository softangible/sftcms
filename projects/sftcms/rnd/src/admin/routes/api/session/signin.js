var keystone = require('keystone');
var utils = keystone.utils;
var session = keystone.session;

function signin (req, res) {
	if (!req.body.email || !req.body.password) {
		return res.status(401).json({ error: 'email and password required' });
	}
	var User = keystone.list(keystone.get('user model'));
	var emailRegExp = new RegExp('^' + utils.escapeRegExp(req.body.email) + '$', 'i');
	
	User.model.findOne({ email: emailRegExp }).exec().then(user=>{
		if (!user) return res.json({ error: 'invalid details' });
		
		keystone.callHook(user, 'pre:signin', function (err) {
			if (err) return res.json({ error: 'pre:signin error', detail: err });
			user._.password.compare(req.body.password, function (err, isMatch) {
				if (err) return res.status(500).json({ error: 'bcrypt error', detail: err });
				if (!isMatch) return res.json({ error: 'invalid details' });
				
				session.signinWithUser(user, req, res, function () {
					keystone.callHook(user, 'post:signin', function (err) {
						if (err) return res.json({ error: 'post:signin error', detail: err });
						res.json({ success: true, user: user });
					});
				});
				
			});
		});
		
	}).catch(err=>{
		res.status(500).json({ error: 'database error', detail: err });
	})

}

module.exports = signin;
