var keystone = require('keystone');

module.exports = function (req, res, data) {

    var response = {
        csrf: keystone.security.csrf.getToken(req, res),
        data: data
    };

    res.json(response);
}