var CallResponse = require('../base-response'),
	Wysiwyg = require('./wysiwyg');
	Elfinder = require('./elfinder'),
	Google = require('./google'),
	Global = require('./global'),
	keystone = require('keystone');

exports = module.exports = function(req, res) {
	var loadData = {
		wysiwyg : Wysiwyg.getOptions(),
		elfinder : Elfinder.getOptions(),
		google : Google.getOptions(),
		global : Global.getOptions(),
		defaultLangs : keystone.get('langs')
	}
	
	CallResponse(req,res,loadData);
};
