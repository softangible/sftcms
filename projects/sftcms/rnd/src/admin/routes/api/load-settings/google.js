module.exports.getOptions = function(){
    if (!process.env.Google)
        return {};

    var GoogleConfig =  JSON.parse(process.env.Google);
    
    return {
        ApiKey : GoogleConfig && GoogleConfig.API && GoogleConfig.API.Place && GoogleConfig.API.Place.key ? GoogleConfig.API.Place.key : ""
    }
}