var keystone = require('keystone');

module.exports.getOptions = function(){
    if (!process.env.ImagePicker) return {};
    
    var pickerConfig = JSON.parse(process.env.ImagePicker);
    
    return {
                    url: '/' + keystone.get('admin path') + '/imagePicker/api/', // connector URL (REQUIRED)
                    lang: 'he', // language (OPTIONAL),
                    tmbUrl: '/image-handler/w-50/',
                    height: 712,
                    debug: false,
                    //getFileCallback: self.getFilesCallback,
                    commandsOptions: {
                        getfile: {
                            folders: false,
                            //multiple: self.props.multiple
                        }
                    },
                    uiOptions: {
                        toolbar: [['back', 'forward'], ['mkdir', 'upload'], ['open', 'download', 'getfile'], ['rm'], ['view']],
                        tree: {
                            openRootOnLoad: true,
                            syncTree: true
                        },
                        navbar: {
                            minWidth: 150,
                            maxWidth: 500
                        },
                        cwd: {
                            oldSchool: false
                        }
                    },
                    contextmenu: {
                        navbar: ['open'],
                        cwd: ['reload', 'back', '|', 'upload', 'mkdir'],
                        files: ['getfile', '|', 'open', '|', 'download', '|', 'rm']
                    },
                    baseImgUrl : pickerConfig.options.url
                }
}