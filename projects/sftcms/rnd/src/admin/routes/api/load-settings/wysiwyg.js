var _ = require('underscore'),
    keystone = require('keystone');

module.exports.getOptions = function(){
    var wysiwygOptions = {
			enableImages: keystone.get('wysiwyg images') ? true : false,
			enableCloudinaryUploads: keystone.get('wysiwyg cloudinary images') ? true : false,
			additionalButtons: keystone.get('wysiwyg additional buttons') || '',
			additionalPlugins: keystone.get('wysiwyg additional plugins') || '',
			additionalOptions: keystone.get('wysiwyg additional options') || {},
			overrideToolbar: keystone.get('wysiwyg override toolbar'),
			skin: keystone.get('wysiwyg skin') || 'keystone',
			menubar: keystone.get('wysiwyg menubar'),
			importcss: keystone.get('wysiwyg importcss') || ''
    },
    optsExtend = keystone.get('wysiwyg-extend');
    plugins = ['code', 'link'],
    options = _.defaults(
	    {},
		true,
		wysiwygOptions
    ),
	toolbar = options.overrideToolbar ? '' : 'bold italic | alignleft aligncenter alignright | bullist numlist | outdent indent | link';

		if (options.enableImages) {
			plugins.push('image');
			toolbar += ' | image';
		}

		if (options.additionalButtons) {
			var additionalButtons = options.additionalButtons.split(',');
			for (i = 0; i < additionalButtons.length; i++) {
				toolbar += (' | ' + additionalButtons[i]);
			}
		}
		if (options.additionalPlugins) {
			var additionalPlugins = options.additionalPlugins.split(',');
			for (i = 0; i < additionalPlugins.length; i++) {
				plugins.push(additionalPlugins[i]);
			}
		}
		if (options.importcss) {
			plugins.push('importcss');
			var importcssOptions = {
				content_css: options.importcss,
				importcss_append: true,
				importcss_merge_classes: true
			};
			
			_.extend(options.additionalOptions, importcssOptions);
		}
		
		if (!options.overrideToolbar) {
			toolbar += ' | code';
		}

		var opts = {
			toolbar:  toolbar,
			plugins:  plugins,
			menubar:  options.menubar || false,
			skin:     options.skin || 'keystone',
            //file_browser_callback : this.elFinderBrowser
		};

		/*if (this.shouldRenderField()) {
			opts.uploadimage_form_url = '/keystone/api/cloudinary/upload';
		} else {
			_.extend(opts, {
				mode: 'textareas',
				readonly: true,
				menubar: false,
				toolbar: 'code',
				statusbar: false
			});
		}*/

		if (options.additionalOptions){
			_.extend(opts, options.additionalOptions);
		}
		
		opts.fontsize_formats = [8,10,12,14,16,18,24,36].map(function(size){
			return size+'pt';
		}).join(' ');

		if (optsExtend)
			_.extend(opts, optsExtend);

		return opts;
}