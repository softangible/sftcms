module.exports = function (item, field, subField, refData){
    try{
		item = item.toObject();	
	}catch(err){}
	
	
	item.value = subField ? subField.format(refData) : refData[field.refList.options.viewField || field.refList.namePath];
	item.listPath = field.refList.path;
    
    return item;
}