var keystone = require('keystone');
var _ = require('underscore');

module.exports = function(req,list){
    
	var columns = (req.query.cols) ? req.list.expandColumns(req.query.cols) : req.list.defaultColumns;

	for (var i=0 ; i< columns.length; i++){
		for (var key in list.fields){
			if (list.fields[key].path == columns[i].path)
			{
				columns[i] = list.fields[key];
				break;
			}
		}
	}

	return columns;
}