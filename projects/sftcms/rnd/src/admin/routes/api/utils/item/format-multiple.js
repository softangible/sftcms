var _ = require('underscore'),
    setItem = require('./set-item');

module.exports = function(items, fields) {
    for (var i = 0; i < items.length; i++) {
		for (var key in fields) {
			if (fields[key].type === 'relationship') {
				var subField = fields[key].refList.fields[fields[key].refPath];
				var refData = items[i][key];
				if (refData) {
					if (_.isArray(refData))
						for (var z = 0; z < items[i][key].length; z++)
							items[i][key][z] = setItem(items[i][key][z], fields[key], subField, refData[z]);
					else {
						try {
							items[i] = items[i].toObject();
						} catch (err) { }
						items[i][key] = setItem(items[i][key], fields[key], subField, refData);
					}
					items[i][key]['__t'] = items[i][key]['__t'] ? keystone.list(items[i][key]['__t']).path : fields[key].refList.path;
				}
			}
		}
		try{
			items[i] = items[i].toObject();
		}catch(err){}
		
		if (items[i]['__t'])
			items[i]['__t'] = keystone.list(items[i]['__t']).path;
	}


	return items;
}