var _ = require('underscore');
var services = require('../../../../services');

module.exports = function(originalList){
	return services.list.getMetadata(originalList);
}