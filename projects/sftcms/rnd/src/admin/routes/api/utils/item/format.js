var _ = require('underscore'),
    setItem = require('./set-item');

module.exports = function(item, fields){
    for (var key in fields){
	    if (fields[key].type === 'relationship')
		{	
		    var subField = fields[key].refList.fields[fields[key].refPath];
            var refData = item.fields[key];
			if (refData)
			{
			    if (_.isArray(refData))
				    for(var z=0; z<item.fields[key].length; z++)
					    item.fields[key][z] = setItem(item.fields[key][z], fields[key],subField,refData[z]);																			
				else{
				    try{
					    item = item.toObject();	
					} catch(err){}
                    item.fields[key] = setItem(item.fields[key], fields[key],subField,refData);
				}				
			}				
		}
	}
            
    return item;
}