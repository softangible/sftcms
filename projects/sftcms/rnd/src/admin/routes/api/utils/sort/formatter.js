var keystone = require('keystone');
var _ = require('underscore');
var querystring = require('querystring');

module.exports = function(req,res){
    
	var sort = { by: req.query.sort || req.list.defaultSort };

	if (sort.by) {

		sort.inv = sort.by.charAt(0) === '-';
		sort.path = (sort.inv) ? sort.by.substr(1) : sort.by;
		sort.field = req.list.fields[sort.path];

		var clearSort = function () {
			delete req.query.sort;
			var qs = querystring.stringify(req.query);
			return res.redirect(req.path + ((qs) ? '?' + qs : ''));
		};


		if (sort.field) {
			// the sort is set to a field, use its label
			sort.label = sort.field.label;
			// some fields have custom sort paths
			if (sort.field.type === 'name') {
				sort.by = sort.by + '.first ' + sort.by + '.last';
			}
		} else if (req.list.get('sortable') && (sort.by === 'sortOrder' || sort.by === '-sortOrder')) {
			// the sort is set to the built-in sort order, set the label correctly
			sort.label = 'display order';
		} else if (req.query.sort) {
			// it looks like an invalid path has been specified (no matching field), so clear the sort
			return clearSort();
		}

	}

	delete sort.field;

	return sort;
}