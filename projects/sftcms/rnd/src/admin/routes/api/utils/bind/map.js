var keystone = require('keystone');
var _ = require('underscore');
var services = require('../../../../services');

module.exports = function (list, binding) {

	var refList = services.list.getMetadata(list.fields[binding].options.ref);
	if (refList.inherited)
		refList.inherited = refList.inherited.map(function (li) {
			return {
				key: li.key,
				label: li.label,
				path: li.path
			}
		})

	return {
		bindingPath: binding,
		list: refList
	}
}