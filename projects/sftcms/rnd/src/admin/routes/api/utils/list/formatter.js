var keystone = require('keystone');
var _ = require('underscore');

module.exports = function(req, list){
    
	if (list.inherited)
		list.inherited = list.inherited.map(function (li) {
			return {
				key: li.key,
				label: li.label,
				path: li.path
			}
		})

	list.nameFieldPath = req.list.nameField ? req.list.nameField.path : null;
	
	return list;
}