module.exports = {
	format : require('./format'),
	formatMultiple : require('./format-multiple')
}