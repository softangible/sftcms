function apiError(req, res, next) {
	res.apiError = function (key, errors) {
		var statusCode = 500;
		if (key === 404) {
			statusCode = 404;
			key = null;
			key = 'not found';
		}
		if (!key) {
			key = 'unknown error';
		}
		if (!errors) {
			errors = {
				general: {
					message: 'API Error'
				}
			};
		}

		res.status(statusCode);
		res.json({ message: key, errors: errors });
	};
	next();
}

module.exports = apiError;