function userAuth(adminBase) {
	var adminBase = adminBase;
	return function (req, res, next) {
		if (!req.user || !req.user.isAdmin)
			return res.redirect(adminBase + '/#/login?returnUrl=' + encodeURI(req.originalUrl));
		next();
	}
}

module.exports = userAuth;