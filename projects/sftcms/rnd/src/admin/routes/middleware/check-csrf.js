var keystone = require('keystone');

function checkCSRF(req, res, next) {
	if (!keystone.security.csrf.validate(req)) {
		return res.apiError('invalid csrf');
	}
	next();
}

module.exports = checkCSRF;