var keystone = require('keystone');

function initList(respectHiddenOption) {
		return function(req, res, next) {
			
			try{
				req.list = keystone.list(req.params.list);
			} catch(err){
				return res.sendStatus(404)
			}

			if (!req.list || (respectHiddenOption && req.list.get('hidden'))) {
				return res.sendStatus(404);
			}

			next();
		};
	}

module.exports = initList;