var _ = require('underscore');
var FieldType = require('keystone').Field;
var util = require('util');

/**
 * virtualPhone FieldType Constructor
 * @extends Field
 * @api public
 */
function virtualPhone(list, path, options) {
	this._fixedSize = 'large';
	options.nofilter = true; // TODO: remove this when 0.4 is merged
	virtualPhone.super_.call(this, list, path, options);
}
util.inherits(virtualPhone, FieldType);


/**
 * Registers the field on the List's Mongoose Schema.
 *
 * Adds String properties for .phone and .virtualPhone, and a virtual
 * with get() and set() methods for .usedPhone
 *
 * @api public
 */

virtualPhone.prototype.addToSchema = function() {
	var schema = this.list.schema;
	var paths = this.paths = {
		phone: this._path.append('.phone'),
		virtualPhone: this._path.append('.virtualPhone'),
		usedPhone: this._path.append('.usedPhone')
	};

	schema.nested[this.path] = true;
	schema.add({
		phone: String,
		virtualPhone: String
	}, this.path + '.');

	schema.virtual(paths.usedPhone).get(function () {
        return this.get(paths.virtualPhone) || this.get(paths.phone)
	});

	this.bindUnderscoreMethods();
};

/**
 * Formats the field value
 */

virtualPhone.prototype.format = function(item) {
	return item.get(this.paths.usedPhone);
};

/**
 * Validates that a value for this field has been provided in a data object
 */
virtualPhone.prototype.validateInput = function(data, required, item) {
	// Input is valid if none was provided, but the item has data
	if (!(this.path in data || this.paths.phone in data || this.paths.virtualPhone in data || this.paths.usedPhone in data) && item && item.get(this.paths.usedPhone)) return true;
	// Input is valid if the field is not required
	if (!required) return true;
	// Otherwise check for valid strings in the provided data,
	// which may be nested or use flattened paths.
	if (_.isObject(data[this.path])) {
		return (data[this.path].usedPhone || data[this.path].phone || data[this.path].virtualPhone) ? true : false;
	} else {
		return (data[this.paths.usedPhone] || data[this.paths.phone] || data[this.paths.usedPhone]) ? true : false;
	}
};

/**
 * Detects whether the field has been modified
 *
 * @api public
 */
virtualPhone.prototype.isModified = function(item) {
	return item.isModified(this.paths.phone) || item.isModified(this.paths.virtualPhone);
};

/**
 * Updates the value for this field in the item from a data object
 *
 * @api public
 */
virtualPhone.prototype.updateItem = function(item, data) {
	if (!_.isObject(data)) return;
	var paths = this.paths;
	var setValue;
	if (this.path in data && _.isString(data[this.path])) {
		// Allow the root path as an alias to {path}.usedPhone
		item.set(paths.full, data[this.path]);
	} else if (this.path in data && _.isObject(data[this.path])) {
		// Allow a nested object like { path: { phone: 'Jed' } }
		var valueObj = data[this.path];
		setValue = function(key) {
			if (key in valueObj && valueObj[key] !== item.get(paths[key])) {
				item.set(paths[key], valueObj[key]);
			}
		};
	} else {
		// Default to flattened paths like { 'path.phone': 'Jed' }
		setValue = function(key) {
			if (paths[key] in data && data[paths[key]] !== item.get(paths[key])) {
				item.set(paths[key], data[paths[key]]);
			}
		};
	}
	if (setValue) {
		_.each(['usedPhone', 'phone', 'virtualPhone'], setValue);
	}
};


/* Export Field Type */
exports = module.exports = virtualPhone;
