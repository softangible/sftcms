var _ = require('underscore');
var keystone = require('keystone');
var FieldType = keystone.Field;
var util = require('util');

/**
 * imagePicker FieldType Constructor
 * @extends Field
 * @api public
 */
function imagePicker(list, path, options) {
    var pickerConfig = JSON.parse(process.env.ImagePicker);

    this.baseImgUrl = pickerConfig.options.url;
    this.multiple = options.multiple;
    this._defaultSize = 'full';
    this._properties = ['multiple', 'baseImgUrl'];
    options.nofilter = true; // TODO: remove this when 0.4 is merged
    imagePicker.super_.call(this, list, path, options);
}
util.inherits(imagePicker, FieldType);


/**
 * Registers the field on the List's Mongoose Schema.
 *
 * Adds String properties for .phone and .imagePicker, and a virtual
 * with get() and set() methods for .usedPhone
 *
 * @api public
 */

imagePicker.prototype.addToSchema = function () {
    var schema = this.list.schema;
    var paths = this.paths = {
        global: this.path
    };

    if (this.multiple) {
        var ImageSchema = new keystone.mongoose.Schema({
            fullUrl: String,
            src: String,
            description: String,
            title: String,
            alt: String,
            credit: String
        });

        schema.add(this._path.addTo({}, [ImageSchema]));
    } else {
        schema.nested[this.path] = true;

        schema.add({
            fullUrl: String,
            src: String,
            description: String,
            title: String,
            alt: String,
            credit: String
        }, this.path + '.');

    }

    schema.methods.getFullImageSrc = function(field) {
        var baseImageUrl;
        try{
            baseImageUrl = JSON.parse(process.env.ImagePicker).options.url;
        }catch(err){
            baseImageUrl = '';
        }
        if (_.isArray(field))
            return field.map(function(img){ return baseImageUrl + img.src});
        else if (_.isObject(field))
            return field.src ? baseImageUrl + field.src : undefined;

        return field;
    };

    this.bindUnderscoreMethods();
};

/**
 * Formats the field value
 */

imagePicker.prototype.format = function (item) {
    return '';
};

/**
 * Validates that a value for this field has been provided in a data object
 */
imagePicker.prototype.validateInput = function (data, required, item) {

    // Input is valid if the field is not required
    if (!required) return true;

    var thisData = data[this.path];
    if (!thisData) return false;

    if (this.multiple) {
        if (!thisData instanceof Array || !thisData.length) return false
        for (var i = 0, length = thisData.length; i < length; i++)
            if (!thisData[i].src) return false;
    }
    else if (!thisData.src) return false

    return true;
};

/**
 * Detects whether the field has been modified
 *
 * @api public
 */

imagePicker.prototype.isModified = function (item) {
    return true;
};

/**
 * Updates the value for this field in the item from a data object
 *
 * @api public
 */
imagePicker.prototype.updateItem = function (item, data) {
    if (!_.isObject(data)) return;

    var result = data[this.paths.global];

    if (this.multiple && !_.isArray(result) && result) {
        result = []
        for (var key in data[this.paths.global]) {
            result.push(data[this.paths.global][key]);
        }
    }

    item.set(this.paths.global, result);
};

/* Export Field Type */
exports = module.exports = imagePicker;