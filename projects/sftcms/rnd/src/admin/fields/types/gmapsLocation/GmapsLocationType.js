var _ = require('underscore');
var FieldType = require('keystone').Field;
var util = require('util');

/**
 * gmapsLocation FieldType Constructor
 * @extends Field
 * @api public
 */
function gmapsLocation(list, path, options) {
	this._fixedSize = 'large';
	options.nofilter = true; // TODO: remove this when 0.4 is merged
	gmapsLocation.super_.call(this, list, path, options);
}
util.inherits(gmapsLocation, FieldType);


/**
 * Registers the field on the List's Mongoose Schema.
 *
 * Adds String properties for .phone and .gmapsLocation, and a virtual
 * with get() and set() methods for .usedPhone
 *
 * @api public
 */

gmapsLocation.prototype.addToSchema = function() {
	var schema = this.list.schema;
	var paths = this.paths = {
		cords: this._path.append('.cords'),
		address: this._path.append('.address'),
        formattedAddress : this._path.append('.address.formattedAddress'),
        addressJson : this._path.append('.addressJson'),
        neiborhoodJson : this._path.append('.neiborhoodJson'),
        global:this.path
	};
    
	schema.nested[this.path] = true;
	schema.add({
		cords: {},
		address: {}
	}, this.path + '.');

	this.bindUnderscoreMethods();
};

/**
 * Formats the field value
 */

gmapsLocation.prototype.format = function(item) {
	return item.get(this.paths.formattedAddress);
};

/**
 * Validates that a value for this field has been provided in a data object
 */
gmapsLocation.prototype.validateInput = function(data, required, item) {
	
    // Input is valid if the field is not required
	if (!required) return true;
	
    if (this.paths.addressJson in data ){
        var isJson;
        try {
            JSON.parse(data[this.paths.addressJson]);
            return true
        } catch (e) {}    
    }
    
    
    // Otherwise check for valid strings in the provided data,
	// which may be nested or use flattened paths.
	return false;
};

/**
 * Detects whether the field has been modified
 *
 * @api public
 */
gmapsLocation.prototype.isModified = function(item) {
	return item.isModified(this.paths.addressJson) || item.isModified(this.paths.neiborhoodJson);
};

/**
 * Updates the value for this field in the item from a data object
 *
 * @api public
 */
gmapsLocation.prototype.updateItem = function(item, data) {
	if (!_.isObject(data)) return;
    
    if (!data[this.paths.global] || !data[this.paths.global].address || !data[this.paths.global].cords) return;
    
    var currentLocation = data[this.paths.global];
    
    item.set(this.paths.global, currentLocation);
};

/* Export Field Type */
exports = module.exports = gmapsLocation;
